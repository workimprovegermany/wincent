<?php

session_start();

require_once ('app-foundation/server-php/foundation.php');



class WincentApp extends App
{


    public function init()
    {
        if (isset($_GET['token']))
        {
            $em = new Entity_mapper($this->db(),'user_login/1.0/config.xml');
            $fields = array(
                'utoken' => $_GET['token'],
            );

            $user_login_array = $em->find_by_fields($fields);

            if ($user_login_array !== null)
            {
                $this->user = $user_login_array[0]->field('uid');
            }
            else {
                $obj_redirect = new HTML_helper;
                $obj_redirect->redirect('../');

            }

        }

        // User tracking
        /*$tracking = new Entity('user_tracking/1.0/config.xml');
        $tracking->set_field('uid', $this->user);
        $tracking->set_field('site', $_SERVER['REQUEST_URI']);
        Entity_mapper::insert_entity_to_storage($tracking, $this->db());*/

    }




}