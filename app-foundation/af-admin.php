<?php

require_once("../WincentApp.php");
$app = WincentApp::get_instance();

$app_path = APP_ROOT_PATH.$app->config_entry('app_path');
$model_path = $app_path.'/models';

//TODO better routing
if ($_GET['af_create_db'] == '1')
{
    try {
        //build DB
        foreach ($app->entities() as $entity_descr)
        {


            $config_xml = new SimpleXMLElement(file_get_contents($model_path.'/'.$entity_descr['config']));
            $fields = $config_xml->xpath('/config/fields/field[type!="many_to_many"]');

            $sql = "CREATE TABLE IF NOT EXISTS `".$entity_descr['table_name']."` (";

            $sql .= "`id` int unsigned NOT NULL AUTO_INCREMENT,";
            $sql .= "`last_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,";

            foreach ($fields as $field)
            {
                //echo "FieldName:".$field->name."<br/>";

                $name = $field->name;
                $type = $field->type;
                $required = ($field->xpath('required') == false) ? '' : ($field->xpath('required')[0]->__toString() == 'true') ? 'NOT NULL' : '';

                $db_config = '';
                $val_default ='';

                if ($type == 'foreign_key')
                {
                    $db_type = 'int unsigned';
                    $db_config = '';
                }
                elseif ($type == 'timestamp')
                {
                    $db_type = 'timestamp';
                    $db_config = '';
                }
                elseif ($type == 'json')
                {
                    $db_type = 'text';
                    $db_config = 'COLLATE utf8_unicode_ci';
                }
                elseif ($type == 'string')
                {
                    $length = $field->length;
                    $default = $field->default;
                    $val_default = !empty($default) ? 'DEFAULT'.$default : 'DEFAULT "" ';

                    $db_type = empty($length) ? 'varchar(255)' : 'varchar('.$length.')';
                    $db_config = 'COLLATE utf8_unicode_ci';
                }
                elseif ($type == 'int')
                {
                    $default = $field->default;
                    $val_default = !empty($default) ? 'DEFAULT '.$default : 'DEFAULT 0';

                    $db_type = 'int';
                    $db_config = '';
                }
                elseif ($type == 'uint')
                {
                    $db_type = 'int unsigned';
                    $db_config = '';
                }
                elseif ($type == 'bool')
                {
                    $db_type = 'bool';
                    $db_config = '';
                }
                elseif ($type == 'decimal')
                {
                    $length = $field->length;
                    $default = $field->default;
                    $val_default = !empty($default) ? 'DEFAULT'.$default : 'DEFAULT "" ';

                    $db_type = empty($length) ? 'decimal(10,2)' : 'decimal('.$length.')';
                    $db_config = '';
                }
                else
                {
                    echo "<br/>ERROR: Unknown Type ($type) <br/>";
                }

                $sql .= "`$name` $db_type $db_config $val_default $required,";
            }

            $sql .= "PRIMARY KEY (`id`)";
            $sql .= ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

            $ret = $app->db()->db_con->exec($sql);

            print "ret: ".$ret."<br/>";
            echo "<pre>";
            print_r($sql);
            echo "</pre>";


            if (isset($entity_descr['initial_dataset']))
            {
                $initial_file = $model_path.'/'.$entity_descr['initial_dataset'];
                echo "dataset file: $initial_file <br/>";

                $statement = $app->db()->db_con->prepare("SELECT count(*) AS total FROM ".$entity_descr['table_name']);
                $statement->execute();
                $row = $statement->fetch(PDO::FETCH_ASSOC);

                echo "rows in db: ".$row['total']."<br/>";

                if ($row['total'] == 0)
                {
                    $ini_xml = new SimpleXMLElement(file_get_contents($initial_file));
                    $xml_entities = $ini_xml->xpath('/*/'.$entity_descr['name']);
                    foreach ($xml_entities as $xml_entity)
                    {
                        $xml_fields = $xml_entity->xpath('./*');
                        $e = new Entity();
                        foreach ($xml_fields as $xml_field)
                        {
                            $e->set_field($xml_field->getName(), $xml_field[0]);
                        }
                        $em = new Entity_mapper($app->db());
                        $em->set_config(new Entity_config($entity_descr['config']));
                        $result = $em->insert($e);
                    }
                }
            }

            // MANY_TO_MANY RELATION TABLES
            //$path = '/config/fields/field[type="many_to_many"][owner="' . $entity_descr['name'] . '"]';
            $path = '/config/relations/relation[type="many-to-many"]';
            $relations = $config_xml->xpath($path);
            foreach ($relations as $relation)
            {
                $table_name = $entity_descr['name'] . "_" . $relation->name[0];
                $sql = "CREATE TABLE IF NOT EXISTS `".$table_name."` (";

                $sql .= "`". $entity_descr['name'] ."` int unsigned NOT NULL,";
                $sql .= "`". $relation->name[0] ."` int unsigned NOT NULL,";

                $sql .= "PRIMARY KEY (`". $entity_descr['name'] . "`, `" . $relation->name[0] ."`)";
                $sql .= ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

                $ret = $app->db()->db_con->exec($sql);
            }

        }
    } catch (PDOException $e) {
        echo 'DB Connection failed: ' . $e->getMessage();
    }
}
else
{
    echo "<h1> folgende Entitäten sind im System definiert </h1>";
    echo "<ul>";
    foreach ($app->entities() as $entity_descr)
    {
        $en = $entity_descr['name'];
        $ec = $entity_descr['config'];
        echo "<li>$en ($model_path/$ec)</li>";
    }
    echo "</ul>";

    echo "<a href='af-admin.php?af_create_db=1'>DB erstellen</a>";
}