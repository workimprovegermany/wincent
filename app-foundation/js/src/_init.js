function UrlParams(){
    let url_string = window.location,
        url = new URL(url_string);

    this.token = url.searchParams.get('token');
}
const params = new UrlParams();
if(null === params.token && window.location.pathname.split('/').length > 3 && window.location.pathname.split("/")[2] !== 'doc' && window.location.pathname.split("/")[2] !== 'test'){
    redirectLogin();
}

function redirectLogin(){
    window.location.replace(window.location.origin);
}
