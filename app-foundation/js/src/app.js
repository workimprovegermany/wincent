function App(functionAfterInit)
{
    this.array_month_name = ['','Januar', 'Februar', 'März','April', 'Mai', 'Juli', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember'];
    this.array_month_name_short = ['','Jan', 'Feb', 'Mär','Apr', 'Mai', 'Jun', 'Jul', 'Aug', 'Sep', 'Okt', 'Nov', 'Dez'];
    this.array_day_names = ['Sonntag', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag'];
    this.array_day_names_short = ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'];

    this.afterInit = functionAfterInit;

    this.permissions = [];

    this.xhr = new XHR();

    this.user_id = null;

    this.init();
}

App.prototype.get_user_permission = function()
{
    var context = this;

    xhrPermissions=new XMLHttpRequest();

    xhrPermissions.onreadystatechange=function()
    {
        if (xhrPermissions.readyState===4 && xhrPermissions.status===200)
        {
            let response = xhrPermissions.responseText;
            const obj = JSON.parse(response);
            if(obj.permissions !== undefined)
                context.permissions = obj.permissions;

            if(typeof context.afterInit === 'function'){
                context.afterInit();
            }
            init_navi();
            checkNewMessageUser(context.user_id);
            context.check_admin_link();
            set_online_status(true);
            init_timer_online_status();

        }
    };

    xhrPermissions.open('POST', 'api', true);
    xhrPermissions.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'user.get_permissions',
            data: {
                user_id: context.user_id
            }
        }
    };

    xhrPermissions.send("event="+JSON.stringify(data.event));
};

App.prototype.init = function ()
{
    var context = this;

    xhrInit=new XMLHttpRequest();

    xhrInit.onreadystatechange=function()
    {
        if (xhrInit.readyState===4 && xhrInit.status===200)
        {
            let response = xhrInit.responseText;
            const obj = JSON.parse(response);
            const data = obj.data;
            if(data.length > 0){
                context.user_id = obj.data[0].uid;
                context.get_user_permission();
            } else {
                redirectLogin();
            }

        }
    };

    xhrInit.open('POST', 'api', true);
    xhrInit.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'entity.load',
            data: {
                entity_name: 'user_login',
                entity_filter: {utoken: params.token},
                entity_fields: ['uid']
            }
        }
    };

    xhrInit.send("event="+JSON.stringify(data.event));
};

App.prototype.user_can = function (permission)
{
    let bool;
    (this.permissions.indexOf(permission) !== -1)? bool = true : bool = false;
    return bool;

};

App.prototype.check_admin_link = function()
{
    let element  = document.getElementById('admin_link');
    if(!!element && !this.user_can('DO_EVERYTHING')){
        element.innerHTML = '';
    } else if(!!element && this.user_can('DO_EVERYTHING')){
        element.style.display = 'block';
    }
};

App.prototype.check_page_access_restrictions = function(permissions, access_denied_url)
{
    access_granted = true;
    console.log(this.permissions);
    for (var i=0; i < permissions.length; i++)
    {
        access_granted &= this.user_can(permissions[i]);
        if (!this.user_can(permissions[i])) {console.log('missing permission '+permissions[i]);}
    }


    if (!access_granted) window.location.replace(access_denied_url);
};

