function init_bootstrap_table(data_colums, data, search, after_function, on_page_change_function){
    document.getElementById('iframe-table').contentWindow.create_table(data_colums, data, search, after_function, on_page_change_function);
}

function create_table(colums, data, search, after_function, on_page_change_function) {

    if(!search)
        search = true;
    let $table = $('#table');
    $table.bootstrapTable('destroy');
    let pagination = false;

    if(data.length > 10){
        pagination = true;
    }

    $table.bootstrapTable({
        search: search,
        formatSearch: function () {return 'Suchen';},
        silent: true,
        sortable: 'both',
        columns: colums,
        data: data,
        pagination: pagination,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {return  'Zeile '+pageFrom + ' bis ' + pageTo + ' von ' + totalRows + ' Zeilen ';},
        formatRecordsPerPage: function (pageNumber) {return pageNumber + ' Zeilen pro Seite';},
        onPageChange: function () {
            if(typeof on_page_change_function === 'function')
                on_page_change_function();
        }

    });

    create_responsive_bootstrap_table();

    if(typeof after_function === 'function')
        after_function();


}

function create_responsive_bootstrap_table(){
    let table = document.getElementById('table');
    table.classList.add('responsive');
    table.classList.add('resp_as_accordion');
    let elemente = document.getElementsByTagName('th'),
        data_header = [];
    for(let i = 0; i < elemente.length; i++){
        data_header.push(elemente[i].innerText);
    }
    add_table_responsive_style(data_header);

}

