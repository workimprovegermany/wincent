$( function() {
    /*var textFile = null,
        makeTextFile = function (text) {
            var data = new Blob([text], {type: 'text/plain'});

            if (textFile !== null) {
                window.URL.revokeObjectURL(textFile);
            }

            textFile = window.URL.createObjectURL(data);

            return textFile;
        };*/

    var textFile = null;

    function makeTextFile(text){
        var data = new Blob([text], {type: 'text/plain'});

        if (textFile !== null) {
            window.URL.revokeObjectURL(textFile);
        }

        textFile = window.URL.createObjectURL(data);

        return textFile;
    }

});

function setButtonStateLoading(){
    let $button = $('.btn_03');
    $button.addClass('state-loading');
}

function setButtonStateSuccess(){
    let $button = $('.btn_03');
    $button.removeClass('state-loading');
    $button.addClass('state-success');
    setTimeout(function(){
        $('.btn_03').removeClass('state-success');
    }, 2500);
}

function setButtonStateError(){
    let $button = $('.btn_03');
    $button.removeClass('state-loading');
    $button.addClass('state-error');
    setTimeout(function(){
        $('.btn_03').removeClass('state-error');
    }, 2500);
}