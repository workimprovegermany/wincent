function setTwoLineChart(element , data, data2) {

    var chartdata = {
        type: 'line',
        data: {
            labels: ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"],
            datasets: [{
                backgroundColor: getChartcolorBackground($(element).parent().find('.chartcolor')),
                borderColor: getChartcolor($(element).parent().find('.chartcolor')),
                data: get_date_formatted_int_for_chart(data),
                fill: false
            },{
                backgroundColor: getChartcolorBackground($(element).parent().find('.chartcolorHighlight')),
                borderColor: getChartcolor($(element).parent().find('.chartcolorHighlight')),
                data: get_date_formatted_int_for_chart(data2),
                fill: false,
                borderDash: [5,5]
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    };

    var chart = new Chart(element, chartdata);
}

function setThreeLineChart(element , data, label_data, data2, label_data2, data3, label_data3, labels) {

    var chartdata = {
        type: 'line',
        data: {
            labels: get_date_formatted_string_for_chart(labels),
            datasets: [{
                label: label_data,
                backgroundColor: getChartcolorBackground($(element).parent().find('.chartcolor')),
                borderColor: getChartcolor($(element).parent().find('.chartcolor')),
                data: get_date_formatted_int_for_chart(data),
                fill: false
            },{
                label: label_data2,
                backgroundColor: getChartcolorBackground($(element).parent().find('.chartcolor')),
                borderColor: getChartcolor($(element).parent().find('.chartcolor')),
                data: get_date_formatted_int_for_chart(data2),
                fill: false,
                borderDash: [5,5]
            },{
                label: label_data3,
                backgroundColor: getChartcolorBackground($(element).parent().find('.chartcolorHighlight')),
                borderColor: getChartcolor($(element).parent().find('.chartcolorHighlight')),
                data: get_date_formatted_int_for_chart(data3),
                fill: false,
                borderDash: [5,5]
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    };

    var chart = new Chart(element, chartdata);
}

function setFilledLineChart(element , data, labels) {

    var chartdata = {
        type: 'line',
        data: {
            labels: get_date_formatted_string_for_chart(labels),
            datasets: [{
                backgroundColor: getChartcolorBackground(element),
                borderColor: getChartcolor(element),
                data: get_date_formatted_int_for_chart(data),
            }]
        },
        options: {
            legend: { display: false },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    };

    var chart = new Chart(element, chartdata);
}

function setDoughnutChart(element , data) {

    var chartdata = {
        type: 'doughnut',
        data: {
            labels: ["Eigenumsatz", "Partnerumsatz"],
            datasets: [{
                backgroundColor: [getChartcolor(element),'#cccccc'],
                data: get_date_formatted_int_for_chart(data),
            }]
        },
        options: {
            legend: { display: false },
        }
    };

    var chart = new Chart(element, chartdata);
}

function get_date_formatted_int_for_chart(data) {
    let array = data.split(','),
        $arrayData = [];

    for(let i = 0; i < array.length; i++){
        $arrayData.push(parseInt(array[i]));
    }

    return $arrayData;
}

function get_date_formatted_string_for_chart(data) {
    let array = data.split(','),
        $arrayData = [];

    for(let i = 0; i < array.length; i++){
        $arrayData.push(array[i]);
    }

    return $arrayData;
}

function getChartcolor(element) {
    var color = 'rgb(0,0,0)';

    if($(element).hasClass('chartcolor') || $(element).hasClass('chartcolorHighlight')){
        color = $(element).css('color');
    }

    return color;
}

function getChartcolorBackground(element) {
    var color = 'transparent';

    if($(element).hasClass('chartcolor') || $(element).hasClass('chartcolorHighlight')){
        color = $(element).css('color');
        color = "rgba("+color.split('(')[1].split(')')[0]+",0.2)";
    }

    return color;
}
