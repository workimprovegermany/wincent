function createChatTabs(data){
    let tempArray = [];
    for(let i = 0; i < data.length; i++){
        tempArray.push(data[i].absender);
    }

    usersChat.arrayUserIds = uniqArray(tempArray),

        xhrUserData=new XMLHttpRequest();

    xhrUserData.onreadystatechange=function()
    {
        if (xhrUserData.readyState===4 && xhrUserData.status===200)
        {
            let response = xhrUserData.responseText;
            const obj = JSON.parse(response);
            usersChat.arrayData = obj.data;
            sortUserChatmsgByDate();

            usersChat.arrayData.forEach(function (element) {
                element['hash'] = md5(element.vorname+element.name+element.id);
            })
        }
    };

    xhrUserData.open('POST', 'api', true);
    xhrUserData.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'entity.load',
            data: {
                entity_name: 'user',
                entity_filter: {id: usersChat.arrayUserIds},
                entity_fields: ['vorname', 'nachname', 'online_status']
            }
        }
    };

    xhrUserData.send("event="+JSON.stringify(data.event));
}

function sortUserChatmsgByDate() {

    xhrUserData=new XMLHttpRequest();

    xhrUserData.onreadystatechange=function()
    {
        if (xhrUserData.readyState===4 && xhrUserData.status===200)
        {
            let response = xhrUserData.responseText;
            const obj = JSON.parse(response);
            let tmpArray = new Array();
            for(let i = 0; i<obj.data.length; i++){
                tmpArray.push(obj.data[i]);
            }
            let sortedArray = sortedUserArrayByOldestMsg(obj.data);
            createChatTabsHtml(createUserListForChatTabs(sortedArray));
            setUserTabNewMessageIcon(tmpArray);
        }
    };

    xhrUserData.open('POST', 'api', true);
    xhrUserData.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'entity.load',
            data: {
                entity_name: 'support_message',
                entity_filter: {absender: usersChat.arrayUserIds, empfaenger:usersChat.chat_type, beantwortet: 0}
            }
        }
    };

    xhrUserData.send("event="+JSON.stringify(data.event));
}

function sortedUserArrayByOldestMsg(data){
    let dataMsg = data,
        tmpArrayDate = [],
        sortedArray = [];

    for(let i = 0; i <usersChat.arrayUserIds.length; i++){
        let tmpArray = [];

        dataMsg.forEach(function(element,x){
            if(usersChat.arrayUserIds[i] === element.absender){
                tmpArray.push(element);
                dataMsg.splice(x,1);
            }
        });

        tmpArray = sortArrayByDate(tmpArray);
        tmpArrayDate.push({'datum':tmpArray[0].datum, 'id':tmpArray[0].absender});

    }
    tmpArrayDate = sortArrayByDate(tmpArrayDate);
    tmpArrayDate.forEach(function(element){
        sortedArray.push(element.id);
    });
    return sortedArray;
}

function createUserListForChatTabs(data){
    let tmpArrayUserData = usersChat.arrayData,
        tmpArraySortedUsers = [];
    data.forEach(function (id) {
        tmpArrayUserData.forEach(function (element, x) {
            if(id === element.id){
                tmpArraySortedUsers.push(element);
                //tmpArrayUserData.splice(x,1);
            }
        });
    });
    return tmpArraySortedUsers;
}

function createChatTabsHtml(data){
    let listContainer = document.getElementById('chat_tabs'),
        html = '',
        activeUser;
    if(activeUser = getUserIdActiveChat()){
        for(let i = 0; i < data.length; i++){
            let online_status = '';
            if(data[i].online_status === '1')
                online_status = 'online';

            if(data[i].id === activeUser){
                html = html + '<li class="js-get-user-chat chat-active '+online_status+'" id="'+data[i].hash+'">'+data[i].vorname+' '+data[i].nachname+'</li>';
            } else {
                html = html + '<li class="js-get-user-chat '+online_status+'" id="'+data[i].hash+'">'+data[i].vorname+' '+data[i].nachname+'</li>';
            }
        }
    } else {
        for(let i = 0; i < data.length; i++){
            let online_status = '';
            if(data[i].online_status === '1')
                online_status = 'online';

            html = html + '<li class="js-get-user-chat '+online_status+'" id="'+data[i].hash+'">'+data[i].vorname+' '+data[i].nachname+'</li>';
        }
    }

    listContainer.innerHTML = html;
    initEventListenerChatTabs();
}

function setChatListelementActive(element){
    element.classList.add('chat-active');
    let listElemente = element.parentElement.getElementsByTagName('li');
    for(let i = 0; i < listElemente.length ; i++){
        if(element !== listElemente[i]){
            listElemente[i].classList.remove('chat-active');
        }
    }

    showChatWindowRD();
}

function getUserIdActiveChat(){
    let activeListElement = document.getElementsByClassName('chat-active')[0];
    if(!!activeListElement){
        let users = usersChat.arrayData;

        let hash= activeListElement.getAttribute('id');

        for(let i = 0; i < users.length; i++) {
            let user = users[i];
            if (user.hash === hash) {
                return user.id;
            }
        }

        return undefined;
    } return undefined;
}

function initEventListenerChatTabs(){
    let elemente = document.getElementsByClassName('js-get-user-chat');
    for(let i = 0; i < elemente.length; i++){
        elemente[i].addEventListener('click', getUserChat);
    }
}

function setUserTabNewMessageIcon(userData) {

    let tmpArray = [];
    userData.forEach(function (element, x) {
        if(element.gelesen === '0'){
            tmpArray.push(element.absender);
        }
    });

    tmpArray = uniqArray(tmpArray);
    tmpArray.forEach(function (element) {
        for(let i = 0 ; i <usersChat.arrayData.length; i++){
            if(element === usersChat.arrayData[i].id){
                let userTabElement = document.getElementById(usersChat.arrayData[i].hash);
                if(!hasClass(userTabElement, 'chat-active')){
                    userTabElement.classList.add('hat-nachricht');
                }
                break;
            }
        }

    });
}


function setChatListelementInactive(){
    let listElemente = document.getElementById('chat_tabs').getElementsByTagName('li');
    for(let i = 0; i < listElemente.length ; i++){
        listElemente[i].classList.remove('chat-active');
    }
    hideChatWindowRD();
}

function hideChatWindowRD(){
    document.getElementById('chat_tabs').parentElement.classList.remove('active');
}

function showChatWindowRD(){
    document.getElementById('chat_tabs').parentElement.classList.add('active');
}
