function Entity_manager(entity, fields, xhr = null)
{
    this.entity = entity;
    this.fields = fields;

    this.xhr = (xhr == null) ? new XHR() : xhr;
}

Entity_manager.prototype.load_by_filter = function(filter, ready_function, optional_val)
{
    _this = this;

    data = {
        event: {
            name: 'entity.load',
            data: {
                entity_name: _this.entity,
                entity_filter: filter
            }
        }
    };
    if(!!this.fields){
        data.event.data.entity_fields = this.fields;
    }

    post_data = "event="+JSON.stringify(data.event);

    this.xhr.add_xhr('post', post_data, ready_function, optional_val);
};

Entity_manager.prototype.save = function(fields, ready_function)
{
    _this = this;

    data = {
        event: {
            name: 'entity.save',
            data: {
                entity_name: _this.entity,
                entity_fields: fields
            }
        }
    };

    post_data = "event="+JSON.stringify(data.event);

    this.xhr.add_xhr('post', post_data, ready_function);
};

Entity_manager.prototype.save_array = function(fields, ready_function)
{
    for(var i = 0; i < fields.length; i++)
    {
        var f = fields[i];
        this.save(f, ready_function)
    }
};


Entity_manager.prototype.update = function(id, fields, ready_function)
{
    _this = this;

    data = {
        event: {
            name: 'entity.update',
            data: {
                entity_name: _this.entity,
                entity_id: id,
                entity_fields: fields
            }
        }
    };

    post_data = "event="+JSON.stringify(data.event);

    this.xhr.add_xhr('post', post_data, ready_function);
};

Entity_manager.prototype.update_array = function(id, fields, ready_function)
{
    for(var i = 0; i < fields.length; i++)
    {
        var f = fields[i];
        this.update(id, f, ready_function)
    }
};

Entity_manager.prototype.delete = function(id, ready_function)
{
    _this = this;

    data = {
        event: {
            name: 'entity.delete',
            data: {
                entity_name: _this.entity,
                entity_id: id
            }
        }
    };

    post_data = "event="+JSON.stringify(data.event);

    this.xhr.add_xhr('post', post_data, ready_function);
};
