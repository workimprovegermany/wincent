$(function() {
    $('.js-iban-format').mask('SS00 0000 0000 0000 0000 00', {
        placeholder: '____ ____ ____ ____ ____ __'
    });


});

function getBoolBinaryForDB(value){
    let bool;
    (value) ? bool =  '1' : bool =  false;
    return bool;
}

function getBoolVariable(value){
    let bool;
    (value === '1') ? bool =  true : bool =  false;
    return bool;
}

/**
 *
 * @returns {string|Date} aktuelles Datum im Format Bsp.: 2019-01-01 00:00:00
 */
function getDateNow() {
    let today = new Date(),
        dd = today.getDate(),
        mm = today.getMonth()+1,
        yyyy = today.getFullYear(),
        time = today.toLocaleTimeString();

    if(dd<10)
    {
        dd='0'+dd;
    }

    if(mm<10)
    {
        mm='0'+mm;
    }
    today = yyyy+'-'+mm+'-'+dd+' '+time;
    return today;
}

/**
 *
 * @param dateData
 * @returns {string|*} Datumsformat Bsp.: 02.04.2019
 */
function get_date_formatted_DD_MM_YYYY_with_point(dateData) {
    let date = new Date(dateData.replace(/-/g, "/")),
        dd = date.getDate(),
        mm = date.getMonth()+1,
        yyyy = date.getFullYear();

    if(dd<10)
    {
        dd='0'+dd;
    }

    if(mm<10)
    {
        mm='0'+mm;
    }
    date = dd+'.'+mm+'.'+yyyy;
    return date;
}


/**
 *
 * @param dateData
 * @returns {string|*} Datumsformat Bsp.: 01. Januar 2019 10:00 Uhr
 */
function getDateFormatted(dateData) {
    let date = new Date(dateData.replace(/-/g, "/")),
        dd = date.getDate(),
        mm = date.getMonth(),
        yyyy = date.getFullYear(),
        mins = date.getMinutes(),
        hours = date.getHours(),
        monthName = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'November', 'Dezember'];

    if(dd<10)
    {
        dd='0'+dd;
    }
    if(mm<10)
    {
        mm='0'+mm;
    }
    if(hours<10)
    {
        hours='0'+hours;
    }
    if(mins<10)
    {
        mins='0'+mins;
    }
    today = dd+'. '+monthName[parseInt(mm)]+' '+yyyy+'  '+hours+':'+mins+' Uhr';
    return today;
}

/**
 *
 * @param dateData
 * @returns {string} Datumsformat Bsp.: 01. Januar 2019
 */
function getDateFormattedDay(dateData) {
    let date = new Date(dateData.replace(/-/g, "/")),
        dd = date.getDate(),
        mm = date.getMonth(),
        yyyy = date.getFullYear(),
        monthName = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'November', 'Dezember'];

    if(dd<10)
    {
        dd='0'+dd;
    }
    if(mm<10)
    {
        mm='0'+mm;
    }

    return dd+'. '+monthName[parseInt(mm)]+' '+yyyy;
}

function getTimestamp(stringValue) {
    let day = parseInt(stringValue.substr(0,2)),
        month = parseInt(stringValue.substr(3,2)),
        year = parseInt(stringValue.substr(6));

    return year+'-'+month+'-'+day+' 00:00:00';
}

function formatThousandPoint(zahl) {
    if(zahl < 1000)
        return zahl;

    let i;
    let j=0,
        ergebnis="";

    zahl = zahl.toString();

    i=zahl.length;
    while (i >= 0) {
        ergebnis=zahl.substr(i,1)+ergebnis;
        j++;
        if (j===4) {
            ergebnis="."+ergebnis;
            j=0;
        }
        i--;
    }
    return ergebnis;
}

function add_leading_null(number){
    (number < 10) ? number = '0'+number : '';
    return number;
}
