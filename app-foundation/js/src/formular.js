function init_settings_account_user(){
    check_daten();
    init_event_listener_password();
    init_event_listener_inputs();
    init_event_listener_save_buttons_account();
    get_number_investors_user();
}

function init_event_listener_save_buttons_account(){

    document.getElementById('form_persoenlich').onsubmit = function(ev){
        ev.preventDefault();
        let email = document.getElementById('email_benutzer').value + '@' + document.getElementById('email_domain').value;
        setButtonStateLoading();

        //DATEN SENDEN
        $.ajax({
            url: 'api',
            type: 'POST',
            data: {
                event: {
                    name: 'entity.update_email',
                    data: {
                        entity_name: 'user',
                        entity_id: app.user_id,
                        entity_fields: {
                            firma: document.getElementById('firma').value,
                            anrede: document.getElementById('anrede').value,
                            vorname: document.getElementById('vorname').value,
                            nachname: document.getElementById('nachname').value,
                            email: email
                        }
                    }
                },
            },

            success:function(response){
                setButtonStateSuccess();
                check_daten();
            },
            error: function (response) {
                console.log(response);
                setButtonStateError();
                check_daten();
            }
        });
    };

    document.getElementById('form_kontakt').onsubmit = function(ev){
        ev.preventDefault();
        setButtonStateLoading();
        //DATEN SENDEN
        $.ajax({
            url: 'api',
            type: 'POST',
            data: {
                event: {
                    name: 'entity.update',
                    data: {
                        entity_name: 'user',
                        entity_id: app.user_id,
                        entity_fields: {
                            strasse: document.getElementById('strasse').value,
                            hausnummer: document.getElementById('hausnr').value,
                            plz: document.getElementById('plz').value,
                            ort: document.getElementById('ort').value,
                            mobil: document.getElementById('mobil').value,
                            telefon: document.getElementById('telefon').value
                        }
                    }
                },
            },
            success: function (response) {
                setButtonStateSuccess();
                check_daten();
            },
            error: function (response) {
                console.log(response);
                setButtonStateError();
                check_daten();
            }
        });
    };

    document.getElementById('form_bank').onsubmit = function(ev){
        ev.preventDefault();
        setButtonStateLoading();
        //DATEN SENDEN
        $.ajax({
            url: 'api',
            type: 'POST',
            data: {
                event: {
                    name: 'entity.update',
                    data: {
                        entity_name: 'user',
                        entity_id: app.user_id,
                        entity_fields: {
                            kontoinhaber: document.getElementById('kontoinhaber').value,
                            bank: document.getElementById('bankname').value,
                            iban: document.getElementById('iban').value.replace(/\s/g, "").toUpperCase(),
                            bic: document.getElementById('bic').value,
                            steuernummer: document.getElementById('steuernr_art').value + ':' + document.getElementById('steuernr').value,
                            steuerpflicht: document.getElementById('steuerpflicht').value
                        }
                    }
                },
            },
            success: function (response) {
                setButtonStateSuccess();
                check_daten();
            },
            error: function (response) {
                console.log(response);
                setButtonStateError();
                check_daten();
            }
        });
    };


    document.getElementById('form_passwort').onsubmit = function(ev){
        ev.preventDefault();
        setButtonStateLoading();
        //DATEN SENDEN
        $.ajax({
            url: 'api',
            type: 'POST',
            data: {
                event: {
                    name: 'entity.update_password',
                    data: {
                        entity_name: 'user_login',
                        entity_id: app.user_id,
                        entity_pass: document.getElementById('oldPassword').value,
                        entity_fields: {
                            upass: document.getElementById('newPassword2').value
                        }
                    }
                },
            },
            success: function (response) {
                console.log(response);
                let element_msg = document.getElementById('message');

                if(response === 'error') {
                    document.getElementById('oldPassword').value = '';
                    element_msg.innerText = 'Ihr altes Passwort ist falsch!';
                    $(element_msg).fadeIn('slow', function () {
                        $(element_msg).delay(3000).fadeOut();
                    });
                    setButtonStateError();
                    check_daten();
                }
                else {
                    setButtonStateSuccess();
                    check_daten();
                }
            },
            error: function (response) {
                console.log(response);
                setButtonStateError();
            }
        });
    };
}

function init_event_listener_password(){
    document.getElementById('js-show-password').addEventListener('click', function (ev) {
        const input_pass = document.getElementById('newPassword'),
            type = input_pass.getAttribute('type');
        if("password" === type){
            input_pass.setAttribute('type', 'text');
        } else {
            input_pass.setAttribute('type', 'password');
        }
    });

    let element_new_pass = document.getElementById('newPassword'),
        element_new_pass_2 = document.getElementById('newPassword2'),
        element_error_pass = document.getElementById('error_pass');

    element_new_pass_2.addEventListener('change',function (ev) {
        if(this.value !== element_new_pass.value){
            element_error_pass.style.display = 'block';
        } else {
            element_error_pass.style.display = 'none';
        }
    });

    element_new_pass.addEventListener('change',function (ev) {
        const valuePass = this.value;
        if('' !== valuePass){
            element_new_pass_2.setAttribute('pattern', valuePass);
        } else {
            element_new_pass_2.removeAttribute("pattern");
        }

    });
}

function init_event_listener_inputs(){
    document.getElementById('plz').addEventListener('keyup', function () {
        if(this.value.length > 4)
        {
            $.ajax({
                url: 'form_data.php',
                type: 'POST',
                data: {
                    code: 'plz',
                    plz: this.value
                },
                success:function(response){
                    document.getElementById('ort').value = response;
                }
            });
        }
    });

    document.getElementById('iban').addEventListener('keyup', function () {
        if(this.value.length > 25)
        {
            $.ajax({
                url: 'form_data.php',
                type: 'POST',
                data: {
                    code: 'iban',
                    iban: this.value
                },
                success:function(response){
                    document.getElementById('bic').value = response;
                }
            });
        }
    });
}

function check_daten(){
    xhrCheck=new XMLHttpRequest();

    xhrCheck.onreadystatechange=function()
    {
        if (xhrCheck.readyState===4 && xhrCheck.status===200)
        {
            let response = xhrCheck.responseText;
            const obj = JSON.parse(response);
            if(!!obj.data){
                if(obj.data.message === 'empty'){ //
                    add_attribute(document.getElementsByTagName('a'),'disabled','disabled');
                    document.getElementsByTagName('nav')[0].setAttribute('disabled','disabled');
                    document.getElementById('icon_logout').removeAttribute('disabled');
                    document.getElementById('btn-vertrag').style.display='none';
                }

            } else {
                if(document.getElementsByTagName('nav')[0].getAttribute('disabled')){
                    add_attribute(document.getElementsByTagName('a'),'disabled',false);
                    document.getElementsByTagName('nav')[0].removeAttribute('disabled');
                    document.getElementById('btn-vertrag').style.display='block';
                }
            }

        }
    };

    xhrCheck.open('POST', 'api', true);
    xhrCheck.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'user.check_daten',
            data: {
                entity_fields: {
                    vorname: document.getElementById("vorname").value,
                    nachname: document.getElementById("nachname").value,
                    email_benutzer: document.getElementById("email_benutzer").value,
                    email_domain: document.getElementById("email_domain").value,
                    strasse: document.getElementById("strasse").value,
                    hausnr: document.getElementById("hausnr").value,
                    plz: document.getElementById("plz").value,
                    ort: document.getElementById("ort").value,
                    kontoinhaber: document.getElementById("kontoinhaber").value,
                    iban: document.getElementById("iban").value,
                    bic: document.getElementById("bic").value,
                    steuernr: document.getElementById("steuernr").value,
                }
            }
        }
    };

    xhrCheck.send("event="+JSON.stringify(data.event));
}

function get_number_investors_user(){
    em = new Entity_manager('user');
    entity_filter = {id: app.user_id};
    em.load_by_filter(entity_filter, get_skynet_id);

    function get_skynet_id(data){
        em = new Entity_manager('skynet_affiliate_conversions');
        entity_filter = {affiliate: data[0].skynet_id};
        em.load_by_filter(entity_filter, set_skynet_investment_ids);
    }

    function set_skynet_investment_ids(data){
        let array_investment_ids = [];
        if(data.length > 0){
            for(let i = 0; i < data.length; i++){
                array_investment_ids.push(data[i].investment_id)
            }
        }
        get_number_of_investors_user(array_investment_ids);
    }

    function get_number_of_investors_user(array_investment_ids){
        entity_fields = ['sum'];
        em = new Entity_manager('skynet_investments', entity_fields);
        entity_filter = {id: array_investment_ids};
        em.load_by_filter(entity_filter, set_number_of_investors_user);
    }
    
    function set_number_of_investors_user(data) {
        document.getElementById('number_investors').innerText = formatThousandPoint(data.length);
        get_number_affiliates_user();
    }
}



function get_number_affiliates_user() {
    em = new Entity_manager('user');
    entity_filter = {id: app.user_id};
    em.load_by_filter(entity_filter, get_affiliate_id);


    function get_affiliate_id(data){
        em = new Entity_manager('user');
        entity_filter = {affiliate_short_id: data[0].short_id};
        em.load_by_filter(entity_filter, set_number_of_affiliates_user);
    }

    function set_number_of_affiliates_user(data) {
        document.getElementById('number_affiliates').innerText = formatThousandPoint(data.length);

        if(data.length === 0) {
            document.getElementById('info-affiliates').style.display='none';
        }
    }

}