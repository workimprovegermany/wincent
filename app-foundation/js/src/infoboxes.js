function Infobalken(){
    this.arrayDataInfoboxes = [];
}

var infobalken = new Infobalken();


function initInfobalkenIFrame(){
    if(typeof app === "undefined"){
        app = new App();
    }

    loadData();

    function loadData(){
        em = new Entity_manager('infoboxes');
        entity_filter = {datum_bis: '>='+getDateNow(), datum_ab: '<='+getDateNow()};
        em.load_by_filter(entity_filter, laodDataUserInfoRead);
    }

    function laodDataUserInfoRead(data){
        let tmpArray = sortArray(data),
            user_id = app.user_id;

        infobalken.arrayDataInfoboxes = [];
        for(let i = 0; i < tmpArray.length; i++){
            infobalken.arrayDataInfoboxes.push(tmpArray[i]);
        }

        em = new Entity_manager('infoboxes_user');
        entity_filter = {user_id: user_id};
        em.load_by_filter(entity_filter, deleteReadMessagesFromArray);
    }

    function deleteReadMessagesFromArray(data){
        for(let i = 0; i < data.length; i++){
            let box_id = data[i].box_id;
            for(let x = 0; x < infobalken.arrayDataInfoboxes.length; x++){
                if(infobalken.arrayDataInfoboxes[x].id === box_id){
                    infobalken.arrayDataInfoboxes.splice(x,1);
                }
            }
        }
        createInfoboxes(infobalken.arrayDataInfoboxes);
    }

    function sortArray(data) {
        let newArray =[];
        newArray = data.sort(function (a,b) {
            if(a.art < b.art) { return -1; }
            if(a.art > b.art) { return 1; }
            return 0;
        });
        return newArray;
    }


    function createInfoboxes(data) {
        document.getElementById('container-infoboxen').innerHTML = '';
        if(data.length === 0)
            return false;

        let array = createInfoboxArray(data),
            html = '';
        for(let i = 0; i < data.length; i++){
            html = html + array[i];
        }
        document.getElementById('container-infoboxen').innerHTML = html;
        initEventlistenerOnClose();
        let height = 0;
        if(data.length > 0){
            height = $(document).height();
        }
        window.parent.postMessage({"height": height}, "*");

    }

    function createInfoboxArray(data){
        let array = [];
        for(let i = 0; i < data.length; i++){
            let cssClass = '',
                cssIcon = '';
            switch (data[i].art){
                case 'warnung':
                    cssClass = 'infobox_user_warning';
                    cssIcon = 'fa-times';
                    break;

                case 'hinweis':
                    cssClass = 'infobox_user_note';
                    cssIcon = 'fa-exclamation';
                    break;

                case 'info':
                    cssClass = 'infobox_user_info';
                    cssIcon = 'fa-info';
                    break;
            }

            let html = '<div class="'+cssClass+'">\n' +
                '        <i class="fas '+cssIcon+'"></i>\n' +
                '        <p>'+data[i].text+'</p>\n' +
                '        <div class="close js-close-infobox" box-id="'+data[i].id+'"><i class="fas fa-times"></i></div>\n' +
                '    </div>';

            array.push(html);
        }
        return array;
    }

    function initEventlistenerOnClose(){
        let elements = document.getElementsByClassName('js-close-infobox');
        for(let i = 0; i < elements.length; i++){
            elements[i].addEventListener('click', function (ev) {
                saveData(this.getAttribute('box-id'));
            })
        }
    }

    function saveData(boxId){
        let user_id = app.user_id;
        em = new Entity_manager('infoboxes_user');
        entity_fields = {datum_gelesen: getDateNow(), box_id: boxId, user_id: user_id};
        em.save(entity_fields, loadData);
    }
}
