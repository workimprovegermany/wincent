$( function() {
    $( "#tabs" ).tabs();

    $( ".tooltip" ).tooltip({
        show: null,
        position: {
            my: "left top",
            at: "left bottom"
        },
        open: function( event, ui ) {
            ui.tooltip.animate({ top: ui.tooltip.position().top + 10 }, "fast" );
        }
    });

    $( ".tooltip" ).on('click', function (ev) {
        ev.preventDefault();
    });

    $( ".js-sortable-list" ).sortable();

    $( ".js-datepicker" ).datepicker({
        firstDay: 1,
        dateFormat: "dd.mm.yy",
        dayNames: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag"],
        dayNamesMin: [ "So","Mo", "Di", "Mi", "Do", "Fr", "Sa"],
        changeMonth: true,
        changeYear: true,
        minDate: new Date(),
        monthNames: [ "Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember" ],
        monthNamesShort: [ "Jan", "Feb", "März", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez" ]
    });

    $('.counter').each(function() {
        var $this = $(this),
            countTo = $this.attr('data-count');

        $({ countNum: $this.text()}).animate({
                countNum: countTo
            },

            {

                duration: 1500,
                easing:'linear',
                step: function() {
                    $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                    $this.text(formatThousandPoint(this.countNum));
                }

            }
        );
    });

});