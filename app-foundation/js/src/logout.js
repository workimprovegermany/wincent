$(function() {
    $('#icon_logout').on('click',function (ev) {
        ev.preventDefault();

        //DATEN SENDEN
        $.ajax({
            url: 'api',
            type: 'POST',
            data: {
                event: {
                    name: 'entity.update',
                    data: {
                        entity_name: 'user_login',
                        entity_id: $('#user_id').val(),
                        entity_fields: {
                            utoken: '',
                        }
                    }
                },
            },
            success:function(response){
                console.log(response);
                set_online_status(false);
                window.location.href = '../';
            },
            error: function (response) {
                console.log(response);
            }
        });

    });

});