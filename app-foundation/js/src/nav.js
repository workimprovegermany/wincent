function init_navi() {
    if(app.user_can('DO_EVERYTHING') || app.user_can('DO_SUPPORT_SALES')){
        let link = document.getElementsByTagName('nav')[0].getElementsByClassName('support')[0].getElementsByTagName('a')[0],
            href = link.href;
        href = href.replace('support.html', 'support-admin-menu.html');
        link.href = href;
    }
}