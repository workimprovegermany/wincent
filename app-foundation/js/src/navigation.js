$(function() {
    let naviElement = document.getElementById('navi-bar');
    if(naviElement !== null){
        naviElement.addEventListener('click', function (ev) {
            $(ev.target).toggleClass('active')
        });
    }
});