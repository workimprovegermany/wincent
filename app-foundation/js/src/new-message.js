function Timer(){
    this.timeout = '';
}

var timer = new Timer();
function checkNewMessageUser(user_id){
    clearTimeout(timer.timeout);
    xhrNewMsg=new XMLHttpRequest();

    xhrNewMsg.onreadystatechange=function()
    {
        if (xhrNewMsg.readyState===4 && xhrNewMsg.status===200)
        {
            let response = xhrNewMsg.responseText;
            const obj = JSON.parse(response);
            if(obj.data.length > 0){
                setNewMessageUser(user_id);
            } else {
                remove_icon_has_message();
            }
        }
    };

    xhrNewMsg.open('POST', 'api', true);
    xhrNewMsg.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    if(app.user_can('DO_EVERYTHING')){
        user_id = ['support', 'vertrieb'];
    }
    data = {
        event: {
            name: 'entity.load',
            data: {
                entity_name: 'support_message',
                entity_filter: {empfaenger: user_id, gelesen: 0}
            }
        }
    };

    xhrNewMsg.send("event="+JSON.stringify(data.event));

   timer.timeout = setTimeout(function(){checkNewMessageUser(user_id);},10000);

}

function setNewMessageUser(user_id) {
    let chat_fenster = document.getElementById('support_chat_fenster');
    if(chat_fenster){
        if(app.user_can('DO_EVERYTHING')){
            add_icon_has_message();
            load_data_support_admin();
        } else {
            loadDataSupport();

        }

    } else {
        add_icon_has_message();
    }
}

function remove_icon_has_message() {
    let element = document.getElementById('msg-header');
    if(element){
        element.classList.remove('hat-nachricht');
    }
}

function add_icon_has_message() {
    let element = document.getElementById('msg-header');
    if(element){
        element.classList.add('hat-nachricht');
    }
}
