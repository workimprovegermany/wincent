function Online_Status(){
    this.timer = '';
    this.status = null;
}

const online_status = new Online_Status();

function set_online_status(status){

    if(typeof app === "undefined")
        return false;
    xhrStatus=new XMLHttpRequest();

    xhrStatus.open('POST', 'api', true);
    xhrStatus.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'entity.update',
            data: {
                entity_name: 'user',
                entity_id: app.user_id,
                entity_fields: {
                    online_status: status
                }
            }
        }
    };

    xhrStatus.send("event="+JSON.stringify(data.event));
    online_status.status = status;
}

window.onbeforeunload = function(event) {
    if(typeof app !== "undefined"){
        set_online_status(false);
    }
};

function init_timer_online_status() {
    clearTimeout(online_status.timer);

    online_status.timer = setTimeout(function () {
        set_online_status(false);
        clearTimeout(online_status.timer);
    }, 300000)
}
add_event_listener_online_status();

function add_event_listener_online_status(){
    window.addEventListener('click', function () {
        init_timer_online_status();
        if(!online_status.status){
            set_online_status(true);
        }
    });

    window.addEventListener('change', function () {
        init_timer_online_status();
        if(!online_status.status){
            set_online_status(true);
        }
    });
}