function show_overlay(){
    document.getElementById('overlay').style.display = 'block';
    document.getElementById('js-close-overlay').addEventListener('click', function (ev) {
        hide_overlay();
    });
}

function hide_overlay(){
    document.getElementById('overlay').style.display = 'none';
}