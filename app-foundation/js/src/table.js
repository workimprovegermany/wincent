function createTable(dataHeader, dataTable, containerId, css){
    if(!css)
        css = '';
    let table = '<table class="'+css+'">';
    table = table + createTableHeader(table, dataHeader);
    table = table + createTableBody(dataHeader, dataTable);
    table = table + '</table>';

    document.getElementById(containerId).innerHTML = table;
    if(hasClass(document.getElementsByTagName('table')[0], 'responsive'))
        add_table_responsive_style(dataHeader);
}

function createTableHeader(html, dataHeader){
    let tableHead = '<thead><tr>';
    for(let i = 0; i < dataHeader.length; i++){
        tableHead = tableHead+'<th>'+dataHeader[i]+'</th>';
    }

    tableHead = tableHead + '</tr></thead>';
    return tableHead;
}

function createTableBody(dataHeader, dataTable){
    let tableBody = '';
    if(dataTable.length > 0){
        for(let i = 0; i < dataTable.length; i++){
            let dataArray = dataTable[i];
            tableBody = tableBody + '<tr>';
            for(let x in dataArray){
                tableBody = tableBody + '<td>'+dataArray[x]+'</td>';
            }
            tableBody = tableBody + '</tr>';
        }


    } else {
        tableBody = tableBody + '<tr><td class="center" colspan="'+ dataHeader.length+'">Keine Daten gefunden</td></tr>';
    }
    return tableBody;
}

/**
 * Für die normale responsive Tabelle css-Klasse 'responsive' zum table-Tag hinzufügen.
 * Soll die Tablle als Accoridon dargestellt werden die Klasse 'resp_as_accordion' ergänzen.
 * @param data_header
 * @returns {boolean}
 */
function add_table_responsive_style(data_header){

    let style = '@media only screen and (max-width: 979px){';
    for(let i = 0; i < data_header.length; i++){
        style += 'table.responsive td:nth-of-type('+(i+1)+'):before { content: "'+data_header[i]+'"; }';
    }
    style += '}';

    let head = document.head || document.getElementsByTagName('head')[0],
        style_tag = document.createElement('style');

    head.appendChild(style_tag);

    style_tag.type = 'text/css';
    if (style_tag.styleSheet){
        // This is required for IE8 and below.
        style_tag.styleSheet.cssText = style;
    } else {
        style_tag.appendChild(document.createTextNode(style));
    }

    let tables = document.getElementsByTagName('table');
    for(let i = 0; i < tables.length; i++){
        if(hasClass(tables[i], 'resp_as_accordion'))
            add_event_listener_resp_table(tables[i]);
    }
}

function add_event_listener_resp_table(table) {

    let elemente = table.getElementsByTagName('tr');
    for(let i = 0; i < elemente.length; i++){
        elemente[i].firstElementChild.addEventListener('click', function () {
            toggle_Class(this, 'active');
        });
    }
}