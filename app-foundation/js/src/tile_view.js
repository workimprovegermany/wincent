function Tile_view()
{
    var id = '';
    var data = [];
    var grid_id = '';
    var pagination_id = '';
    var entity = '';
}

Tile_view.prototype.init = function(el)
{
    console.log('### INIT TILE_VIEW');

    var id = el.id;
    this.id = id;
    this.grid_id = '_af_grid_' + this.id;
    this.pagination_id = '_af_pagination_' + this.id;
    this.entity = el.getAttribute('entity');

    let str = "<section class=\"container-fluid well\">\n" +
        "            <div class=\"grid_12 bg-weiss flex\" id=\""+this.grid_id+"\">\n" +
        "            </div>\n" +
        "        </section>\n" +
        "\n" +
        "        <section class=\"container-fluid well well9\">\n" +
        "            <div class=\"grid_12 bg-weiss center navi-nr\" id=\""+this.pagination_id+"\">\n" +
        "            </div>\n" +
        "        </section>\n";

    el.innerHTML = str;
};

Tile_view.prototype.load_data = function(page, event = null)
{
    var self = this;

    if(event != null)
    {
        event.preventDefault();
    }

    if (window.XMLHttpRequest)
    {
        // AJAX nutzen mit IE7+, Chrome, Firefox, Safari, Opera
        xhr=new XMLHttpRequest();
    }
    else
    {
        // AJAX mit IE6, IE5
        xhr=new ActiveXObject("Microsoft.XMLHTTP");
    }

    xhr.onreadystatechange=function()
    {
        if (xhr.readyState==4 && xhr.status==200)
        {
            var response = xhr.responseText;
            console.log(response);
            self.data = JSON.parse(response);
            self.render();
        }
    };

    xhr.open('POST', '../api', true);

    var form_data = new FormData();

    if (this.entity == 'webinar_feedback')
    {
        form_data.append('af_cmd', 'get_latest_webinarfeedback');
        console.log('webinarfeedback');
    }
    else
    {
        form_data.append('af_cmd', 'get_latest_webinars');
        console.log('webinars');

    }

    form_data.append('seite', page);

    if (this.entity === 'webinar_feedback')
    {
        form_data.append('webinar_id', document.querySelectorAll('input[name="videoid"]')[0].value);
    }
    else
    {
        form_data.append('kat', document.querySelectorAll('select[name="kategorie"]')[0].value);
        form_data.append('such', document.querySelectorAll('input[name="suche"]')[0].value);
    }

    xhr.send(form_data);
};

Tile_view.prototype.render = function()
{
    if (this.entity == 'webinar_feedback')
    {
        this.render_data_feedback(this.data[0]);
    }
    else
    {
        this.render_data(this.data[0]);
    }
    this.render_pagination(this.data[1])
};

Tile_view.prototype.render_data = function(data)
{
    var html = "";
    var grid_el = document.getElementById(this.grid_id);

    var tile_template = `
        <a href="%%LINK%%" title="%%LINKTITLE%%" class="grid_4 webinar tile01">
            <div>
                <div class="banner-container" [af_hide_if_empty]>
                    <div class="banner-content">%%TEXTBANNER%%</div>
                </div>
                <img src="%%IMG%%" class="webinar-vorschau">
                <div style="background-image: url(%%IMG_CORNER%%);" class="webinar-referent"></div>
                <div class="termin">
                    <p>%%IMG_COMMENT%%</p>
                </div>
                <div class="beschreibung">
                    <h3>%%TITLE%%</h3>
                    <p>%%DESCRIPTION%%</p>
                    %%RATING%%
                </div>
            </div>
        </a>
    `;

    if(data.length != 0){

        for(var i = 0; i < data.length; i++){
            var element = data[i];

            var urlBild = element.bild,
                urlReferent = element.referent;

            if(urlBild == ''){
                urlBild = 'images/webinar-vorschau-platzhalter.jpg';
            }
            if(urlReferent == ''){
                urlReferent = 'images/webinar-referent-platzhalter.jpg';
            }

            var htmlElement = tile_template;
            htmlElement = htmlElement.replace("%%LINK%%", element.link);
            htmlElement = htmlElement.replace("%%LINKTITLE%%", "Webinaranmeldung " + element.titel);
            htmlElement = htmlElement.replace("%%IMG%%", urlBild);
            htmlElement = htmlElement.replace("%%IMG_CORNER%%", urlReferent);
            htmlElement = htmlElement.replace("%%IMG_COMMENT%%", element.termin +" | "+ element.uhr);
            htmlElement = htmlElement.replace("%%TITLE%%", element.titel);
            htmlElement = htmlElement.replace("%%DESCRIPTION%%", element.beschreibung);
            htmlElement = htmlElement.replace("%%RATING%%", this.rating_svg(element.rating, element.bewertungen));


            htmlElement = htmlElement.replace("%%TEXTBANNER%%", element.textbanner);

            html = html + htmlElement;
        }

    } else {
        html = "<div class='grid_12 center'><h3>Es wurden keine Webinare gefunden!</h3></div>"
    }

    var dummy = document.createElement('div');
    dummy.innerHTML = html;

    he = dummy.querySelectorAll("[\\[af_hide_if_empty\\]]");
    for (i = 0; i < he.length; i++)
    {
        if (he[i].textContent.trim() === '')
        {
            he[i].setAttribute('hidden','');
        }
    }

    var cn = dummy.childNodes;
    while (grid_el.firstChild)
    {
        grid_el.firstChild.remove(); // faster than grid_el.removeChild(grid_el.firstChild)
    }
    for (i = 0; i < cn.length; i++)
    {
        grid_el.appendChild(cn[i].cloneNode(true));
        console.log(grid_el);
    }
};

Tile_view.prototype.render_data_feedback = function(data)
{
    var html = "";
    var grid_el = document.getElementById(this.grid_id);

    var tile_template = `
        <div class="grid_4">
                    <div class="box_feedback">
                        <img src="%%ICON%%" alt="Imparare ist einzigartig" class="well8">
                        <p><strong>%%ANREDE%% %%NACHNAMEKURZ%%.</strong></p>
                        <p>Feedback vom %%DATUM%%</p>
                        <hr>

                        <div class="rating_fix" id="two_two">
                            <span data-nr="5" id="5_two" class="%%filled_5%%">☆</span>
                            <span data-nr="4" id="4_two" class="%%filled_4%%">☆</span>
                            <span data-nr="3" id="3_two" class="%%filled_3%%">☆</span>
                            <span data-nr="2" id="2_two" class="%%filled_2%%">☆</span>
                            <span data-nr="1" id="1_two" class="%%filled_1%%">☆</span>
                        </div>
                        <p>%%FEEDBACKTEXT%%</p>
                    </div>
                </div>
    `;

    if(data.length != 0){

        for(var i = 0; i < data.length; i++){
            var element = data[i];

            var htmlElement = tile_template;
            htmlElement = htmlElement.replace("%%ANREDE%%", element.anrede);
            htmlElement = htmlElement.replace("%%NACHNAMEKURZ%%", element.nachnameKurz);
            htmlElement = htmlElement.replace("%%DATUM%%", element.datum);
            htmlElement = htmlElement.replace("%%FEEDBACKTEXT%%", element.feedbacktext);
            htmlElement = htmlElement.replace("%%filled_1%%", element.filled_1);
            htmlElement = htmlElement.replace("%%filled_2%%", element.filled_2);
            htmlElement = htmlElement.replace("%%filled_3%%", element.filled_3);
            htmlElement = htmlElement.replace("%%filled_4%%", element.filled_4);
            htmlElement = htmlElement.replace("%%filled_5%%", element.filled_5);

            if(element.anrede === "Frau"){
                var iconSrc = "images/icons/icon-frau-blau.svg";
            } else {
                var iconSrc = "images/icons/icon-mann-blau.svg";
            }

            htmlElement = htmlElement.replace("%%ICON%%", iconSrc);

            html = html + htmlElement;
        }

    } else {
        html = "<div class='grid_12 center'><h3>Es wurde kein Webinarfeedback gefunden!</h3></div>"
    }

    var dummy = document.createElement('div');
    dummy.innerHTML = html;

    he = dummy.querySelectorAll("[\\[af_hide_if_empty\\]]");
    for (i = 0; i < he.length; i++)
    {
        if (he[i].textContent.trim() === '')
        {
            he[i].setAttribute('hidden','');
        }
    }

    var cn = dummy.childNodes;
    while (grid_el.firstChild)
    {
        grid_el.firstChild.remove(); // faster than grid_el.removeChild(grid_el.firstChild)
    }
    for (i = 0; i < cn.length; i++)
    {
        grid_el.appendChild(cn[i].cloneNode(true));
        console.log(grid_el);
    }
};

Tile_view.prototype.render_pagination = function (data)
{
    var pagination_el = document.getElementById(this.pagination_id);

    var aktSeite = parseInt(data.aktSeite),
        gesSeiten = parseInt(data.gesSeiten),
        links = "";

    if (gesSeiten === 0 ) return;

    // arrow left
    links = links + '<a href="" onclick="_af_grids[0].load_data(1, event)"><i class="fa fa-angle-double-left"></i></a>';

    // five pages
    var start = Math.max(aktSeite - 2, 1);
    var ende = Math.min(start + 4, gesSeiten);

    for(i = start; i <= ende; i++){
        if(i === aktSeite){
            links = links + '<a href="" class="active" onclick="_af_grids[0].load_data('+ i +', event)">'+ i +'</a>';
        } else {
            links = links + '<a href="" onclick="_af_grids[0].load_data('+ i +', event)">'+ i +'</a>';
        }
    }

    // arrow right
    links = links + '<a href="" onclick="_af_grids[0].load_data('+gesSeiten+', event)"><i class="fa fa-angle-double-right"></i></a>';

    //$('#navi_pages').html(links);
    pagination_el.innerHTML = links;
};

Tile_view.prototype.rating_svg = function(stars, user)
{
    var svg = "";
    switch(stars){
        case 0:
            break;

        case 1:
            svg = '<svg fill="#4989e1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 96 96" enable-background="new 0 0 96 96" xml:space="preserve"><path d="M88,46.201h-6.121l-1.89-5.824l-1.894,5.824h-6.089h-0.033h-6.088l-1.892-5.823l-1.893,5.823h-6.088h-0.032h-6.088  l-1.893-5.823l-1.892,5.823h-6.087h-0.036h-6.087l-1.892-5.823l-1.892,5.823h-6.087h-0.036h-6.086l-1.893-5.823l-1.892,5.823H8  l4.953,3.598l-1.891,5.823l4.952-3.599l4.953,3.599l-1.892-5.823l4.936-3.585l4.935,3.585l-1.892,5.823l4.953-3.599l4.954,3.599  l-1.893-5.823l4.936-3.585l4.935,3.585l-1.891,5.823l4.952-3.599l4.954,3.599l-1.893-5.823l4.936-3.586l4.935,3.586l-1.89,5.823  l4.951-3.599l4.953,3.599l-1.892-5.823l4.935-3.586l4.937,3.586l-1.891,5.823l4.953-3.599l4.951,3.6l-1.892-5.824L88,46.201z   M33.937,49.432l1.193,3.67l-3.122-2.269l-3.121,2.269l1.192-3.67l-3.121-2.268h3.857l1.192-3.67l1.192,3.67h3.858L33.937,49.432z   M49.931,49.432l1.192,3.67l-3.122-2.269l-3.121,2.269l1.192-3.67l-3.121-2.268h3.857l1.192-3.671l1.193,3.671h3.857L49.931,49.432z   M65.924,49.432l1.191,3.67l-3.121-2.269l-3.121,2.269l1.191-3.67l-3.121-2.268h3.858l1.192-3.67l1.192,3.67h3.858L65.924,49.432z   M83.108,53.101l-3.119-2.268l-3.123,2.269l1.192-3.67l-3.121-2.268h3.858l1.193-3.67l1.19,3.67h3.858l-3.121,2.268L83.108,53.101z"></path></svg>';
            break;

        case 2:
            svg = '<svg fill="#4989e1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 96 96" enable-background="new 0 0 96 96" xml:space="preserve"><path d="M88,46.201h-6.121l-1.89-5.824l-1.894,5.824h-6.089h-0.033h-6.088l-1.892-5.823l-1.893,5.823h-6.088h-0.032h-6.088  l-1.893-5.823l-1.892,5.823h-6.087h-0.036h-6.087l-1.892-5.823l-1.892,5.823h-6.087h-0.036h-6.086l-1.893-5.823l-1.892,5.823H8  l4.953,3.598l-1.891,5.823l4.952-3.599l4.953,3.599l-1.892-5.823l4.936-3.585l4.935,3.585l-1.892,5.823l4.953-3.599l4.954,3.599  l-1.893-5.823l4.936-3.585l4.935,3.585l-1.891,5.823l4.952-3.599l4.954,3.599l-1.893-5.823l4.936-3.586l4.935,3.586l-1.89,5.823  l4.951-3.599l4.953,3.599l-1.892-5.823l4.935-3.586l4.937,3.586l-1.891,5.823l4.953-3.599l4.951,3.6l-1.892-5.824L88,46.201z   M49.931,49.432l1.192,3.67l-3.122-2.269l-3.121,2.269l1.192-3.67l-3.121-2.268h3.857l1.192-3.671l1.193,3.671h3.857L49.931,49.432z   M65.924,49.432l1.191,3.67l-3.121-2.269l-3.121,2.269l1.191-3.67l-3.121-2.268h3.858l1.192-3.67l1.192,3.67h3.858L65.924,49.432z   M83.108,53.101l-3.119-2.268l-3.123,2.269l1.192-3.67l-3.121-2.268h3.858l1.193-3.67l1.19,3.67h3.858l-3.121,2.268L83.108,53.101z"></path></svg>';
            break;

        case 3:
            svg = '<svg fill="#4989e1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 96 96" enable-background="new 0 0 96 96" xml:space="preserve"><path d="M88,46.201h-6.121l-1.89-5.824l-1.894,5.824h-6.089h-0.033h-6.088l-1.892-5.823l-1.893,5.823h-6.088h-0.032h-6.088  l-1.893-5.823l-1.892,5.823h-6.087h-0.036h-6.087l-1.892-5.823l-1.892,5.823h-6.087h-0.036h-6.086l-1.893-5.823l-1.892,5.823H8  l4.953,3.598l-1.891,5.823l4.952-3.599l4.953,3.599l-1.892-5.823l4.936-3.585l4.935,3.585l-1.892,5.823l4.953-3.599l4.954,3.599  l-1.893-5.823l4.936-3.585l4.935,3.585l-1.891,5.823l4.952-3.599l4.954,3.599l-1.893-5.823l4.936-3.586l4.935,3.586l-1.89,5.823  l4.951-3.599l4.953,3.599l-1.892-5.823l4.935-3.586l4.937,3.586l-1.891,5.823l4.953-3.599l4.951,3.6l-1.892-5.824L88,46.201z   M65.924,49.432l1.191,3.67l-3.121-2.269l-3.121,2.269l1.191-3.67l-3.121-2.268h3.858l1.192-3.67l1.192,3.67h3.858L65.924,49.432z   M83.108,53.101l-3.119-2.268l-3.123,2.269l1.192-3.67l-3.121-2.268h3.858l1.193-3.67l1.19,3.67h3.858l-3.121,2.268L83.108,53.101z"></path></svg>';
            break;

        case 4:
            svg = '<svg fill="#4989e1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 96 96" enable-background="new 0 0 96 96" xml:space="preserve"><path d="M88,46.201h-6.121l-1.89-5.824l-1.894,5.824h-6.089h-0.033h-6.088l-1.892-5.823l-1.893,5.823h-6.088h-0.032h-6.088  l-1.893-5.823l-1.892,5.823h-6.087h-0.036h-6.087l-1.892-5.823l-1.892,5.823h-6.087h-0.036h-6.086l-1.893-5.823l-1.892,5.823H8  l4.953,3.598l-1.891,5.823l4.952-3.599l4.953,3.599l-1.892-5.823l4.936-3.585l4.935,3.585l-1.892,5.823l4.953-3.599l4.954,3.599  l-1.893-5.823l4.936-3.585l4.935,3.585l-1.891,5.823l4.952-3.599l4.954,3.599l-1.893-5.823l4.936-3.586l4.935,3.586l-1.89,5.823  l4.951-3.599l4.953,3.599l-1.892-5.823l4.935-3.586l4.937,3.586l-1.891,5.823l4.953-3.599l4.951,3.6l-1.892-5.824L88,46.201z   M83.108,53.101l-3.119-2.268l-3.123,2.269l1.192-3.67l-3.121-2.268h3.858l1.193-3.67l1.19,3.67h3.858l-3.121,2.268L83.108,53.101z"></path></svg>';
            break;

        case 5:
            svg = '<svg fill="#4989e1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 96 96" enable-background="new 0 0 96 96" xml:space="preserve"><polygon points="88,46.201 81.879,46.201 79.989,40.377 78.096,46.201 72.007,46.201 71.974,46.201 65.886,46.201 63.994,40.378   62.102,46.201 56.014,46.201 55.981,46.201 49.894,46.201 48.001,40.378 46.109,46.201 40.022,46.201 39.986,46.201 33.899,46.201   32.008,40.378 30.116,46.201 24.029,46.201 23.993,46.201 17.907,46.201 16.015,40.378 14.123,46.201 8,46.201 12.953,49.799   11.063,55.622 16.015,52.023 20.968,55.622 19.076,49.799 24.012,46.214 28.946,49.799 27.055,55.622 32.008,52.023 36.962,55.622   35.069,49.799 40.005,46.214 44.939,49.799 43.049,55.622 48.001,52.023 52.955,55.622 51.063,49.799 55.998,46.213 60.933,49.799   59.043,55.622 63.994,52.023 68.947,55.622 67.056,49.799 71.99,46.213 76.927,49.799 75.036,55.622 79.989,52.023 84.94,55.623   83.049,49.799 "></polygon></svg>';
            break;

        default:
            break;
    }

    if(svg != ""){
        svg = svg + "<span>("+user+")</span>";
    }

    return svg;
};
