$(function() {
    $('.js-toggle-element').on('click',function (ev) {
        const dataNr = $(this).data('nr');
        if(!$(this).hasClass('active')){
            $('.js-toggle-element').removeClass('active');
            $(this).addClass('active');
            toggleElement(dataNr);
        }

    });

    function toggleElement(dataNr) {
        const elemente = $('.toggle-elemente'),
            nextActiveElement = elemente.find('.toggle-element-'+dataNr);

        elemente.find('[class*="toggle-element-"]').removeClass('active');
        nextActiveElement.addClass('active');

    }
});