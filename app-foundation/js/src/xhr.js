function XHR()
{
    this.url = 'api';

    this.xmlHR = new XMLHttpRequest();
    this.xhr_queue = [];
    this.xhr_busy = false;

    this.ready_func = null;
    this.optional_val = null;
}

XHR.prototype.post = function(data, ready_func, optional_val)
{
    console.log('POST()');

    this.ready_func = ready_func;
    this.optional_val = optional_val;

    _this = this;


    this.xmlHR.onreadystatechange = function()
    {
        if (_this.xmlHR.readyState===4 && _this.xmlHR.status===200)
        {
            if(typeof(_this.ready_func) === 'function')
            {
                try {
                    const obj = JSON.parse(_this.xmlHR.responseText);
                    _this.ready_func(obj.data, _this.optional_val);

                    //next worker
                    _this.xhr_busy = false;
                    console.log('NOT busy');
                    _this.start_xhr_worker();
                }
                catch (e) {
                    //TODO: Was passiert bei Fehler?
                    console.log('Fehler try catch xhr.js :'+ e);
                }
            }
        }
    };

    this.xmlHR.open('POST', this.url, true);
    this.xmlHR.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    this.xmlHR.send(data);
};


XHR.prototype.add_xhr = function(type, data, ready_func, ready_func_param = null)
{
    request = {
        type: type,
        data: data,
        ready_func: ready_func,
        ready_func_param: ready_func_param
    };

    this.xhr_queue.push(request);
    this.start_xhr_worker();
};

XHR.prototype.start_xhr_worker = function()
{
    if (this.xhr_busy == true || this.xhr_queue.length === 0) return;

    console.log('start new worker');

    this.xhr_busy = true;
    request = this.xhr_queue.shift();

    console.log(request);

    this.post(request.data, request.ready_func, request.ready_func_param);
};
