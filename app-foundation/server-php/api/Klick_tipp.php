<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 10.11.17
 * Time: 10:37
 */

class Klick_tipp
{
    private $kt_base_url;
    private $kt_api_key;
    private $kt_timeout;

    private $kt_list_id;

    public function __construct($config)
    {
        //TODO: test if $config contains valid values
        $this->kt_base_url = $config['kt_base_url'];
        $this->kt_api_key = $config['kt_api_key'];
        $this->kt_timeout = $config['kt_timeout'] / 1000;

        $this->kt_list_id = $this->get_first_list_id();
    }

    public function set_list_id($id)
    {
        $this->sib_list_id = $id;
    }

    public function create_update_user($teilnehmer, $export_config)
    {
        $export_fields = array(
            "vorname" => "Vorname",
            "name" => "Name",
            "email" => "E-Mail",
            "anrede" => "Anrede",
            "ansprache" => "Ansprache",
            "videotitel" => "Videotitel",
            "videountertitel" => "Videountertitel",
            "videoid" => "Webinar ID",
            "videodatum" => "Datum Teilnahme",
            "teilgenommen" => "teilgenommen",
            "strasse" => "Straße",
            "plz" => "PLZ",
            "ort" => "Ort",
        );

        // sollen in db stehen
        $imparare_field_names = array(
            "vorname" => "vorname",
            "name" => "name",
            "email" => "email",
            "anrede" => "anrede",
            "ansprache" => "ansprache",
            "videotitel" => "video_title",
            "videountertitel" => "video_untertitel",
            "videoid" => "video_id",
            "videodatum" => "teilgenommen_datum",
            "teilgenommen" => "teilgenommen",
            "strasse" => "strasse",
            "plz" => "plz",
            "ort" => "ort",
        );

        $required_export_fields = array('vorname', 'name', 'email');

        $export = array();
        foreach ($export_fields as $field => $descr)
        {
            // wenn Pflichtfeld oder Exporthaken gesetzt ist
            if (in_array($field, $required_export_fields) || $export_config[api_table_export_column_name($field)] == 1)
            {
                // übernehmen bedeutet im array zu setzen: mailer_feldname => wert_von(imparare_feldname)
                $export[$export_config[api_table_mailer_column_name($field)]] = $teilnehmer->nice_field($imparare_field_names[$field]);
            }
        }



        require ('klicktipp.api.inc');

        $username = 'username';
        $password = 'password';

        $email_address = 'emailaddress@domain.com';
        $double_optin_process_id = 1;

        $tag_id = 1;
        $fields = array ( // Use field_index to get all custom fields.
            'fieldFirstName' => 'Thomas',
            'fieldLastName' => 'Weber',
        );
        $smsnumber = '0000';

        $connector = new KlicktippConnector();
        $connector->login($username, $password);
        $subscriber = $connector->subscribe(
            $teilnehmer->field('email'),
            $double_optin_process_id,
            $tag_id,
            $export,
            $smsnumber);
        $connector->logout();

        //if ($subscriber) {
        //    print('<pre>'.print_r($subscriber, true).'</pre>');
        //} else {
        //    print $connector->get_last_error();
        //}

        return true;
    }

    public function test()
    {

        
    }

    function get_first_list_id()
    {
        /*$ch = curl_init('https://us16.api.mailchimp.com/3.0/lists');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $auth = 'user:'.$this->kt_api_key;
        curl_setopt($ch, CURLOPT_USERPWD, $auth);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        //for debug
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch);
        curl_close($ch);

        $res_dec = json_decode($result);
        return $res_dec->lists[0]->id;*/
        return 0;
    }
}

function api_table_export_column_name($fieldname)
{
    return "p_".$fieldname."_export";
}
function api_table_imparare_column_name($fieldname)
{
    return "p_".$fieldname."_imparare";
}
function api_table_mailer_column_name($fieldname)
{
    return "p_".$fieldname."_mailer";
}

