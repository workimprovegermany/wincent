<?php


class Login extends AF\Action implements Event_notification_interface
{
    public static function get_events_to_notify()
    {
        return array('user.login');
    }

    public function execute()
    {
        $user = $_POST['user'];
        $pass = $_POST['pass'];

        $event = new AF\Event();

        $em = new Entity_mapper($this->app->db(),'user_login/1.0/config.xml');
        $fields = array(
            'uname' => $user,
            );

        $user_login_array = $em->find_by_fields($fields);

        if ($user_login_array !== null)
        {
            //user found in db
            $user_login = $user_login_array[0];

            // check password
            $user_password = $user_login->field('upass');
            if(password_verify($pass,$user_password))
            {
                // if password need rehash
                if(password_needs_rehash($user_password, PASSWORD_DEFAULT))
                {
                    $new_hash = password_hash($user_password, PASSWORD_DEFAULT);
                    // hier update HASH
                    $user_login->set_field('upass',$new_hash);
                    $em->save($user_login);
                }

                // save login number
                $user_all_logins = $user_login->field('all_logins');
                $user_login->set_field('all_logins',$user_all_logins+1);
                $em->save($user_login);
                // save login time
                $user_login->set_field('last_visit',date('Y-m-d H:i:s'));
                $em->save($user_login);
                // set falsch to 0
                $user_login->set_field('falsch',0);
                $em->save($user_login);

                $user_login->set_field('utoken', bin2hex(openssl_random_pseudo_bytes(64)));
                $em->save($user_login);

                $event->set_name('user.login_succeded')->set_data($user_login->field('utoken'));
            }
            else
            {
                $event->set_name('user.login_failed')->set_data($user);
            }

        }
        else
        {
            $event->set_name('user.login_failed')->set_data($user);
        }

        $this->app->dispatch_event($event);

        header('Content-type: application/json');
        echo $event->to_json();
    }
}