<?php


class Admin_upload_save_action extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('admin.werbematerial_save');
    }

    public function execute()
    {
        $event = new AF\Event();
        // target nach Zielgruppe machen
        $target_path = APP_ROOT_PATH . '/_media/'.$this->data['entity_folder'].'/'.$this->data['entity_fields']['zielgruppe'] . '/';

        $uploadfile = $target_path . basename(str_replace(" ","",$_FILES['file']['name']));
        $fileTmpPath = $_FILES['file']['tmp_name'];
        if(!is_dir($target_path)) {
            mkdir($target_path, 0777, true);
        }
        move_uploaded_file($fileTmpPath, $uploadfile);
        chmod($uploadfile,0777);
        //print_r($_FILES);
        //echo "hello from entity update action <br/>\n";
        //echo "data:\n";
        //print_r($this->data);
        //echo "<br/>\n";

        if($_SERVER['HTTP_HOST'] == '192.168.240.113') {
            $target_path_save = 'http://192.168.240.113/wincent.de/_media/'.$this->data['entity_folder'].'/'.$this->data['entity_fields']['zielgruppe'] . '/';
        }
        else {
            $target_path_save = 'https://wincent-online.de/_media/'.$this->data['entity_folder'].'/'.$this->data['entity_fields']['zielgruppe'] . '/';
        }

        // Datei speichern
        $app = App::get_instance();

        $app_config = $app->config();
        $entity_config_path = $app_config->entity_config_for_name($this->data['entity_name']);

        $e = Entity_config::create($entity_config_path);


        // Daten speichern
        foreach ($this->data['entity_fields'] as $name => $value)
         {
             $e->set_field($name, $value);
         }
             $e->set_field('pfad',$target_path_save);
             $em = new Entity_mapper($app->db(), $entity_config_path);
             $em->insert($e);

        $event = new AF\Event();
        $event->set_name('entity.saved')->set_data('success');

        header('Content-type: application/json');
        echo $event->to_json();
    }
}