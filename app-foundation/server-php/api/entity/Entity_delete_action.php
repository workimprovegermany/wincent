<?php


class Entity_delete_action extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('entity.delete');
    }

    public function execute()
    {
        /*
         * gelöscht wird vom Modell entity_name die ID entity_id
         */

        $event = new AF\Event();

        //echo "hello from entity load action <br/>\n";
        //echo "data:\n";
        //print_r($this->data);
        //echo "<br/>\n";

        $app = App::get_instance();
        if($this->data['entity_name'] == "advertising_media") {
            $file_pfad = APP_ROOT_PATH . '/_media/werbematerial/'. $this->data['entity_ziel'].'/'.$this->data['entity_datei'];
            if(@unlink(realpath($file_pfad)) ) {
                $test =  "Deleted file ";
            }
            else {
                $test = "File can't be deleted";
                // $test = is_readable($this->data['entity_datei']);
                //$test = stat($file_pfad);
            }
        }


        $app_config = $app->config();
        $entity_config = $app_config->entity_config_for_name($this->data['entity_name']);
        $ec = new Entity_config($entity_config);

        // delete relations
        $rel_keys = $ec->get_relation_keys();
        foreach ($rel_keys as $rel_key)
        {
            // delete all relations
            $relation_table_name = $ec->get_table_name() . "_" . $rel_key;
            $app->db()->delete_field_value($ec->get_table_name(), $this->data['entity_id'], $relation_table_name);
        }



        $em = new Entity_mapper($app->db(), $entity_config);
        $em->delete($this->data['entity_id']);

        $event->set_name('entity.deleted')->set_data($this->data['entity_name']);
        header('Content-type: application/json');
        echo $event->to_json();
    }
}