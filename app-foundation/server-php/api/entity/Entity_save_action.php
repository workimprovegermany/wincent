<?php


class Entity_save_action extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('entity.save');
    }

    public function execute()
    {
        $error_fields = array();

        $app = App::get_instance();

        $app_config = $app->config();
        $entity_config_path = $app_config->entity_config_for_name($this->data['entity_name']);

        $ec = new Entity_config($entity_config_path);

        $e = Entity_config::create($entity_config_path);
        foreach ($this->data['entity_fields'] as $name => $value)
        {
            if ($ec->is_relation_key($name))
            {
                // save relations later, when entity-id is set
            }
            else
            {
                $e->set_field($name, $value);

                $fd = $ec->field_description($name);

                if (isset($fd['validation'])) {
                    $m = preg_match('$' . $fd['validation'] . '$', $value, $matches);

                    if ($m == 0) {
                        array_push($error_fields, $name);
                    }
                }
            }
        }

        $em = new Entity_mapper($app->db(), $entity_config_path);
        $em->insert($e);

        foreach ($this->data['entity_fields'] as $name => $value)
        {
            if ($ec->is_relation_key($name))
            {
                // Only many-to-many implemented

                $relation_table_name = $ec->get_table_name() . "_" . $name;

                // delete all relations
                //$app->db()->delete_field_value($ec->get_table_name(), $e->id(), $relation_table_name);

                // and save new
                foreach ($value as $v)
                {
                    $app->db()->insert([$ec->get_table_name() => $e->id(), $name => $v], $relation_table_name);
                }

            }
        }

        $event = null;

        if (!empty($error_fields))
        {
            $event = new Event_error('Fehlerhafte Felder.', $error_fields);
        }
        else
        {
            $event = new Event_ok();
        }

        header('Content-type: application/json');
        echo $event->to_json();
    }
}