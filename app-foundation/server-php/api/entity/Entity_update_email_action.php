<?php


class Entity_update_email_action extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('entity.update_email');
    }

    public function execute()
    {
        $error_fields = array();

        $app = App::get_instance();

        $app_config = $app->config();
        $entity_config_path = $app_config->entity_config_for_name($this->data['entity_name']);

        $ec = new Entity_config($entity_config_path);

        $em = new Entity_mapper($app->db(), $entity_config_path);

        $entity = $em->find_by_id($this->data['entity_id']);

        //

        foreach ($this->data['entity_fields'] as $name=>$value)
        {
            $entity->set_field($name, $value);

            $fd = $ec->field_description($name);

            if (isset($fd['validation']))
            {
                $m = preg_match('$'.$fd['validation'].'$', $value, $matches);

                if ($m == 0)
                {
                    array_push($error_fields, $name);
                }
            }
            if($name == 'email') {
                $email = new Entity_mapper($app->db(),'user_login/1.0/config.xml');
                $entity_email = $email->find_by_fields(['uid' => $entity->id()]);
                $entity_email[0]->set_field('uname',$value);
            }
        }
        $email->save($entity_email[0]);
        $em->save($entity);

        $event = null;

        if (!empty($error_fields))
        {
            $event = new Event_error('Fehlerhafte Felder.', $error_fields);
        }
        else
        {
            $event = new Event_ok();
        }

        header('Content-type: application/json');
        echo $event->to_json();
    }
}