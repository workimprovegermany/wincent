<?php

class Paypal
{
    private $path_payment = '/v1/payments/payment';
    private $path_invoicing = '/v1/invoicing/invoices';
    private $path_auth = '/v1/oauth2/token';
    private $url_sandbox = 'https://api.sandbox.paypal.com';

    private $api_url = '';
    private $return_url = '';
    private $cancel_url = '';
    private $error_url = '';

    private $auth_mode = 'webauth';
    private $auth_login = '';
    private $auth_password = '';
    private $token = '';

    private $payment_id = '';
    private $sale_id = '';
    private $sale_response = '';
    private $create_response = '';
    private $invoice_id = '';
    private $invoice_response = '';

    private $merchant_info = array();

    public function __construct()
    {
    }

    public function init($config_array)
    {
        $this->api_url = $config_array['api_url'];
        $this->return_url = $config_array['return_url'];
        $this->cancel_url = $config_array['cancel_url'];
        $this->error_url = $config_array['error_url'];

        if (isset($config_array['login']) && isset($config_array['password']))
        {
            if (!empty($config_array['login']) && !empty($config_array['password']))
            {
                $this->authorize($config_array['login'], $config_array['password']);
            }
        }

        $this->merchant_info = $config_array['merchant_info'];
    }

    public function set_auth($login, $password)
    {
        $this->auth_login = $login;
        $this->auth_password = $password;
    }

    public function new_token()
    {
        $rc = new Rest($this->url_sandbox);
        $rc->set_post_format('default');
        $auth_data = array();
        $auth_data['login'] = $this->auth_login;
        $auth_data['password'] = $this->auth_password;
        $rc->set_auth_mode($this->auth_mode, $auth_data);
        $response = $rc->post($this->path_auth, 'grant_type=client_credentials');

        $this->token = $response->access_token;
    }

    public function authorize($login, $password)
    {
        $this->set_auth($login, $password);
        $this->new_token();
    }

    public function create_payment($data_json)
    {
        $rc = new Rest($this->url_sandbox);
        $rc->set_auth_mode('bearer', $this->token);
        $response = $rc->post($this->path_payment, $data_json);

        //$this->debug_output("payment response", $response);
        $this->create_response = $response;

        if (isset($response->id))
        {
            $this->payment_id = $response->id;
        }
    }

    public function create_payment_from_item_list($item_list)
    {
        $subtotal = array_sum(array_column($item_list, 'price'));
        $tax = array_sum(array_column($item_list, 'tax'));
        $total = $subtotal + $tax;

        $data_create_payment = array(
            'intent' => 'sale',
            'redirect_urls' => array(
                'return_url' => $this->return_url,
                'cancel_url' => $this->cancel_url
            ),
            'payer' => array(
                'payment_method' => 'paypal'
            ),
            'transactions' => array(
                array(
                    'amount' => array(
                        'total' => $total,
                        'currency' => 'EUR',
                        'details' => array(
                            'subtotal' => $subtotal,
                            'shipping' => '0.00',
                            'tax' => $tax,
                            'shipping_discount' => '0.00'
                        )
                    ),
                    'item_list' => array(
                        'items' => $item_list
                    )
                )
            )
        );
        //TODO after item list, the following fields are optional:
        // "description": "The payment transaction description.",
        //'invoice_number' => date('YmdHis')
        // "custom": "merchant custom data"

        $this->create_payment(json_encode($data_create_payment));
    }

    public function execute_payment($payment_id, $payer_id)
    {
        $rc = new Rest($this->url_sandbox);
        $rc->set_auth_mode('bearer', $this->token);
        $execute_payment_path = $this->path_payment . '/' . $payment_id . '/execute/';
        $data = array();
        $data['payer_id'] = $payer_id;
        $data_json = json_encode($data);
        $response = $rc->post($execute_payment_path, $data_json);

        //$this->debug_output("payment response", $response);
        $this->sale_id = $response->transactions->related_resources->sale->id;
        $this->sale_response = json_encode($response);
    }

    public function generate_invoice($data_json)
    {
        $rc = new Rest($this->url_sandbox);
        $rc->set_auth_mode('bearer', $this->token);
        $response = $rc->post($this->path_invoicing, $data_json);

        $this->invoice_id = $response->id;
        $this->invoice_response = json_encode($response);

        $this->debug_output("generate invoice", $response);
    }

    private function get_invoice_item_list_from_sales_info($sales_info)
    {
        $items = array();

        foreach ($sales_info->transactions[0]->item_list->items as $item)
        {
            $i = array();
            $i['name'] = $item->name;
            $i['quantity'] = $item->quantity;
            $i['unit_price'] = array(
                    'currency' => $item->currency,
                    'value' => $item->price
            );
            $i['tax'] = array(
                    'name' => 'Tax',
                    'percent' => 19
            );
            array_push($items, $i);
        }

        return $items;
    }

    public function generate_invoice_for_checkout($sales_info)
    {
        //$this->debug_output('sales info', $sales_info);

        $data_generate_invoice = array(
                'merchant_info' => $this->merchant_info,
                'billing_info' => array(
                        array(
                            'email' => $sales_info->payer->payer_info->email,
                            'first_name' => $sales_info->payer->payer_info->first_name,
                            'last_name' => $sales_info->payer->payer_info->last_name
                        )
                ),
                'items' => $this->get_invoice_item_list_from_sales_info($sales_info),
                'shipping_cost' => array(
                        'amount' => array(
                                'currency' => 'EUR',
                                'value' => 0
                        )
                ),
                'note' => 'Danke.',
                'terms' => 'keine Rücknahme nach 30 Tagen'
        );

        //echo "<pre>";
        //print_r($data_generate_invoice);
        //echo "</pre>";

        $this->generate_invoice(json_encode($data_generate_invoice));
    }

    public function send_invoice()
    {
        $rc = new Rest($this->url_sandbox);
        $rc->set_auth_mode('bearer', $this->token);
        $response = $rc->post($this->path_invoicing . '/' . $this->invoice_id . '/send?notify_merchant=true?notify_customer=true');

        $this->debug_output("send invoice", $response);
    }

    public function mark_invoice_as_paid()
    {
        $data_json = '{
            "method": "PAYPAL",
            "date": "2018-06-01 10:30:00 PST",
            "note": "bezahlt",
            "amount": {
                "currency": "EUR",
                "value": "10.00"
            }
        }';

        $rc = new Rest($this->url_sandbox);
        $rc->set_auth_mode('bearer', $this->token);
        $response = $rc->post($this->path_invoicing . '/' . $this->invoice_id . '/record-payment', $data_json);

        $this->debug_output("mark invoice as paid", $response);
    }

    public function invoice_details()
    {
        $rc = new Rest($this->url_sandbox);
        $rc->set_auth_mode('bearer', $this->token);
        $response = $rc->get($this->path_invoicing . '/' . $this->invoice_id);

        $this->debug_output("invoice details " . $this->invoice_id, $response);
    }

    public function invoice_response()
    {
        return $this->invoice_response;
    }

    public function payment_id()
    {
        return $this->payment_id;
    }

    public function payment_id_json()
    {
        $ret = array();
        $ret['id'] = $this->payment_id;

        //debug
        //$ret['debug'] = json_encode($_POST);
        //$ret['debug'] = $this->create_response;

        return json_encode($ret);
    }

    public function sale_id()
    {
        return $this->sale_id;
    }

    public function sale_id_json()
    {
        $ret = array();
        $ret['id'] = $this->sale_id;
        return json_encode($ret);
    }

    public function sale_response()
    {
        return $this->sale_response;
    }

    private function debug_output($txt, $data)
    {
        echo $txt;
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }


    public function render_button($check_id_array = null, $message_id = null)
    {
        $validation = "";
        $validation_pp = "";

        if ($check_id_array !== null)
        {
            $chk_str = array();
            $el_str = "";
            foreach ($check_id_array as $c)
            {
                array_push($chk_str, "document.querySelector('#$c').checked");
                $el_str .= "document.querySelector('#$c').addEventListener('change', handler);";
            }

            $validation =
                "function isValid() {return ".implode(' && ', $chk_str).";}
                 function onChangeCheckbox(handler) { $el_str }
                 function toggleValidationMessage() {document.querySelector('#$message_id').style.display = (isValid() ? 'none' : 'block');}
                 function toggleButton(actions) {return isValid() ? actions.enable() : actions.disable();}";

            $validation_pp =
                "
                validate: function(actions) 
                {
                    toggleButton(actions);
                    onChangeCheckbox(function() {
                        toggleButton(actions);
                    });
                },
                onClick: function() {
                    toggleValidationMessage();
                },
            ";
        }

        ?>

        <script>
            <?php echo $validation ?>

            paypal.Button.render({
                env: 'sandbox', // 'production' or 'sandbox',
                commit: true, // Show a 'Pay Now' button
                style: {
                    color: 'blue',
                    size: 'medium'
                },
                funding: {
                    allowed: [ paypal.FUNDING.CARD, paypal.FUNDING.ELV ]
                },
                <?php echo $validation_pp ?>
                payment: function(data, actions)
                {
                    return paypal.request.post('<?php echo $this->api_url ?>', {"af_cmd" : "paypal_create_payment", "af_data" : '{"product_id" : "1"}' }).then(function(data) {
                        return data.id;
                    });
                },
                onAuthorize: function(data, actions) {
                    return paypal.request.post('<?php echo $this->api_url ?>', {
                        paymentID: data.paymentID,
                        payerID:   data.payerID,
                        af_cmd : "paypal_execute_payment"
                    }).then(function() {

                        // The payment is complete!
                        // You can now show a confirmation message to the customer
                        //alert("Zahlung abgeschlossen");
                        //console.log("data abgeschlossen");
                        //console.log(data);
                        window.location.href = data.returnUrl;
                    });
                },
                onCancel: function(data, actions) {
                    /*
                     * Buyer cancelled the payment
                     */
                    //alert("abgebrochen");
                    //console.log("abgebrochen");
                    //console.log(data);
                    //console.log(actions);
                    window.location.href = data.cancelUrl;
                },
                onError: function(err) {
                    /*
                     * An error occurred during the transaction
                     */
                    //alert(err);
                    //console.log("error:");
                    //console.log(err);
                    window.location.href = '<?php echo $this->error_url ?>';
                }
            }, '#paypal-button');
        </script>

        <?php
    }
}