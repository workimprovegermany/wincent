<?php


class Ping_action extends AF\Action implements Event_notification_interface
{
    public static function get_events_to_notify()
    {
        return array('system.ping');
    }

    public function execute()
    {
        sleep(2);

        $event = new Event_ok();

        header('Content-type: application/json');
        echo $event->to_json();
    }
}