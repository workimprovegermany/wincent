<?php


class User_daten_action extends AF\Action implements Event_notification_interface
{
    public static function get_events_to_notify()
    {
        return array('user.check_daten');
    }

    public function execute()
    {
        //$event = new AF\Event();

        $fehler = 'OK';

        foreach ($this->data['entity_fields'] as $name=>$value) {
            if(empty($value) || $value=='NULL') {
                $fehler = 'empty';
            }
        }

        if($fehler == 'empty') {
            $event = new Event_error($fehler);
        }
        else {
            $event = new Event_ok();
        }
        //$event->set_name('daten.empty')->set_data($fehler);
        header('Content-type: application/json');
        echo $event->to_json();


    }
}
