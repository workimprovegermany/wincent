<?php


class User_get_permissions extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('user.get_permissions');
    }

    private function get_user_roles($id)
    {
        $em = new Entity_mapper($this->app->db(), 'user/1.0/config.xml');
        $user = $em->find_by_id($id);
        $user->load_relations();
        return $user->relations['role'];
    }

    private function get_role_permissions($id)
    {
        $em = new Entity_mapper($this->app->db(), 'role/1.0/config.xml');
        $role = $em->find_by_id($id);
        $role->load_relations();
        return $role->relations['permission'];
    }

    private function get_permission_name($id)
    {
        $em = new Entity_mapper($this->app->db(), 'permission/1.0/config.xml');
        $permission = $em->find_by_id($id);
        return $permission->field('name');
    }

    private function send_data($data)
    {
        header('Content-type: application/json');
        echo json_encode($data);
    }

    public function execute()
    {
        $ret = array();

        $roles = $this->get_user_roles($this->data['user_id']);
        if (!is_array($roles) || empty($roles)) {$this->send_data($ret);return;}

        foreach ($roles as $role)
        {
            $permissions = $this->get_role_permissions($role);
            if (!is_array($permissions) || empty($permissions)) continue;

            foreach ($permissions as $permission)
            {
                array_push($ret, $this->get_permission_name($permission));
            }
        }

        $return_data = ['permissions' => $ret];
        $this->send_data($return_data);
    }
}