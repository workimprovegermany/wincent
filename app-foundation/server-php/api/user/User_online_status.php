<?php


class User_online_status extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('user.online_status');
    }

    private function get_user_roles($id)
    {
        $em = new Entity_mapper($this->app->db(), 'user/1.0/config.xml');
        $user = $em->find_by_id($id);
        $user->load_relations();
        return $user->relations['role'];
    }

    private function send_data($data)
    {
        header('Content-type: application/json');
        echo json_encode($data);
    }

    private function get_user_online_status($id)
    {
        $em = new Entity_mapper($this->app->db(), 'user/1.0/config.xml');
        $user = $em->find_by_id($id);
        $ret = $user->field('online_status') == '1' ? 'online' : 'offline';
        return $ret;
    }

    private function get_role_online_status($role_id)
    {
        $em = new Entity_mapper($this->app->db(), 'user/1.0/config.xml');
        $users = $em->find_by_fields(['online_status' => '1']);

        $ret = 'offline';
        foreach ($users as $user)
        {
            $roles = $this->get_user_roles($user->id());
            if (in_array($role_id, $roles)) return 'online';
        }

        return $ret;
    }

    public function execute()
    {
        $online_status = 'offline';

        if (isset($this->data['user_id'])) $online_status = $this->get_user_online_status($this->data['user_id']);

        if (isset($this->data['role'])) $online_status = $this->get_role_online_status($this->data['role']);

        $return_data = ['online_status' => $online_status];
        $this->send_data($return_data);
    }
}