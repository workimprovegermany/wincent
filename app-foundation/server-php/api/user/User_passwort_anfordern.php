<?php


class User_passwort_anfordern extends AF\Action implements Event_notification_interface
{
    public static function get_events_to_notify()
    {
        return array('user.passwort_anfordern');
    }

    public function execute()
    {
        $user_name = $_POST['user'];

        $em = new Entity_mapper($this->app->db(), 'user_login/1.0/config.xml');
        $fields = array(
            'uname' => $user_name,
        );

        $user_login_array = $em->find_by_fields($fields);

        if ($user_login_array !== null) {
            //user found in db
            $user_login = $user_login_array[0];
             $user_login->set_field('sessionpass', session_id());
             $em->save($user_login);
             $mail_event = new AF\Event('mail.send', [
                       'config' => 'supportmails',
                       'template' => ['mails/user-passwort-anfordern.html',
                            [
                                'user_login' => $user_login->id(),
                            ]
                        ],
                 ]);
             $this->app->dispatch_event($mail_event);


             $event = new AF\Event();
             $event->set_name('email.erfolgreich')->set_data($user_login->id());

             header('Content-type: application/json');
             echo $event->to_json();

        }
        else {

            $event = new AF\Event();
            $event->set_name('entity.error')->set_data('error_email');

            header('Content-type: application/json');
            echo $event->to_json();
        }
    }
}
