<?php


class User_passwort_update_action extends AF\Action implements Event_notification_interface
{

    public static function get_events_to_notify()
    {
        return array('user.passwort_update');
    }

    public function execute()
    {
        $event = new AF\Event();

        $passwort = $_POST['pass'];
        $session = $_POST['session'];
        echo $passwort;

        $em = new Entity_mapper($this->app->db(), 'user_login/1.0/config.xml');
        $fields = array(
            'sessionpass' => $session,
        );
        $user_login_array = $em->find_by_fields($fields);
        if ($user_login_array !== null) {
            $user_login = $user_login_array[0];
            $pass_value = password_hash($passwort,PASSWORD_DEFAULT);
            $user_login->set_field('upass', $pass_value);
            $em->save($user_login);

            $event->set_name('passwort.erfolgreich')->set_data($pass_value);
        }
        else {
            $event->set_name('passwort.error')->set_data('keine Session');
        }
        header('Content-type: application/json');
        echo $event->to_json();


    }
}