<?php


class App
{

    /*
     * Singleton
     */
    protected static $instance = null;
    public static function get_instance()
    {
        $singleton_class = get_called_class();

        if (self::$instance === null)
        {
            self::$instance = new $singleton_class;
            // call special init after App contructor
            call_user_func(array(self::$instance, 'init'));
        }
        return self::$instance;
    }
    final protected function __clone() {}
    protected function __construct()
    {
        $this->app_config = new Config(APP_CONFIG);

        $this->event_dispatcher = new Event_dispatcher();
        $this->subscribe_listeners();

        $this->init_entities();

        $this->init_db();

        //Routingtest
        $this->router = new Router();
        if ($this->config_entry('host') != null)
        {
            $this->router->set_host($this->config_entry('host'));
        }
        $this->router->init();
    }


    /*
     * App Config
     */
    private $app_config;

    public function config()
    {
        return $this->app_config;
    }

    public function config_entry($entry)
    {
        return $this->app_config->entry($entry);
    }


    /*
     * Event Dispatcher
     */
    private $event_dispatcher = null;

    public function dispatch_event(AF\Event $e)
    {
        return $this->event_dispatcher->dispatch($e);
    }

    private function subscribe_listeners()
    {
        $events = $this->app_config->events();

        foreach ($events as $event_name=>$action_name)
        {
            $action_path = APP_ROOT_PATH.'/'.$action_name;
            $classname=basename($action_path, '.php');
            $this->event_dispatcher->add_subscriber($classname);
        }

    }

    /*
     * Entities
     */
    private $entities = array();
    public function entities()
    {
        return $this->entities;
    }
    private function init_entities()
    {
        $entities = $this->app_config->entity_array();

        $i = 0;
        foreach ($entities as $entity)
        {
            $ec = new Entity_config($entity['config']);
            $entities[$i]['table_name'] = $ec->get_table_name();
            $i++;
        }

        $this->entities = $entities;

    }

    /*
     * DB
     */
    private $pdo;
    private function init_db()
    {
        // initialize db connection
        $this->pdo = new PDO_storage_adapter(APP_CONFIG);
    }
    public function db()
    {
        return $this->pdo;
    }


    public $user = false;

    public $router;

    public $filter = array();

    public function run()
    {
        $site = $this->router->run();

        // 1. nach einem Datenbankeintrag für die URL suchen
        $path = '';
        $url_comp_arr = parse_url($_SERVER['REQUEST_URI']);
        if(isset($url_comp_arr['path']))
        {
            $path = $url_comp_arr['path'];
        }
        //echo "path: $path <br/>";
        $em = new Entity_mapper($this->pdo);
        $em->set_config(new Entity_config('text/1.0/config.xml'));
        $fields = ['url' => $path];
        $db_site = $em->find_by_fields($fields, 1)[0];
        if ($db_site != null)
        {
            //echo "found in db <br/>";
            $view = new Entity_view($this);
            $view->show($db_site->get_field('text'));
        }
        else
        {
            //echo "NOT found in db <br/>";
            // 2. filename suchen
            $filename = $site;
            if (file_exists($filename))
            {
                //echo "file ($filename) found <br/>";
                include ($filename);
            }
            else
            {
                //echo "file ($filename) NOT found <br/>";
                // if site shows entity
                $view = new Entity_view($this);
                $view->show();


                // TODO 404 Site
                //echo "nothing found ($filename) fallback homepage from db <br/>";
                //Filter::echo_all_filters($this->filter);
                //$db_site = $this->entity_mapper->find_by_fields(['url' => '/'],'texts');
                //$view = new Entity_view($this);
                //$view->show($db_site->get_field('text'));
            }


        }
    }
}
