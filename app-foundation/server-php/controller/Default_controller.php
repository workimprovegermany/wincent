<?php

class Default_controller
{
    public $name;

    function __construct($name)
    {
        $this->name = $name;
        if ($this->name == '')
        {
            $this->name = 'home';
        };
    }

    function execute($action = '', $parameter = '')
    {
        define('CONTROLLER_NAME', $this->name);
        define('CONTROLLER_ACTION', $action);
        define('CONTROLLER_PARAMETER', $parameter);

        //echo "Defaultcontroller " . "<br/>";
        //echo "controllername " . CONTROLLER_NAME . "<br/>";
        //echo "controlleraction " . CONTROLLER_ACTION . "<br/>";
        //echo "controllerparameter " . CONTROLLER_PARAMETER . "<br/>";

        $viewname = $this->name;

        //TODO: Don't append action with underscore
        /*if ($action != '' && $action != 'show')
        {
            $viewname = $viewname.'_'.$action;
        }*/

        $file_name = '../views/'.$viewname.'.php';

        if (file_exists($file_name))
        {
            return $file_name;
        }

        return null;
    }
}