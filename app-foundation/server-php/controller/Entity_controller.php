<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 01.09.17
 * Time: 13:36
 */

class Entity_controller
{
    public $name;

    public function __construct($name)
    {
        $this->name = $name;
        if ($this->name == '')
        {
            $this->name = 'home';
        };
    }

    public function execute($action = '', $parameter = '')
    {
        define('CONTROLLER_NAME', $this->name);
        define('CONTROLLER_ACTION', $action);
        define('CONTROLLER_PARAMETER', $parameter);

        $fn = $this->name;
        if ($action != 'show')
        {
            $fn = $fn . '_' . $action;
        }

        //TODO: action deleted
        $filename = '../views/'.$this->name.'.php';

        if (file_exists($filename))
        {
            return $filename;
        }

        return null;

    }
}