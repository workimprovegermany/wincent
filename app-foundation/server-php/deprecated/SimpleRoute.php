<?php

class SimpleRoute
{
    public static $routes = Array();
    public static $routes_err = Array();
    public static $path;

    public static function init()
    {
        self::$path = '';

        $url_comp_arr = parse_url($_SERVER['REQUEST_URI']);
        if(isset($url_comp_arr['path']))
        {
            self::$path = trim($url_comp_arr['path'],'/');
        }
    }

    public static function add_route($expression, $function)
    {
        array_push(self::$routes,Array(
            'expression'=>$expression,
            'function'=>$function
        ));
    }

    public static function add_route_err($function)
    {
        array_push(self::$routes_err,$function);
    }

    public static function run()
    {
        $route_found = false;

        foreach(self::$routes as $route)
        {
            /*if(Config::get('basepath'))
            {
                $route['expression'] = '('.Config::get('basepath').')/'.$route['expression'];
            }*/

            //Add 'find string start' automatically
            $route['expression'] = '^'.$route['expression'];
            //Add 'find string end' automatically
            $route['expression'] = $route['expression'].'$';

            //check match
            if (preg_match('#'.$route['expression'].'#',self::$path,$matches))
            {
                array_shift($matches);//first element contains whole string
                /*if(Config::get('basepath'))
                {
                    array_shift($matches); // remove basepath
                }*/

                call_user_func_array($route['function'], $matches);

                $route_found = true;
            }
        }

        if(!$route_found)
        {
            foreach(self::$routes_err as $route404)
            {
                call_user_func_array($route404, Array(self::$path));
            }
        }

    }
}