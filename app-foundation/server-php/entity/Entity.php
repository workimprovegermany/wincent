<?php


class Entity
{
    private $id = null;
    private $state = 'TRANSIENT';

    private $config_path;
    private $attr;
    private $storage_adapter = null;

    public $relations = null;

    /*
     * CONSTRUCTING
     */

    public function __construct($config_path = null)
    {
        $this->config_path = $config_path;

        if (!empty($config_path))
        {
            $conf = new Entity_config($this->config_path);
            //$this->relations = $conf->get_relation_keys();
        }
    }


    /*
     * *******************************
     */

    public function init($attr_array)
    {
        $this->attr = $attr_array;
    }


    public static function from_state(array $state, $config_path = null)
    {
        $e = new self($config_path);

        foreach ($state as $k => $v)
        {
            if ($k == 'id')
            {
                $e->id = $v;
            }
            else
            {
                $e->attr[$k] = $v;
            }
        }

        return $e;
    }

    public static function to_state($entity)
    {
        $ea = $entity->attr;
        $ea['id'] = $entity->id();
        return $ea;
    }

    public function get_field($field_name)
    {
        return $this->field($field_name);
    }

    public function field($field_name)
    {
        return $this->attr[$field_name];
    }

    public function set_field($name, $value)
    {
        $this->attr[$name] = $value;
    }

    public function attr()
    {
        return $this->attr;
    }

    public function get_id()
    {
        return $this->id();
    }

    public function id()
    {
        return $this->id;
    }

    public function set_id($id)
    {
        $this->id = $id;
    }

    public function config_path()
    {
        return $this->config_path;
    }

    public function turn_into($state)
    {
        $this->state = $state;
    }

    public function load_relations()
    {
        $adapter = App::get_instance()->db();
        $ec = new Entity_config($this->config_path);
        $relation_keys = $ec->get_relation_keys();
        if (!empty($relation_keys))
        {
            foreach ($relation_keys as $relation_key)
            {
                if ($ec->get_relation_type($relation_key) == 'many-to-many')
                {
                    $relation_table_name = $ec->get_table_name() . "_" . $relation_key;

                    $filter_fields = array();
                    $filter_fields[$ec->get_table_name()] = $this->id();
                    $dbg_ff = $filter_fields;

                    $relation_table_rows = $adapter->rows_for_field($filter_fields, $relation_table_name);
                    $related_entity_ids = array();
                    foreach ($relation_table_rows as $r_row)
                    {
                        array_push($related_entity_ids, $r_row[$relation_key]);
                    }
                    if ($this->relations == null) $this->relations = array();
                    $this->relations[$relation_key] = $related_entity_ids;
                }
            }
        }
    }

    /**
     * find Entity objects that are related with $relation
     *
     * @param $entity
     * @return Entity[]|null
     */
    public function get_related($relation)
    {
        $m_cfg = new Entity_config($this->config_path);
        $adapter = App::get_instance()->db();

        if ($m_cfg->get_relation_type($relation) == 'many_to_many')
        {
            $relation_table_name = $m_cfg->get_table_name() . "_" . $relation;

            $filter_fields = array();
            $filter_fields[$m_cfg->get_table_name()] = $this->get_field("id");

            $relation_table_rows = $adapter->rows_for_field($filter_fields, $relation_table_name);
            $related_entity_ids = array();
            foreach ($relation_table_rows as $r_row)
            {
                array_push($related_entity_ids, $r_row[$relation]);
            }

            $related_entities = array();
            $r_path = $relation."/1.0/config.xml";
            $r_cfg = new Entity_config($r_path);
            $r_mapper = new Entity_mapper($adapter);
            $r_mapper->set_config($r_cfg);

            foreach ($related_entity_ids as $r_id)
            {
                $e = $r_mapper->find_by_id($r_id);
                array_push($related_entities, $e);
            }

            return $related_entities;
        }
        elseif ($m_cfg->get_relation_type($relation) == 'one_to_many')
        {
            $filter_fields = array();
            $filter_fields[$m_cfg->get_table_name()."_id"] = $this->get_field("id");
            $relation_table_rows = $adapter->rows_for_field($filter_fields, $relation);

            $related_entity_ids = array();
            foreach ($relation_table_rows as $r_row)
            {
                array_push($related_entity_ids, $r_row['id']);
            }

            $related_entities = array();
            $r_path = $relation."/1.0/config.xml";
            $r_cfg = new Entity_config($r_path);
            $r_mapper = new Entity_mapper($adapter);
            $r_mapper->set_config($r_cfg);

            foreach ($related_entity_ids as $r_id)
            {
                $e = $r_mapper->find_by_id($r_id);
                array_push($related_entities, $e);
            }

            return $related_entities;
        }

        return null;
    }


    public function compare_with_fields($fields)
    {
        //TODO new attribute in $fields possible -> add to db

        $ret = array();
        foreach ($this->attr as $k=>$v)
        {
            if (isset($fields[$k]) && $fields[$k] != $v)
            {
                $ret[$k] = $fields[$k];
            }
        }

        return empty($ret) ?  null : $ret;
    }
}