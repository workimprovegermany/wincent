<?php

require_once ("Entity.php");

class Entity_manager
{
    private $mapper;
    private $config_path;

    // TODO wieder private machen
    public $entities = array();

    public function __construct($entity_mapper, $config_path)
    {
        $this->mapper = $entity_mapper;
        $this->config_path = $config_path;
    }

    function registerXPathNamespaces($el)
    {
        foreach($el->getDocNamespaces() as $strPrefix => $strNamespace) {
            if(strlen($strPrefix)==0) {
                $strPrefix="a";
            }
            $el->registerXPathNamespace($strPrefix,$strNamespace);
        }
    }

    // TODO: gehört zu Amazon -> auslagern
    public function init_from_amazon_answer($xmlstr)
    {
        $mxl_wo_ns = str_replace('xmlns=', 'ns=', $xmlstr); //destroy namespace
        $aa = new SimpleXMLElement($mxl_wo_ns);
        //$this->registerXPathNamespaces($aa);

        $conf = new Entity_config($this->config_path);
        $config_xml = new SimpleXMLElement(file_get_contents($conf->get_absolute_path()));
        $fields = $config_xml->xpath('/config/fields/field');

        foreach ($aa->Items->Item as $item)
        {
            $valid = true;

            $this->registerXPathNamespaces($item);

            $p = new Entity();

            $prod_attr = array();
            $timestamp = date('Y-m-d G:i:s');
            $prod_attr['last_mod'] = $timestamp;
            foreach ($fields as $field)
            {
                $name = $field->name;
                $ampath = $field->amazon_item_path;

                if (empty($ampath) || $item->xpath($ampath) == false)
                {
                    $prod_attr["$name"] = "";
                    if (!empty($ampath))
                    {
                        $valid = false;
                    }
                }
                else
                {
                    $prod_attr["$name"] = $item->xpath($ampath)[0]->__toString();
                }
            }

            //TODO remove special code
            if (!empty($prod_attr['amazon_offer_price']) && !empty($prod_attr['amazon_list_price']) && (intval($prod_attr['amazon_offer_price']) < intval($prod_attr['amazon_list_price'])))
            {
                $prod_attr['sale'] = 1;
            }
            else
            {
                $prod_attr['sale'] = 0;
            }

            if ($valid)
            {
                $prod_attr['valid'] = 1;
            }
            else
            {
                $prod_attr['valid'] = 0;
            }


            $p->init($prod_attr);

            array_push($this->entities, $p);
        }
    }

    // TODO: gehört zu Amazon -> auslagern
    public function errors_from_amazon_answer($xmlstr)
    {
        $mxl_wo_ns = str_replace('xmlns=', 'ns=', $xmlstr); //destroy namespace
        $aa = new SimpleXMLElement($mxl_wo_ns);
        //$this->registerXPathNamespaces($aa);

        $conf = new Entity_config($this->config_path);
        $config_xml = new SimpleXMLElement(file_get_contents($conf->get_absolute_path()));

        $fields = $config_xml->xpath('/config/fields/field');

        $errors = $aa->xpath('Items/Request/Errors/Error[Code = "AWS.InvalidParameterValue"]');

        return $errors;
    }

    // TODO: gehört zu Amazon -> auslagern
    public function add_from_asin_list($asin_array)
    {
        foreach ($asin_array as $asin)
        {
            $pr = new Entity();
            $pr->set_field('last_mod', "2000-01-01 00:00:00");
            $pr->set_field('asin', $asin);

            array_push($this->entities, $pr);

        }
    }

    // TODO: gehört zu Amazon -> auslagern
    // hier müßte es sowas geben wie make_managed_entity_from_xml für amazon oder normal
    // bei amazon nur ASIN übernehmen, weil der rest nachgeladen wird, sonst alle felder übernehmen
    public function add_from_xml($xml_entities)
    {
        foreach ($xml_entities as $entity)
        {

            $en = new Entity();
            $en->set_field('last_mod', "2000-01-01 00:00:00");
            $en->set_field('asin', $entity->ASIN->__toString());

            array_push($this->entities, $en);
        }
    }

    public function save_all_entities($table)
    {
        foreach ($this->entities as $product)
        {
            $result = $this->mapper->insert($product, $table);
        }
    }
}