<?php

class Event_dispatcher
{
    private $listeners = array();

    public function __construct()
    {
    }

    public function add_listener($event_name, $listener)
    {
        //TODO: insert php callables as additional listeners
        //$this->listeners[$event_name] = $listener;
    }

    public function add_subscriber($subscriber)
    {
        $event_names = $subscriber::get_events_to_notify();

        foreach ($event_names as $event_name)
        {
            $this->listeners[$event_name] = $subscriber;

            //TODO: add priority for events
        }

    }

    public function dispatch(AF\Event $event)
    {
        $action_name = $this->listeners[$event->name()];

        if (empty($action_name))
        {
            return;
        }

        $action = new $action_name($event->data());
        $action->set_app(App::get_instance());
        $action->execute();

        //TODO: add parameter for listeners, that stops other listeners for the same event
    }
}