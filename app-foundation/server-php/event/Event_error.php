<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 2019-02-19
 * Time: 17:32
 */


class Event_error extends \AF\Event
{

    public function __construct($message = null, $data = null)
    {
        $d = array();
        $d['message'] = $message;
        $d['info'] = $data;

        parent::__construct('event.error', $d);
    }
}