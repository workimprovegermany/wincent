<?php
/**
 * CSV_file
 *
 * for writing data to csv file
 *
 * here some known settings templates:
 *
 * EXCEL-WINMAC: (works with Excel for windows and mac-os)
 *   UTF-16LE with BOM and Tab (chr(9)) as separator
 *
 * SALIA: Software CSV Importer (sql-ag.de)
 *   UTF-8 without BOM, separator ':', Windows CRLF
 *   escape special characters like " and \
 *   those are automatically unescaped when imported
 */

class CSV_file
{
    private $file_name = '';
    private $has_bom = false; // write bom at start of file
    private $encoding = 'UTF-8'; //e.g. UTF-16LE
    private $data = array(); // each line with fields
    private $separator = ','; //e.g. chr(9) for tab
    private $line_break = PHP_EOL; //for windows '\r\n'

    public function __construct($file_name = '')
    {
        $this->file_name = $file_name;
    }

    /**
     * @param bool $has_bom
     */
    public function set_has_bom($has_bom)
    {
        $this->has_bom = $has_bom;
    }

    /**
     * @param string $file_name
     */
    public function set_file_name($file_name)
    {
        $this->file_name = $file_name;
    }

    /**
     * @param string $encoding
     */
    public function set_encoding($encoding)
    {
        $this->encoding = $encoding;
    }

    /**
     * @param array $data
     */
    public function set_data($data)
    {
        $this->data = $data;
    }

    /**
     * @param string $separator
     */
    public function set_separator($separator)
    {
        $this->separator = $separator;
    }

    /**
     * @param string $line_break
     */
    public function set_line_break($line_break)
    {
        $this->line_break = $line_break;
    }

    /**
     * @param string $template
     */
    public function use_settings_template($template)
    {
        switch ($template)
        {
            case 'EXCEL-WINMAC':
                $this->set_encoding('UTF-16LE');
                $this->set_has_bom(true);
                $this->set_separator(chr(9));
                $this->set_line_break(PHP_EOL);
                break;
            case 'SALIA':
                $this->set_encoding('UTF-8');
                $this->set_has_bom(false);
                $this->set_separator(':');
                $this->set_line_break('\r\n');
                break;
            default:
                $this->set_encoding('UTF-8');
                $this->set_has_bom(false);
                $this->set_separator(',');
                $this->set_line_break(PHP_EOL);
        }
    }




    public function write_file()
    {
        $fp = fopen($this->file_name, 'w');

        if ($this->has_bom)
        {
            fwrite($fp, "\xFF\xFE"); //BOM
        }

        foreach ($this->data as $line_array)
        {
            $line = implode($this->separator, $line_array);
            $line_conv = $this->separator != 'UTF-8' ? iconv("UTF-8",$this->encoding, $line) : $line;

            fwrite($fp, $line_conv);
            fwrite($fp, $this->line_break);
        }

        fclose($fp);
    }


}