<?php

define("APP_FOUNDATION_SERVER_PATH", __DIR__);
define("APP_FOUNDATION_PATH", realpath(__DIR__.'/..'));
define("APP_ROOT_PATH", realpath(__DIR__.'/../..'));
define("APP_CONFIG", realpath(APP_ROOT_PATH.'/app_config.xml'));

require_once('autoload/Autoload.php');

$autoloader = new Autoload();
spl_autoload_register([$autoloader, 'load']);

$autoloader->register('Entity_config', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/entity/Entity_config.php');
});
$autoloader->register('Entity_mapper', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/entity/Entity_mapper.php');
});
$autoloader->register('Entity_manager', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/entity/Entity_manager.php');
});
$autoloader->register('Entity_view', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/view/Entity_view.php');
});

$autoloader->register('Event_dispatcher', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/event/Event_dispatcher.php');
});
$autoloader->register('Event_ok', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/event/Event_ok.php');
});
$autoloader->register('Event_error', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/event/Event_error.php');
});
$autoloader->register('Event_notification_interface', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/event/Event_notification_interface.php');
});


$autoloader->register('Storage_adapter', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/storage/Storage_adapter.php');
});
$autoloader->register('Array_storage_adapter', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/storage/Array_storage_adapter.php');
});
$autoloader->register('PDO_storage_adapter', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/storage/PDO_storage_adapter.php');
});


$autoloader->register('URL_Builder', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/url/URL_Builder.php');
});
$autoloader->register('Route', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/router/Route.php');
});


$autoloader->register('Amazon_PA', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/data_provider/Amazon_PA.php');
});


$autoloader->register('Tile', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/view/Tile.php');
});
$autoloader->register('Tile_view', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/view/Tile_view.php');
});

$autoloader->register('Table', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/view/Table.php');
});
$autoloader->register('Table_view', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/view/Table_view.php');
});


$autoloader->register('Send_in_blue', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/api/Send_in_blue.php');
});
$autoloader->register('Mailchimp', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/api/Mailchimp.php');
});
$autoloader->register('Klick_tipp', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/api/Klick_tipp.php');
});
$autoloader->register('Active_campaign', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/api/Active_campaign.php');
});
$autoloader->register('Clever_reach', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/api/Clever_reach.php');
});
$autoloader->register('Get_response', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/api/Get_response.php');
});
$autoloader->register('Paypal', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/api/paypal/Paypal.php');
});


$autoloader->register('Xml_document', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/xml/Xml_document.php');
});

$autoloader->register('HTML_helper', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/view/HTML_helper.php');
});

$autoloader->register('CSV_file', function(){
    return require(APP_FOUNDATION_SERVER_PATH.'/file/CSV_file.php');
});


$config_xml = new Xml_document(APP_ROOT_PATH.'/app_config.xml');
$e = $config_xml->query("/config/events/event");
foreach ($e as $item)
{
    $event = $item->getAttribute('id');
    $action_path = $item->getElementsByTagName('action')[0]->nodeValue;

    $autoloader->register(basename($action_path,'.php'), function() use ($action_path) {
        $path = APP_ROOT_PATH.'/'.$action_path;
        return require($path);
    });
}
