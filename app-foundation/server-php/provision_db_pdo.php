<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$pdo = new PDO_storage_adapter(APP_CONFIG);

$app_config = new Config(APP_CONFIG);
$app_entities = $app_config->entities();

$app_path = APP_ROOT_PATH.$app_config->entry('app_path');
$model_path = $app_path.'/models';

if (!file_exists($model_path)) return;

try {
    //build DB
    foreach ($app_entities as $entity_name)
    {
        $entity_descr = $app_config->entity_description($entity_name);

        $config_xml = new SimpleXMLElement(file_get_contents($model_path.'/'.$entity_descr['config']));
        $fields = $config_xml->xpath('/config/fields/field[type!="many_to_many"]');

        $sql = "CREATE TABLE IF NOT EXISTS `".$entity_descr['table_name']."` (";

        $sql .= "`id` int unsigned NOT NULL AUTO_INCREMENT,";
        $sql .= "`last_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,";

        foreach ($fields as $field)
        {
            //echo "FieldName:".$field->name."<br/>";

            $name = $field->name;
            $type = $field->type;
            $required = ($field->xpath('required') == false) ? '' : ($field->xpath('required')[0]->__toString() == 'true') ? 'NOT NULL' : '';

            $db_type = '';
            $db_config = '';

            if ($type == 'foreign_key')
            {
                $db_type = 'int unsigned';
                $db_config = '';
            }
            elseif ($type == 'timestamp')
            {
                $db_type = 'timestamp';
                $db_config = '';
            }
            elseif ($type == 'json')
            {
                $db_type = 'text';
                $db_config = 'COLLATE utf8_unicode_ci';
            }
            elseif ($type == 'string')
            {
                $db_type = 'varchar(255)';
                $db_config = 'COLLATE utf8_unicode_ci';
            }
            elseif ($type == 'uint')
            {
                $db_type = 'int unsigned';
                $db_config = '';
            }
            elseif ($type == 'bool')
            {
                $db_type = 'bool';
                $db_config = '';
            }
            else
            {
                $db_type = $field->db_type;
                $db_config = $field->db_config;
            }

            $sql .= "`$name` $db_type $db_config $required,";
        }

        $sql .= "PRIMARY KEY (`id`)";
        $sql .= ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

        $ret = $pdo->db_con->exec($sql);
        //print "ret: ".$ret."<br/>";
        //echo "<pre>";
        //print_r($sql);
        //echo "</pre>";

        if (isset($entity_descr['initial_dataset']))
        {
            $initial_file = $model_path.'/'.$entity_descr['initial_dataset'];
            $statement = $pdo->db_con->prepare("SELECT count(*) AS total FROM ".$entity_descr['table_name']);
            $statement->execute();
            $row = $statement->fetch(PDO::FETCH_ASSOC);
            if ($row['total'] == 0)
            {
                $ini_xml = new SimpleXMLElement(file_get_contents($initial_file));
                $xml_entities = $ini_xml->xpath('/*/'.$entity_descr['name']);
                foreach ($xml_entities as $xml_entity)
                {
                    $xml_fields = $xml_entity->xpath('./*');
                    $e = new Entity();
                    foreach ($xml_fields as $xml_field)
                    {
                        $e->set_field($xml_field->getName(), $xml_field[0]);
                    }
                    $em = new Entity_mapper($pdo);
                    $em->set_config(new Entity_config($entity_descr['config']));
                    $result = $em->insert($e);
                }
            }
        }

        // MANY_TO_MANY RELATION TABLES
        $path = '/config/fields/field[type="many_to_many"][owner="' . $entity_descr['name'] . '"]';
        $fields = $config_xml->xpath($path);
        foreach ($fields as $field)
        {
            $table_name = $field->owner[0] . "_" . $field->model[0];
            $sql = "CREATE TABLE IF NOT EXISTS `".$table_name."` (";

            $sql .= "`". $field->owner[0] ."` int unsigned NOT NULL,";
            $sql .= "`". $field->model[0] ."` int unsigned NOT NULL,";

            $sql .= "PRIMARY KEY (`". $field->owner[0] . "`, `" . $field->model[0] ."`)";
            $sql .= ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

            $ret = $pdo->db_con->exec($sql);
        }

    }
} catch (PDOException $e) {
    echo 'DB Connection failed: ' . $e->getMessage();
}

