<?php

abstract class Storage_adapter
{
    public $db_con = null;

    public function __construct()
    {
    }

    /**
     * @param int $id
     * @param string $table
     * @return array|null entry
     */
    abstract public function find($id, $table);

    /**
     * @param array $fields
     * @param string $table
     * @return array|null entry
     */
    abstract public function find_by_fields($fields, $table, $limit = null, $offset = null);

    /**
     * @param array $fields
     * @param string $table
     * @return array|null ids
     */
    abstract public function find_ids_by_fields($fields, $table, $limit = null, $offset = null);

    /**
     * @param array $fields
     * @param string $table
     * @return int count
     */
    abstract public function count_by_fields($fields, $table);

    /**
     * @param string $column
     * @param array $fields
     * @param string $table
     * @return int count
     */
    abstract public function sum_column_by_fields($column, $fields, $table);



    /**
     * @param string $table
     * @param int $id
     * @param array $fields
     * @return mixed
     */
    public function update($table, $id, $fields)
    {
    }

}
