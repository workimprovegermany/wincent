<?php

class URL_Builder
{
    private $url_components;
    private $entity;
    private $parameter_components;
    private $base;

    function __construct($base, $entity, $url_components, $parameter_components)
    {
        $this->url_components = $url_components;
        $this->parameter_components = $parameter_components;
        $this->entity = $entity;
        $this->base = $base;
    }

    public function get_url_for_filters($filter)
    {
        $url = $this->base;
        $is_filtered = false;

        $url .= $this->entity . '/';

        foreach ($this->url_components as $param)
        {
            $fi = Filter::get_filter_by_name($param, $filter);
            if ($fi != false && $fi->has_value)
            {
                if ($is_filtered == true)
                {
                    $url.="-";
                }
                if ($fi->value_type == 'bool')
                {
                    $url .= $fi->name;
                }
                else
                {
                    // Bindestrich in Unterstrich
                    $fv = str_replace("-", "_", $fi->value);
                    $fv2 = rawurlencode($fv);

                    $url .= $fv2;
                }
                $is_filtered = true;
            }
        }

        foreach ($this->parameter_components as $param)
        {
            // only range filter here
            // TODO rest of the code

            $fi = Filter::get_filter_by_name($param, $filter);
            if ($fi != false && $fi->has_value)
            {
                $url .= "?" . $param . "=";
                $url .= $fi->value[0] . "-";
                $url .= $fi->value[1];
                $has_pc = true;
            }
        }

        return $url;
    }

}