<?php

class Table
{
    public $inhalt;
    public $format;
    public $downloadData;

    public $headline1 = null;
    public $headline2 = null;


    public $message_counter = null;

    public function __construct($inhalt, $format, $downloadData, $headline1= null, $headline2= null)
    {
        $this->inhalt = $inhalt;
        $this->format = $format;
        $this->downloadData = $downloadData;

        $this->headline1 = $headline1;
        $this->headline2 = $headline2;
    }

    public function set_message_counter($counter)
    {
        $this->message_counter = $counter;
    }
}