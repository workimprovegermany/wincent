<?php

class Table_view
{
    private $table_data;
    private $table_data_head;

    public function __construct($data, $dataHead)
    {
        $this->table_data = $data;
        $this->table_data_head = $dataHead;
    }

    public function generate_table()
    {

        echo "<table>";
            echo "<thead>";
            echo "</thead>";
            echo "<tbody>";

            $nr = 1;

            foreach ($this->table_data_head as $item)
            {
                $this->generate_table_cell_head($item);
            }

            foreach ($this->table_data as $item)
            {
                $this->generate_table_cell($item, $nr);
                $nr++;
            }
            echo "</tbody>";

        echo "</table>";

    }

    private function generate_table_cell_head(Table $item)
    {
        echo "<tr>";
            echo "<th>Nr.</th>";
            echo "<th>".$item->headline1."</th>";
            echo "<th>".$item->headline2."</th>";
            echo "<th>Download</th>";
        echo "</tr>";
    }

    private function generate_table_cell(Table $item, $nr)
    {
        echo "<tr>";
            echo "<td>$nr</td>";
            echo "<td>".$item->inhalt."</td>";
            echo "<td><div class='btn_02 cursor-default ".$item->format."'><div class='icon_".$item->format." weiss'></div></div></td>";
            echo "<td><a href='".$item->downloadData."' class='btn_02 animate ' download><div class='icon_download weiss'></div></a></td>";
        echo "</tr>";
    }
}