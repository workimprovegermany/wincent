<?php

class Tile
{
    public $href;
    public $title;
    public $icon;
    public $active = null;


    public $message_counter = null;

    public function __construct($href, $title, $icon,  $active = null)
    {
        $this->href = $href;
        $this->title = $title;
        $this->icon = $icon;
        $this->active = $active;
    }

    public function set_message_counter($counter)
    {
        $this->message_counter = $counter;
    }
}