<?php

class Tile_view
{
    private $tile_data;

    public function __construct($data)
    {
        $this->tile_data = $data;
    }

    public function generate_view()
    {
        echo "<section class=\"\">";
        echo "<div class=\"\">";
        echo "<div id='navi-bar' class='size50 icon_navi-bar'></div>";
        echo "<nav>";
        echo "<ul>";

        foreach ($this->tile_data as $item)
        {
            $this->generate_navigation_tile($item);
        }

        echo "</ul>";
        echo "</nav>";
        echo "</div>";
        echo "</section>";

    }

    private function generate_navigation_tile(Tile $item)
    {
        echo "<li class=\"".$item->active."\">";
            echo "<a href=\"".$item->href."\" title=\"".$item->title."\" >";
                echo $item->icon;
                echo "<span>".$item->title."</span>";
            echo "</a>";
        echo "</li>";
    }

    public function generate_view_tile()
    {
        echo "<ul class='kacheln'>";

        foreach ($this->tile_data as $item)
        {
            $this->generate_tile($item);
        }

        echo "</ul>";
    }

    private function generate_tile(Tile $item)
    {
        echo "<a class=\"grid_4\" href=\"".$item->href."\" title=\"".$item->title."\" >";
        echo "<li class=\"flex_center\">";
        echo "<div class=\"weiss ".$item->icon."\"></div>";
        echo "<span>".$item->title."</span>";
        echo "</li>";
        echo "</a>";
    }
}