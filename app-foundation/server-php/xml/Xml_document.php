<?php

class Xml_document
{
    private $file = '';
    private $doc = null;


    public function __construct($file = '')
    {
        if (!empty($file) && file_exists($file))
        {
            $this->file = $file;
            $this->doc = new DOMDocument();
            $this->doc->load($file);
        }
    }

    public function query_string($xpath)
    {
        if (empty($this->doc)) return null;

        $dx = new DOMXPath($this->doc);
        $node = $dx->query($xpath)->item(0);

        if ($node == null) return null;

        return $node->nodeValue;
    }

    public function query($xpath)
    {
        if (empty($this->doc)) return null;

        $dx = new DOMXPath($this->doc);
        $nodes = $dx->query($xpath);

        return $nodes;
    }
}