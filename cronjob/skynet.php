<?php
/**
 * Created by PhpStorm.
 * User: marco
 * Date: 2019-03-05
 * Time: 14:43
 */

class Skynet
{
    public $apiKey = '06263709075bd4836965620ce405004e';
    public $apiUrl = 'https://api.crowddesk.io';
    public $apiAccept = 'application/json';
    public $apiContentType = 'application/json';
    public $apiUserAgent = 'CrowdDesk CMS';

    public $httpcode = null;
    public $error = null;

    public function curl($methodUrl, $call = 'GET', $data = NULL)
    {
        //echo "curl: ".$methodUrl."<br/>";

        if (!$this->apiKey || !$this->apiUrl) return false;

        $url = $this->apiUrl . $methodUrl;
        $ch = curl_init($url);
        if ($call == 'POST') {
            curl_setopt($ch, CURLOPT_POST, 1);
            $data = json_encode($data);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);

        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Token token=' . $this->apiKey,
            'Accept: ' . $this->apiAccept,
            'Content-Type: ' . $this->apiContentType,
            'User-Agent: ' . $this->apiUserAgent));

        curl_setopt($ch, CURLOPT_HEADER, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $header = substr($response, 0, $header_size);
        $body = substr($response, $header_size);
        //echo "#";echo(htmlspecialchars($header));echo "#<br/>";


        $header_lines = explode("\n", $header);
        $header_array = array();
        foreach ($header_lines as $line)
        {
            list($key,$val) = explode(':',$line,2);
            $header_array[strtolower($key)] = trim($val);
        }

        $return = array();
        if (isset($header_array['link']))
        {
            $links = explode(', ', $header_array['link']);

            $link_array = array();
            foreach ($links as $link)
            {
                preg_match('$<(?<link>.*)>; rel="(?<rel>.*)"$', $link, $matches);
                array_shift($matches);
                if (isset($matches['rel']) && $matches['rel'] == 'next') $return['next_link'] = $matches['link'];
            }
        }

        $return['header'] = $header;
        $return['curl_exec'] = $body;
        $return['curl_error'] = curl_error($ch);
        $return['curl_httpcode'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        curl_close($ch);

        usleep(250000);

        //print_r($return['curl_exec']);

        return $return;
    }

    public function get_project_by_id($projectID)
    {
        $return = $this->curl('/v1/projects/'.$projectID, 'GET');

        $this->httpcode = $return['curl_httpcode'];
        $this->error = $return['curl_error'];

        if ($return['curl_httpcode'] != '200') return null;

        return json_decode($return['curl_exec'], true);
    }

    public function get_project_investments($projectID)
    {
        $return = $this->curl('/v1/projects/'.$projectID.'/investments', 'GET');

        $this->httpcode = $return['curl_httpcode'];
        $this->error = $return['curl_error'];

        if ($return['curl_httpcode'] != '200') return null;

        return json_decode($return['curl_exec'], true);
    }

    public function all_to_db($db, $entity_name)
    {
        $ret = $this->curl('/v1/'.$entity_name.'?limit=50');
        $objects = json_decode($ret['curl_exec'], true);
        $object_columns = array_keys($objects[0]);
        array_shift($object_columns);

        $sql = "CREATE TABLE IF NOT EXISTS `skynet_$entity_name` (";
        $sql .= "`id` int unsigned NOT NULL AUTO_INCREMENT,";
        $sql .= "`last_mod` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,";

        foreach ($object_columns as $column)
        {

            $db_type = 'varchar(255)';
            $db_config = 'COLLATE utf8_unicode_ci';

            $sql .= "`$column` $db_type $db_config,";

        }
        $sql .= "PRIMARY KEY (`id`)";
        $sql .= ") ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

        $sql_ret = $db->db_con->exec($sql);

        foreach ($objects as $object)
        {
            // save array fields as json
            foreach ($object as $k=>$v)
            {
                if (is_array($v))
                {
                    //$object[$k] = json_encode($v,JSON_UNESCAPED_UNICODE);
                    $object[$k] = $v['id'];

                    echo "<br/><br/>array detected<br/><br/>";
                    echo "array";print_r($v);echo "<br/>";
                    echo "json: ".json_encode($v)."<br/>";
                }
            }

            $db->insert($object, 'skynet_'.$entity_name);
            echo "insert project";
            print_r($object);
        }

        echo "next_link".$ret['next_link'];

        while ($ret['next_link'])
        {
            $method = substr($ret['next_link'],24);

            $ret = $this->curl($method);
            $objects = json_decode($ret['curl_exec'], true);
            foreach ($objects as $object)
            {
                // save array fields as json
                foreach ($object as $k=>$v)
                {
                    if (is_array($v))
                    {
                        //$object[$k] = json_encode($v,JSON_UNESCAPED_UNICODE);
                        $object[$k] = $v['id'];

                        echo "<br/><br/>array detected<br/><br/>";
                        echo "array";print_r($v);echo "<br/>";
                        echo "json: ".json_encode($v)."<br/>";
                    }
                }

                $db->insert($object, 'skynet_'.$entity_name);
                echo "insert project";
                print_r($object);            }
        }

        echo "<hr>";
    }

    public function update_table($db, $entity_name)
    {
        // TODO: 1.) skynet doesn't support filtering at the moment.
        //           so for updates we have to load all data and compare with db to detect changes.
        //       2.) detect also changes in tablecolumns

        $ret = $this->curl('/v1/'.$entity_name.'?limit=50');
        $objects = json_decode($ret['curl_exec'], true);

        foreach ($objects as $object)
        {
            // save array fields as json
            foreach ($object as $k=>$v)
            {
                if (is_array($v))
                {
                    $object[$k] = json_encode($v);
                }
            }

            // 1. load obj from db
            $config_path = 'skynet_'.$entity_name.'/1.0/config.xml';
            $em = new Entity_mapper($db, $config_path);
            $obj_db = $em->find_by_id($object['id']);
            if ($obj_db !== null)
            {
                // 2. compare with obj
                $changes = $obj_db->compare_with_fields($object);

                // 3. update fields and set date
                if ($changes !== null)
                {
                    $db->update('skynet_'.$entity_name, $object['id'], $changes);
                }

            }
            else
            {
                //Object not found -> new Object
                $db->insert($object, 'skynet_'.$entity_name);
            }


            $db->update('skynet_'.$entity_name, $object['id'], $object);
            //echo "update object";
            //print_r($object);
        }

        //echo "next_link".$ret['next_link'];

        while ($ret['next_link'])
        {
            $method = substr($ret['next_link'],24);

            $ret = $this->curl($method);
            $objects = json_decode($ret['curl_exec'], true);
            foreach ($objects as $object)
            {
                $db->update('skynet_'.$entity_name, $object['id'], $object);
            }
        }

        //echo "<hr>";
    }

}