<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

require_once('../app-foundation/server-php/foundation.php');

$app_config = new Config(APP_CONFIG);
$app_name = $app_config->entry('app_name').'App';
require_once(APP_ROOT_PATH.'/'.$app_name.'.php');
$app = $app_name::get_instance();

echo "<pre>";

require_once ("skynet.php");
$rc = new Skynet();

$rc->update_table($app->db(),'projects');
$rc->update_table($app->db(),'emissions');
$rc->update_table($app->db(),'issuers');
$rc->update_table($app->db(),'investors');
$rc->update_table($app->db(),'investments');
$rc->update_table($app->db(),'affiliates');
$rc->update_table($app->db(),'affiliate_conversions');
$rc->update_table($app->db(),'bonus_code_entries');

