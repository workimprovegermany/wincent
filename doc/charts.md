## Übersicht Daten Charts

### Dashboard

#### Investment 
* Zeitraum: letzen 6 Monate 
* State: running

#### Gesamtzahlungen 
* Zeitraum: letzen 6 Monate 
* State: running & pending

#### Zahlungen 2019 
* Zeitraum: ab Januar bis aktueller Monat
* State: running

#### Investment große Übersicht
* Wert grün
  * Zeitraum: letzen 7 Tage 
  * State: running
  
* Wert rot gestrichelt
  * Zeitraum: letzen 7 Tage 
  * State: canceld
  
* Wert grün gestrichelt
  * Zeitraum: letzen 7 Tage 
  * State: pending
  
#### Investoren
* Zeitraum: von Beginn
* State: --

#### Eigenumsatz
* Zeitraum: von Beginn
* Aus Abrechnung

#### Partnerumsatz
* Zeitraum: von Beginn
* Aus Abrechnung


##Statistik

###Investment
* Zeitraum: ab Beginn
* Aus Abrechnung
* Was Kunden gezahlt haben
* Eigenumsatz + Umsatz aus Struktur

### Gesamtzahlungen
* Zeitraum: ab Beginn
* Alle Zahlungen die User erhalten hat
* Aus Abrechnung

### Gesamtzahlungen aktuelles Jah
* Zeitraum: ab Beginn
* Alle Zahlungen die User erhalten hat
* Aus Abrechnung

### Aktuell Brutto
* Was aktuell offen ist
* Nicht abgerechnet, nicht verprovioniert und nicht ausgezahlt

### Aktuell Netto
* Was aktuell offen ist
* Kunde hat bezahlt und es kommt zur Abrechnung, ist nur noch nicht abgerechnet

