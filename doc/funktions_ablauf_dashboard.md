# Ablauf Funktionen Dashboard

##Admin
*Init-Funktion:* initDashboard()
1. get_data_for_chart();
2. set_data_for_chart();
3. get_data_last_seven_days();
4. process_data_for_investment_all_state();
5. get_number_investors();
6. *process_number_investors()*;
7. get_site_visitors();
8. set_site_visitors();

##User
*Init-Funktion:* initDashboard()
1. get_data_for_chart();
2. set_data_for_chart();
3. get_data_last_seven_days();
4. process_data_for_investment_all_state();
5. get_number_investors();
6. *process_number_investors_user()*;
7. get_site_visitors();
8. set_site_visitors();