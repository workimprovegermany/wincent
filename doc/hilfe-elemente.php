<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DOC - HILFE Elemente</title>
    <link rel="icon" type="image/x-icon" href="../img/icon.ico">

    <link rel="stylesheet" href="../app-foundation/css/dist/af.css">

    <script src="../vendor/js/other/01-jquery-3.3.1.min.js"></script>
    <script src="../vendor/js/other/jquery.mask.js"></script>
    <script src="../app-foundation/js/dist/af.min.js"></script>

    <style type="text/css">
        figure {
            background-color: #f8f9fa;
            padding: 1.5rem;
            margin: 0;
        }

        figure pre {
            padding: 0;
            margin-top: 0;
            margin-bottom: 0;
            background-color: transparent;
            border: 0;
            display: block;
            font-size: 87.5%;
            overflow: auto;
        }

        figure pre code {
            word-break: normal;
        }

        .nt {
            color: #2f6f9f;
        }

        .na {
            color: #4f9fcf;
        }

        .s {
            color: #d44950;
        }

        .btn-clipboard {
            position: absolute;
            top: .5rem;
            right: .5rem;
            z-index: 10;
            display: block;
            padding: .25rem .5rem !important;
            font-size: 75% !important;
            color: #818a91 !important;
            cursor: pointer !important;
            background-color: transparent !important;
            border: 0 !important;
            border-radius: .25rem !important;
        }

        .btn-clipboard:hover {
            color: #fff;
            background-color: #027de7;
        }

        .bilder > [class*="grid_"] {
            padding: 20px;
            border: 1px solid #ccc;
            border-left: none;
            background-color: #f1f1f1;
        }

        .bilder > [class*="grid_"]:nth-child(4n-3) {
            border-left: 1px solid #ccc;
        }

        ul {
            list-style-type: none;
            padding-left: 30px;
        }

        li {
            padding-bottom: 15px;
        }

        .container-fluid {
            width: 100%;
        }

        @media (max-width: 767px) {
            nav {
                display: none;
            }

            .container-fluid {
                width: 80%;
                margin-left: 10%;
            }
        }

        h3 {
            margin-top: 1em;
        }

        .btn_02 {
            display: inline-block;
            margin: 10px 0;
        }

    </style>

</head>
<body>
<header class="center">
    <h2>Übersicht Elemente</h2>
    <h3>Hier sind alle Elemente von WinCent aufgeführt.</h3>
</header>
<main class="marg150 extern">
    <div class="container-fluid">
        <div class="grid_2">
            <div style="position: fixed;">
                <nav>
                    <ul>
                        <li>
                            <a href="#ueberschriften">Überschriften</a>
                        </li>
                        <li>
                            <a href="#text">Text</a>
                        </li>
                        <li>
                            <a href="#variablen">Variablen</a>
                        </li>
                        <li>
                            <a href="#formular">Formular</a>
                        </li>
                        <li>
                            <a  class="small80" href="#anchor_input"> - Input</a>
                        </li>
                        <li>
                            <a  class="small80" href="#anchor_checkbox"> - Checkbox</a>
                        </li>
                        <li>
                            <a  class="small80" href="#anchor_radio"> - Radiobutton</a>
                        </li>
                        <li>
                            <a  class="small80" href="#anchor_button"> - Button</a>
                        </li>
                        <li>
                            <a href="#elemente">Weitere Elemente</a>
                        </li>
                        <li>
                            <a  class="small80" href="#btn_download"> - Download Button</a>
                        </li>
                        <li>
                            <a href="#charts">Diagramme</a>
                        </li>
                        <li>
                            <a href="#bilder">Aktuelle Bilder</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="grid_8">
            <section class="pad150" id="ueberschriften">
                <h2>Überschriften</h2>
                <hr>
                <h1>h1: Überschrift</h1>
                <h2>h2: Überschrift</h2>
                <h3>h3: Überschrift</h3>
                <h4>h4: Überschrift</h4>
            </section>

            <section class="pad150" id="text">
                <h2>Text</h2>
                <hr>
                <p>Normaler Text</p>
                <p class="small80">Textgröße 80% (class: small80)</p>
                <p class="small60">Textgröße 60% (class: small60)</p>
            </section>

            <section class="pad150" id="variablen">
                <h2>Variablen</h2>
                <hr>
                <h3>Farben</h3>
                <p><b>$weiss</b> <span class="weiss" style="background-color: #000;">Weiß</span></p>
                <p><b>$schwarz</b> <span class="schwarz">Schwarz</span></p>
                <p><b>$fontcolor</b> Textfarbe</p>
                <p><b>$highlightFont1</b> <span class="highlightFont1">Highlight Text 1 </span> (class : highlightFont1)</p>
                <p><b>$highlightFont2</b> <span class="highlightFont2">Highlight Text 2 </span> (class : highlightFont2)</p>

                <h3>Grautöne</h3>
                <p><b>$grau95</b>&nbsp;&nbsp;&nbsp;<span style="background-color: hsl(0, 0%, 95%);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                <p><b>$grau90</b>&nbsp;&nbsp;&nbsp;<span style="background-color: hsl(0, 0%, 90%);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                <p><b>$grau80</b>&nbsp;&nbsp;&nbsp;<span style="background-color: hsl(0, 0%, 80%);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                <p><b>$grau65</b>&nbsp;&nbsp;&nbsp;<span style="background-color: hsl(0, 0%, 65%);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                <p><b>$grau30</b>&nbsp;&nbsp;&nbsp;<span style="background-color: hsl(0, 0%, 30%);">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>

                <h3>Links</h3>
                <p><b>$linkcolor</b> <a href="">Linkfarbe</a></p>
                <p><b>$highlightLink</b> <a href="" class="highlightLink">Highlight Linkfarbe</a> (class : highlightLink)</p>

                <h3>Buttons</h3>
                <p><button type="button" class="btn_0" style="margin-top: 0;">Normaler Button</button> <b>$buttoncolor</b>  (class : btn_0)</p>
                <p><button class="btn_01" type="button">Highlight Button</button> <b>$highlightButton</b> (class : btn_01)</p>

                <h3>Header & Navi</h3>
                <p><b>$headercolor</b>&nbsp;&nbsp;&nbsp;<span class="headercolor">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                <p><b>$navicolor</b>&nbsp;&nbsp;&nbsp;<span class="navicolor">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                <p><b>$navicolorActive</b>&nbsp;&nbsp;&nbsp;<span class="navicolorActive" ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                <p><b>$navicolorSettings</b>&nbsp;&nbsp;&nbsp;<span class="navicolorSetting">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                <p><b>$navicolorSettingsActive</b>&nbsp;&nbsp;&nbsp;<span class="navicolorSettingsActive">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>

                <h3>Weitere</h3>
                <p><b>$basiccolorIntern</b>&nbsp;&nbsp;&nbsp;<span class="basiccolorIntern">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
                <p><svg height="50px" fill="#000000" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><g><path d="M142.129,459.057h223.688c27.57,0,50-22.43,50-50V285.798l11.886,9.005c1.802,1.365,3.925,2.03,6.033,2.03   c2.846,0,5.665-1.209,7.637-3.538l25.566-30.188c5.343-6.308,7.809-14.31,6.942-22.532c-0.866-8.222-4.945-15.535-11.486-20.591   l-49.996-38.653V62.943c0-5.523-4.478-10-10-10h-57.796c-5.522,0-10,4.477-10,10v58.244l-54.138-41.855   c-14.404-11.136-34.527-11.136-48.932,0L49.604,219.985c-6.541,5.057-10.62,12.369-11.486,20.591   c-0.866,8.222,1.6,16.224,6.942,22.532l25.566,30.188c3.433,4.053,9.437,4.716,13.67,1.508l7.832-5.934v120.188   C92.129,436.627,114.559,459.057,142.129,459.057z M291.352,439.057h-70.048v-73.009c0-16.542,13.458-30,30-30h10.048   c16.542,0,30,13.458,30,30V439.057z M395.817,409.057c0,16.542-13.458,30-30,30h-54.466v-73.009c0-27.57-22.43-50-50-50h-10.048   c-27.57,0-50,22.43-50,50v73.009h-59.175c-16.542,0-30-13.458-30-30V273.716l131.793-99.855c7.11-5.388,17.044-5.389,24.156,0   l127.739,96.784V409.057z M392.399,72.943v20h-37.796v-20H392.399z M354.604,112.943h37.796v52.927l-37.796-29.221V112.943z    M79.768,273.142l-19.445-22.96c-1.781-2.103-2.603-4.77-2.314-7.51c0.289-2.741,1.648-5.178,3.829-6.864l181.93-140.653   c7.203-5.569,17.266-5.568,24.467,0l181.93,140.654c2.181,1.686,3.54,4.123,3.829,6.864c0.288,2.74-0.533,5.407-2.314,7.51   l-19.445,22.96L280.156,157.919c-7.111-5.388-15.634-8.082-24.156-8.082s-17.045,2.694-24.156,8.082L79.768,273.142z"></path></g></svg>
                    <b style="vertical-align: super;">$iconcolor</b>
                </p>
                <p><div style="background-color: black" class="size40 icon_user"></div> <b style="vertical-align: super;">$iconcolorIntern</b>
                </p>
            </section>

            <section class="pad150" id="formular">
                <h2>Formular</h2>
                <hr>
                <h3 id="anchor_input">Input</h3>
                <p class="small80">! Die äußeren div-Tags sind hier immer mit grid_12 und einem margin (bsp.: marg150) versehen. Diese müssen individuell angepasst werden. <br>Die anderen Klassen müssen beibehalten werden.</p>

                <fieldset class="marg150">
                    <legend>Label & Input:</legend>
                    <div class="grid_6 flex_center marg150">
                        <div class="grid_3">
                            <label for="[input-id]">Label Text</label>
                        </div>
                        <div class="grid_9">
                            <input type="text" name="[input-name]" id="[input-id]" value="[input-value]" placeholder="[input-placeholder]"/>
                        </div>
                    </div>

                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_12 marg150 flex_center"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_3"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;label</span> <span class="na">for=</span><span class="s">"[input-id]"</span><span class="nt">&gt;</span>Label Text<span class="nt">&lt;/label&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_9"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">name=</span><span class="s">"[input-name]"</span> <span class="na">id=</span><span class="s">"[input-id]"</span> <span class="na">value=</span><span class="s">"[input-value]"</span> <span class="na">placeholder=</span><span class="s">"[input-placeholder]"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

                <fieldset class="marg150">
                    <legend>Label & Select:</legend>
                    <div class="grid_6 marg150 flex_center">
                        <div class="grid_3">
                            <label for="[select-id]">Label Text</label>
                        </div>
                        <div class="grid_9">
                            <select id="[select-id]" name="[select-name]">
                                <option value="0">Wert 1</option>
                                <option value="1">Wert 2</option>
                                <option value="2">Wert...</option>
                            </select>
                        </div>
                    </div>

                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_12 marg150 flex_center"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_3"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;label</span> <span class="na">for=</span><span class="s">"[select-id]"</span><span class="nt">&gt;</span>Label Text<span class="nt">&lt;/label&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_9"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;select</span> <span class="na">name=</span><span class="s">"[select-name]"</span> <span class="na">id=</span><span class="s">"[select-id]"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;option <span class="na">value=</span><span class="s">"[select-value-1]"</span>></span>Wert 1<span class="nt">&lt;/option&gt;</span>
            <span class="nt">&lt;option <span class="na">value=</span><span class="s">"[select-value-2]"</span>></span>Wert 2<span class="nt">&lt;/option&gt;</span>
            <span class="nt">&lt;option <span class="na">value=</span><span class="s">"[select-value-3]"</span>></span>Wert ...<span class="nt">&lt;/option&gt;</span>
        <span class="nt">&lt;/select&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

                <fieldset class="marg300">
                    <legend>E-Mail:</legend>
                    <div class="grid_6 marg150 input_email">
                        <input type="text" name="email-benutzer" class="float-left" autocomplete="off" aria-autocomplete="none"/>
                        <input type="text" class="label_email float-left" name="email-domain" autocomplete="off" aria-autocomplete="none"/>
                        <div></div>
                    </div>
                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_12 marg150 input_email"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">name=</span><span class="s">"email-benutzer"</span> <span class="na">class=</span><span class="s">"float-left"</span> <span class="na">autocomplete=</span><span class="s">"off"</span> <span class="na">aria-autocomplete=</span><span class="s">"none"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">name=</span><span class="s">"email-domain"</span> <span class="na">class=</span><span class="s">"float-left label_email"</span> <span class="na">autocomplete=</span><span class="s">"off"</span> <span class="na">aria-autocomplete=</span><span class="s">"none"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div&gt;</span><span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

                <fieldset class="marg300">
                    <legend>Label & E-Mail:</legend>
                    <div class="grid_6 marg150 flex_center">
                        <div class="grid_3">
                            <label for="email">E-Mail</label>
                        </div>
                        <div class="grid_9 input_email">
                            <input type="text" name="email-benutzer" class="float-left" autocomplete="off" aria-autocomplete="none">
                            <input type="text" name="email-domain" class="float-left label_email" autocomplete="off" aria-autocomplete="none">
                            <div></div>
                        </div>
                    </div>
                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_12 marg150 flex_center"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_3"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;label</span> <span class="na">for=</span><span class="s">"email"</span><span class="nt">&gt;</span>E-Mail<span class="nt">&lt;/label&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_9 input_email"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">name=</span><span class="s">"email-benutzer"</span> <span class="na">class=</span><span class="s">"float-left"</span> <span class="na">autocomplete=</span><span class="s">"off"</span> <span class="na">aria-autocomplete=</span><span class="s">"none"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">name=</span><span class="s">"email-domain"</span> <span class="na">class=</span><span class="s">"float-left label_email"</span> <span class="na">autocomplete=</span><span class="s">"off"</span> <span class="na">aria-autocomplete=</span><span class="s">"none"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;div&gt;</span><span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

                <fieldset class="marg300">
                    <legend>Straße & Nr.:</legend>
                    <div class="grid_6 marg150 flex_center">
                        <div class="grid_3">
                            <label>Straße / Nr.</label>
                        </div>
                        <div class="grid_9 input_strasse_nr">
                            <input type="text" name="strasse" id="strasse" value="" placeholder="Straße" required>
                            <input type="text" name="hausnr" id="hausnr" value="" placeholder="Hausnr." required>
                        </div>
                    </div>
                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_12 marg150 flex_center"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_3"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;label</span><span class="nt">&gt;</span>Straße / Nr.<span class="nt">&lt;/label&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_9 input_strasse_nr"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">name=</span><span class="s">"strasse"</span> <span class="na">id=</span><span class="s">"strasse"</span> <span class="na">value=</span><span class="s">""</span> <span class="na">placeholder=</span><span class="s">"Straße"</span> <span class="nt">&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">name=</span><span class="s">"hausnr"</span> <span class="na">id=</span><span class="s">"hausnr"</span> <span class="na">value=</span><span class="s">""</span> <span class="na">placeholder=</span><span class="s">"Hausnr."</span> <span class="nt">&gt;</span>
        <span class="nt">&lt;div&gt;</span><span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

                <fieldset class="marg300">
                    <legend>PLZ & Ort:</legend>
                    <div class="grid_6 marg150 flex_center">
                        <div class="grid_3">
                            <label>PLZ / Ort</label>
                        </div>
                        <div class="grid_9 input_plz_ort">
                            <input type="text" name="plz" id="plz" value="" placeholder="PLZ" maxlength="5" required>
                            <input type="text" name="ort" id="ort" value="" placeholder="Ort" required>
                            <div></div>
                        </div>
                    </div>
                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_12 marg150 flex_center"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_3"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;label</span><span class="nt">&gt;</span>PLZ / Ort<span class="nt">&lt;/label&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_9 input_plz_ort"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">name=</span><span class="s">"plz"</span> <span class="na">id=</span><span class="s">"plz"</span> <span class="na">value=</span><span class="s">""</span> <span class="na">placeholder=</span><span class="s">"PLZ"</span> <span class="nt">&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">name=</span><span class="s">"ort"</span> <span class="na">id=</span><span class="s">"ort"</span> <span class="na">value=</span><span class="s">""</span> <span class="na">placeholder=</span><span class="s">"Ort"</span> <span class="nt">&gt;</span>
        <span class="nt">&lt;div&gt;</span><span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

                <fieldset class="marg300">
                    <legend>Telefon:</legend>
                    <div class="grid_6 marg150 flex_center">
                        <div class="grid_12 marg150 flex_center">
                            <div class="grid_3">
                                <label for="mobil">Mobil</label>
                            </div>
                            <div class="grid_9 input_tel">
                                <input type="text" name="mobil" id="mobil" value="" placeholder="Mobil" required>
                                <div></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_12 marg150 flex_center"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_3"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;label</span> <span class="na">for=</span><span class="s">"mobil"</span><span class="nt">&gt;</span>Mobil<span class="nt">&lt;/label&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_9 input_tel"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">name=</span><span class="s">"mobil"</span> <span class="na">id=</span><span class="s">"mobil"</span> <span class="na">value=</span><span class="s">""</span> <span class="na">placeholder=</span><span class="s">"Mobil"</span> <span class="nt">&gt;</span>
        <span class="nt">&lt;div&gt;</span><span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

                <fieldset class="marg300">
                    <legend>IBAN:</legend>
                    <div class="grid_6 marg150 flex_center">
                        <div class="grid_3">
                            <label for="iban">IBAN</label>
                        </div>
                        <div class="grid_9">
                            <input type="text" name="iban" id="iban" class="js-iban-format uppercase" value="" placeholder="IBAN" required>
                        </div>
                    </div>
                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_12 marg150 flex_center"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_3"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;label</span> <span class="na">for=</span><span class="s">"iban"</span><span class="nt">&gt;</span>IBAN<span class="nt">&lt;/label&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_9"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">name=</span><span class="s">"iban"</span> <span class="na">id=</span><span class="s">"iban"</span> <span class="na">class=</span><span class="s">"js-iban-format uppercase"</span> <span class="na">value=</span><span class="s">""</span> <span class="na">placeholder=</span><span class="s">"IBAN"</span> <span class="nt">&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

                <fieldset class="marg300">
                    <legend>Steuernr.:</legend>
                    <div class="grid_6 marg150 flex_center">
                        <div class="grid_3">
                            <label for="steuernr">Steuernummer</label>
                        </div>
                        <div class="grid_9 input_steuernr">
                            <select id="steuernr_art" name="steuernr_art">
                                <option value="steuernr">Steuernummer</option>
                                <option value="ustnr">USt-IdNr.</option>
                            </select>
                            <input type="text" name="steuernr" id="steuernr" value="<?php echo $steuernr; ?>" placeholder="Nummer" required>
                        </div>
                    </div>
                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_12 marg150 flex_center"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_3"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;label</span> <span class="na">for=</span><span class="s">"steuernr"</span><span class="nt">&gt;</span>Steuernummer<span class="nt">&lt;/label&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_9 input_steuernr"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;select</span> <span class="na">name=</span><span class="s">"steuernr_art"</span> <span class="na">id=</span><span class="s">"steuernr_art"</span><span class="nt">&gt;</span>
            <span class="nt">&lt;option <span class="na">value=</span><span class="s">"steuernr"</span>></span>Steuernummer<span class="nt">&lt;/option&gt;</span>
            <span class="nt">&lt;option <span class="na">value=</span><span class="s">"ustnr"</span>></span>USt-IdNr.<span class="nt">&lt;/option&gt;</span>
        <span class="nt">&lt;/select&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">name=</span><span class="s">"steuernr"</span> <span class="na">id=</span><span class="s">"steuernr"</span> <span class="na">value=</span><span class="s">""</span> <span class="na">placeholder=</span><span class="s">"Nummer"</span> <span class="nt">&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

                <fieldset class="marg300">
                    <legend>Individuell:</legend>
                    <div class="grid_6 marg150 flex_center">
                        <div class="grid_12 marg150 flex_center">
                            <div class="grid_3">
                                <label for="[id-input]">Text Label</label>
                            </div>
                            <div class="grid_9 input_individuell">
                                <input type="text" name="[name-input]" id="[id-input]" value="" placeholder="Text" required>
                                <div data-content="%"></div>
                            </div>
                        </div>
                    </div>
                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_12 marg150 flex_center"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_3"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;label</span> <span class="na">for=</span><span class="s">"[id-input]"</span><span class="nt">&gt;</span>Text Label<span class="nt">&lt;/label&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_9 input_individuell"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"text"</span> <span class="na">name=</span><span class="s">"[name-input]"</span> <span class="na">id=</span><span class="s">"[id-input]"</span> <span class="na">value=</span><span class="s">""</span> <span class="na">placeholder=</span><span class="s">"Text"</span> <span class="nt">&gt;</span>
        <span class="nt">&lt;div <span class="na">data-content=</span><span class="s">"%"</span>&gt;</span><span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

                <h3 id="anchor_checkbox">Checkbox</h3>

                <fieldset class="marg300">
                    <legend>Normale Checkbox: </legend>
                    <div class="grid_12 marg150 flex_center">
                        <input type="checkbox" name="checkbox-name" id="checkbox-id">
                        <label for="checkbox-id"> Label Text</label>
                    </div>
                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_12 marg150 flex_center"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"checkbox"</span> <span class="na">name=</span><span class="s">"[checkbox-name]"</span> <span class="na">id=</span><span class="s">"[checkbox-id]"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;label</span> <span class="na">for=</span><span class="s">"[checkbox-id]"</span><span class="nt">&gt;</span>Label Text<span class="nt">&lt;/label&gt;</span>
<span class="nt">&lt;/div&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

                <h3 id="anchor_radio">Radiobutton</h3>

                <fieldset class="marg300">
                    <legend>Normaler Radiobutton: </legend>
                    <div class="grid_12 marg150 flex_center">
                        <div class="inline-block relative">
                            <input type="radio" name="video" id="video1" value="1" checked>
                            <label for="video1">Radio 1</label>
                        </div>
                        <div class="inline-block relative">
                            <input type="radio" name="video" id="video2" value="2">
                            <label for="video2">Radio 2</label>
                        </div>
                    </div>
                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"grid_12 marg150 flex_center"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"inline-block relative"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"radio"</span> <span class="na">name=</span><span class="s">"[radio-name]"</span> <span class="na">id=</span><span class="s">"[radio-id-1]"</span> <span class="na">value=</span><span class="s">"[radio-value-1]"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;label</span> <span class="na">for=</span><span class="s">"[radio-id-1]"</span><span class="nt">&gt;</span>Label Text<span class="nt">&lt;/label&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"inline-block relative"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;input</span> <span class="na">type=</span><span class="s">"radio"</span> <span class="na">name=</span><span class="s">"[radio-name]"</span> <span class="na">id=</span><span class="s">"[radio-id-2]"</span> <span class="na">value=</span><span class="s">"[radio-value-2]"</span><span class="nt">&gt;</span>
        <span class="nt">&lt;label</span> <span class="na">for=</span><span class="s">"[radio-id-2]"</span><span class="nt">&gt;</span>Label Text<span class="nt">&lt;/label&gt;</span>
    <span class="nt">&lt;/div&gt;</span>
<span class="nt">&lt;/div&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

                <h3 id="anchor_button">Button</h3>

                <fieldset class="marg300">
                    <legend>Submit Button: </legend>
                    <div class="grid_12 marg150">
                        <button type="button" name='btn_submit' class="btn_03 js-btn-demo">
                            <span class="content">Speichern</span>
                            <span class="progress"><span></span></span>
                        </button>
                    </div>
                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;button</span> <span class="na">type=</span><span class="s">"submit"</span> <span class="na">name=</span><span class="s">"btn_submit"</span> <span class="na">class=</span><span class="s">"btn_03 float-right"</span><span class="nt">&gt;</span>
    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"content"</span><span class="nt">&gt;</span>Speichern<span class="nt">&lt;/span</span><span class="nt">&gt;</span>
    <span class="nt">&lt;span</span> <span class="na">class=</span><span class="s">"progress"</span><span class="nt">&gt;</span><span class="nt">&lt;span</span><span class="nt">&gt;</span><span class="nt">&lt;/span</span><span class="nt">&gt;</span><span class="nt">&lt;/span</span><span class="nt">&gt;</span>
<span class="nt">&lt;/button&gt;</span>
</code></pre>
                        </figure>
                    </div>
                    <div class="grid_12 marg150">
                        <p>Button Status</p>
                        <div class="grid_4">
                            <p class="small80">Status: laden</p>
                            <button type="button" name='btn_submit' class="btn_03 state-loading" disabled>
                                <span class="content">Speichern</span>
                                <span class="progress"><span></span></span>
                            </button>
                        </div>
                        <div class="grid_4">
                            <p class="small80">Status: erfolgreich</p>
                            <button type="button" name='btn_submit' class="btn_03 state-success" disabled>
                                <span class="content">Speichern</span>
                                <span class="progress"><span></span></span>
                            </button>
                        </div>
                        <div class="grid_4">
                            <p class="small80">Status: fehler</p>
                            <button type="button" name='btn_submit' class="btn_03 state-error" disabled>
                                <span class="content">Speichern</span>
                                <span class="progress"><span></span></span>
                            </button>
                        </div>

                    </div>
                </fieldset>

            </section>

            <section class="pad150" id="elemente">
                <h2>Weitere Elemente</h2>
                <hr>

                <fieldset class="marg300" id="btn_download">
                    <legend>Download Button: </legend>
                    <div class="grid_12 marg150">
                        <a href="" class="btn_02 animate "><div class="icon_download weiss"></div></a>
                    </div>
                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;a</span> <span class="na">href=</span><span class="s">"[pfad/datei]"</span> <span class="na">class=</span><span class="s">"btn_02 animate"</span> <span class="na">download</span> <span class="nt">&gt;</span>
    <span class="nt">&lt;div</span> <span class="na">class=</span><span class="s">"icon_download weiss"</span><span class="nt">&gt;</span> <span class="nt">&lt;/div</span><span class="nt">&gt;</span>
<span class="nt">&lt;/a&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

            </section>

            <section class="pad150" id="charts">
                <h2>Diagramme</h2>
                <hr>

                <fieldset class="marg300">
                    <legend>Line Chart:</legend>
                    <div class="grid_6 marg150 flex_center">
                        <canvas class="filledLineChart chartcolor" data-chart="20,30,40,30,25,40,45"></canvas>
                    </div>
                    <div class="grid_12">
                        <button type="button" class="btn-clipboard" title="" data-original-title="Copy to clipboard">Copy</button>
                        <figure>
                        <pre>
<code class="language-html" data-lang="html">
<span class="nt">&lt;canvas</span> <span class="na">class=</span><span class="s">"filledLineChart chartcolor"</span> <span class="na">data-chart=</span><span class="s">"20,30,40,30,25,40,45"</span><span class="nt">&gt;</span>
</code></pre>
                        </figure>
                    </div>
                </fieldset>

            </section>

            <section class="pad150" id="bilder">
                <h2>Aktuelle Bilder</h2>
                <hr>
                <h3>Ordner &quot;img&quot;</h3>
                <div class="grid_12 bilder flex">
                    <?php

                    $path = "../img";
                    $dir = opendir($path);
                    $extensions = array("jpg", "bmp", "gif", "jpeg", "png");

                    while(($file = readdir($dir)) !== false)
                    {
                        if(in_array(pathinfo($file, PATHINFO_EXTENSION), $extensions))
                        {
                            echo "<div class='grid_3'>";
                            echo "<p>".$file."</p>";
                            echo  "<img src='".$path."/".$file."'>";
                            echo "</div>";
                        }
                    }
                    ?>
                </div>


            </section>
        </div> <!-- ENDE grid_9 -->

    </div><!-- ENDE container-fluid -->
</main>
<script type="text/javascript">

    var elements = document.getElementsByClassName('btn-clipboard');
    for(var i = 0; i < elements.length; i++){
        elements[i].addEventListener('click', function (ev) {
            copyToClipboard(ev.target.parentElement.getElementsByTagName('pre')[0].textContent);
        })
    }

    function copyToClipboard(copyValue) {
        const textArea = document.createElement('textarea');
        textArea.textContent = copyValue;
        document.body.append(textArea);
        textArea.select();
        document.execCommand("copy");
        textArea.parentNode.removeChild(textArea);

    }

    $('button.js-btn-demo').on('click', function (ev) {
        setButtonStateLoading();
        setTimeout(function(){
            setButtonStateSuccess();
        }, 3000);
    });

    /*$(".twoLineChart").each(function (ev) {
        setTwoLineChart(this, $(this).data('chart'), $(this).data('chart2'));
    });

    $(".filledLineChart").each(function (ev) {
        setFilledLineChart(this, $(this).data('chart'));
    });

    $(".doughnutChart").each(function (ev) {
        setDoughnutChart(this, $(this).data('chart'));
    });*/

</script>
</body>
</html>