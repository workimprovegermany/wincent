# Rechteverwaltung

## RBAC (Role Based Access System)

Wir nutzen RBAC, um zu verwalten, welcher User was darf.
Es besteht aus den folgenden Komponenten:

* Rolle: eine Arbeitsaufgabe oder Titel, der die Kompetenzen eines Users beschreibt
* Recht: die Möglichkeit eine Klasse von Funktionen anzuzeigen oder zu ändern
* Operation: eine Aktion im System, die möglicherweise einige Rechte benötigt, um ausführbar zu sein

Ein User bekommt eine oder mehrere Rollen. Rollen geben dem User einen Satz Rechte.
Rechte sorgen dafür, dass bestimmte Aktionen im System für diesen User verfügbar sind. 

## Warum RBAC

* folgt natürlichen Organisationsstrukturen
* dadurch einfacher zu konfigurieren
* Einzelrechte schwieriger zu konfigurieren (z.B. mit ACL Access Control List), wenn eine Person die Abteilung wechselt
* wenn eine neue Funktion hinzukommt, muss das Recht nur in den Rollen eingetragen und nicht jeder User editiert werden.

## richtige Implementation

### Rechte prüfen, keine Rollen

Im Quellcode sollte sich nie eine Abfrage auf eine Rolle finden.

z.B. 
* if (user->isAdmin()) {...} 
* if ((user->role == 'admin' || user->role == 'sales')) {..}

Stattdessen immer auf Rechte prüfen.

* if (user->can('EDIT_USER_ROLES')) {...}

So kann man ohne den front-end-code zu ändern immer neue Rollen erstellen oder vorhandene anpassen.

### immer positiv, nie negativ

Rechte sollten immer positiv formuliert werden. 
In dem Sinne, dass neue Funktionen hinzukommen. 
Ansonsten kann es zu Konflikten bei der Definition der Rechte kommen, oder wenn ein User mehrere Rollen hat. 

