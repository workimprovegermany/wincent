# Use Cases
 
## U01: Passwort vergessen

* Login Page: Link Passwort vergessen -> leitet auf Passwort vergessen Seite
* Passwort vergessen Seite:
    * Eingabe Emailadresse
    * Button: Passwort anfordern -> Action user.password_new
* Action user.password_new: erzeugt und speichert neues Passwort und verschickt mail



## U02: Bonuscode
* User klickt auf Bonuscode in Navi (5. von oben)
* Seite bonuscodes.html öffnet sich und er sieht alle aktiven und gültigen Bonuscodes
  * App() wird erzeugt
  * Link zur Admin-Seite ist nicht sichtbar
* Er klick in der letzten Spalte auf das Kopieren-Symbol
* Der Bonuscode wird in die Zwischenablage gelegt
* Er geht mit der Maus über das Infosymbol und es erscheint ein Text
* *(✔︎ Stand: 02.04.19)*

## U03: Bonuscode Admin
* Vorgang wie bei User U06
* Link zur Admin-Seite ist sichtbar
* Admin klickt Admin-Link
* Seite bonuscodes-admin.html öffnet sich
  * App() wird erzeugt
* Alle inputs bis auf 'Projekt' und 'Bezeichnung' sind leer
* Admin wählt ein Projekt aus
* Inputs werden automatisch mit Daten befüllt
* Daten werden geändert
* Button speichern klicken
* Daten werden gespeichert
* *(✔︎ Stand: 02.04.19)*