#Wochenaufgaben 18. - 22.3

##Daniela
* Userbild löschen
* Passwort vergessen
* Short-Id unter Namen im Header anzeigen
* Fehler auslesen (z.B. sind alle Userdaten ausgefüllt?)
* Besucherzahlen auf Webseiten (zinempfehlung & partnerzins) erfassen

## Marco
* Datenupdate von Skynet
* Affiliatedaten von Skynet holen und updaten
* PDF stempeln
* get_related Funktion
* xhr_http request gestapelt ausführen
* Rechteverwaltung
* Bei Affiliate-Tabelle update neuen User anlegen ohne Passwort

## Vivien
* Daten auslesen für Charst (Dashboard)
* Support unterscheidung Vertrieb und Support
* Nur Admin Seiten und Links anzeigen wenn Admin eingeloggt
* zinsempfehlung.de individualisierbar machen
* Use Cases beginnen

