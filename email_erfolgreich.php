<!DOCTYPE html>
<html lang="de">
<?php
include "inc/head.php";
?>
<body id="index">
<header>

</header>

<main class="extern">
    <div class="container-fluid">
        <section>
            <div class="grid_6">
                <div class="grid_12 pad300">
                    <img src="img/logo-wincent.png" alt="wincent your gigital assistant from moneywell" />
                </div>
                <div class="grid_10 preffix_1 marg300 farbverlauf">
                    <h2>Wie geht es weiter?</h2>
                    <div class="marg150 grid_12 border-solid">
                        <div class="viereckig">1.</div>
                        <p>Öffnen Sie Ihren Posteingang und halten Sie Ausschau nach der E-Mail: Bitte Ihre E-Mail Adresse bestätigen.</p>
                    </div>

                    <div class="marg150 grid_12 border-solid">
                        <div class="viereckig">2.</div>
                        <p>Klicken Sie auf den Link in der E-Mail, um Ihre E-Mail-Adresse zu bestätigen.</p>
                    </div>

                    <div class="marg150 grid_12 border-solid">
                        <div class="viereckig">3.</div>
                        <p>Nach der Bestätigung der E-Mail bekommen Sie Ihren persönlichen Link zum Passwort eintragen.</p>
                    </div>
                </div>

            </div>
        </section>


    </div>
    <section id="bg-index">
        <img src="img/logo-mw-vertrieb.png" alt="moneywell" />
    </section>
</main>

<?php
include "inc/footer.php";
?>
