<!DOCTYPE html>
<html lang="de">
    <?php
    include "inc/head.php";
    ?>
    <body id="index">
        <header>

        </header>

        <main class="extern">
            <div class="container-fluid">
                <section>
                    <div class="grid_6">
                        <div class="grid_12 pad300">
                            <img src="img/logo-wincent.png" alt="wincent your gigital assistant from moneywell" />
                        </div>
                        <div class="grid_12 pad300">
                            <h1 class="highlightFont1 uppercase">Login</h1>

                            <form id="loginform" class="grid_11" autocomplete="off">
                                <div class="grid_12 flex_center">
                                    <p class="error" id="error">Die eingegebenen Login-Daten sind nicht richtig. Bitte versuchen Sie es erneut.</p>
                                </div>
                                <div class="grid_12 flex_center">
                                    <div class="grid_3 pad150">
                                        <label>Benutzer</label>
                                    </div>
                                    <div class="grid_9 marg150 input_email">
                                        <input type="text" name="email_user" class="float-left" autocomplete="off" aria-autocomplete="none"/>
                                        <input type="text" class="label_email float-left" name="email_domain" autocomplete="off" aria-autocomplete="none"/>
                                        <div></div>
                                    </div>
                                    <div class="grid_3 pad150">
                                        <label>Passwort</label>
                                    </div>
                                    <div class="grid_9 pad150">
                                        <input type="password" name="pass" autocomplete="off" aria-autocomplete="none">
                                    </div>
                                </div>
                                <div class="grid_12 clear">
                                    <button class="btn_01 float-right" type="submit">Anmelden</button>
                                </div>

                            </form>
                            <div class="grid_12">
                                <p class="small80">Passwort vergessen? - <a href="passwort_anfordern.php" class="highlightLink underline">Hier</a> neu anfordern.</p>
                            </div>
                        </div>
                    </div>
                </section>


            </div>
            <section id="bg-index">
                <img src="img/logo-mw-vertrieb.png" alt="moneywell" />
            </section>

        </main>

        <?php
        include "inc/footer.php";
        ?>

    <script>
        (function()
        {
            var form = document.getElementById('loginform');

            form.addEventListener('submit', function (event) {
                event.preventDefault();

                xhr=new XMLHttpRequest();

                xhr.onreadystatechange=function()
                {
                    if (xhr.readyState==4 && xhr.status==200)
                    {
                        console.log('answer');
                        var response = xhr.responseText;
                        console.log(response);
                        token = JSON.parse(response);
                        console.log(token);

                        if(token['name'] == 'user.login_failed') {
                            // alert(token);
                            document.getElementById("error").style.display='block';
                        }
                        else {
                            window.location.href="intern/account.php?token="+token['data'];
                        }
                    }
                };

                xhr.open('POST', 'api', true);

                var form_data = new FormData();

                form_data.append('af_cmd', 'user.login');

                email_user = document.querySelectorAll('input[name="email_user"]')[0].value;
                email_domain = document.querySelectorAll('input[name="email_domain"]')[0].value;

                form_data.append('user', email_user+'@'+email_domain);
                form_data.append('pass', document.querySelectorAll('input[name="pass"]')[0].value);

                xhr.send(form_data);
            }, false);

        }())
    </script>
    </body>
</html>