<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(7);
$token = $_GET['token'];

?>
<style>
    #overlay table tr td:nth-last-child(1), #overlay table tr td:nth-last-child(2){
        text-align: center;
    }
</style>
<main>
    <div class="container-fluid">
        <?php
        $activeSetting = 3;
        include "inc/naviSettings.php";
        ?>
        <hr>

        <section class="pad300 pad15-b grid_12">
            <div class="pad10 size80 bg-intern border-radius">
                <div class="icon_credit weiss"></div>
            </div>
            <div class="inline-block pad015">
                <a class="tooltip" title="lorem ipsum amet"></a>
                <h3>Abrechnung</h3>
            </div>
        </section>

       <section class="grid_12">
            <div id="js_table_container"></div>
        </section>

    </div>

   <div id="overlay">
        <div class="container-fluid">
            <i class="far fa-times-circle" id="js-close-overlay"></i>
            <form id="form_provision">
                <fieldset>
                    <legend>Abrechnung buchen</legend>
                    <div class="grid_12 flex_center marg50">
                        <div class="grid_3 ">
                            <label for="projekt"><strong>Projekt:</strong></label>
                        </div>
                        <div class="grid_3">
                            <div id="abr_projekt">Logistikzins_001</div>
                        </div>

                        <div class="grid_3">
                            <label for="projekt"><strong>Geldeingang:</strong></label>
                        </div>
                        <div class="grid_3">
                            <div id="abr_geld">26.03.2019</div>
                        </div>
                    </div>


                    <div class="grid_12 flex_center marg50">
                        <div class="grid_3">
                            <label for="kunde"><strong>Kunde:</strong></label>
                        </div>
                        <div class="grid_3">
                            <div id="abr_kunde">Max Mustermann</div>
                        </div>
                    </div>

                    <div class="grid_12 flex_center marg50">
                        <div class="grid_3">
                            <label for="kunde"><strong>Vertragsnummer:</strong></label>
                        </div>
                        <div class="grid_3">
                            <div id="abr_vertrag">1332553</div>
                        </div>
                    </div>
                    <hr>


                    <div class="grid_12 marg150" id="container_table_edit_abrechnungn">

                    </div>
                    <div class="grid_12">
                        <div class="add-provision">
                            <button class="btn_0">Buchen</button>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</main>
<script>
   const app = new App(init_abrechnung_view);

</script>

<?php
include "inc/footer.php";
?>
