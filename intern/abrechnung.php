<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(6);

?>
<main>
    <div class="container-fluid">
        <?php
        $activeSetting = 1;
        include "inc/naviSettings.php";
        ?>
        <hr>

       <!-- <section class="pad300 grid_12">
            <div id="admin_link">
                <?php
               /* $tiles = array();
                array_push($tiles, new Tile('#?token='.$token, 'Abrechnungen', 'icon_credit'));
                array_push($tiles, new Tile('provision-admin.html?token='.$token, 'Bezahlen', 'icon_provision'));
                array_push($tiles, new Tile('rechteverwaltung.html?token='.$token, 'E-Mail', 'icon_email'));
                $tv = new Tile_view($tiles);
                $tv->generate_view_tile(); */
                ?>
            </div>
        </section>

-->
        <section>
            <div id="admin_link">
            <div id="navi_einstellungen" class="pad300 grid_12 clear">
                <a href="abrechnung-view.html?token=<?php echo $token; ?>" class="flex active">
                    <div class="size50 icon_credit"></div><p>Abrechnungen</p>
                </a>
                <a href="" class="flex active">
                    <div class="size50 icon_people"></div><p>Überprüfen</p>
                </a>
                <a href="#?token=<?php echo $token; ?>" class="flex active">
                    <div class="size50 icon_credit"></div><p>Bezahlen</p>
                </a>
                <a href="#?token=<?php echo $token; ?>" class="flex active">
                    <div class="size50 icon_facebook"></div><p>E-mail</p>
                </a>
            </div>
            </div>
        </section>

      <!--  <section>
            <div class="grid_12">
                <iframe src="iframe/iframe-table-old.php?dataUrl=https://examples.wenzhixin.net.cn/examples/bootstrap_table/data" frameborder="0" scrolling="no" width="100%" height="300px" id="iframe-table"></iframe>
            </div>
        </section> -->
    </div>
    <script>
        const app = new App();
    </script>

</main>

<?php
include "inc/footer.php";
?>
