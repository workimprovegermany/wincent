<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(7);

//Variablen aus DB
$src_img = empty($user_data->field('bild_name'))? '': $user_data->field('bild_pfad') . $user_data->field('bild_name');

$anzahl_affiliate = 58;

$firma = $user_data->field('firma');
$vorname = $user_data->field('vorname');
$nachname = $user_data->field('nachname');
$strasse = $user_data->field('strasse');
$hausnr = $user_data->field('hausnummer');
$plz = $user_data->field('plz');
$ort = $user_data->field('ort');
$mobil = $user_data->field('mobil');
$telefon = $user_data->field('telefon');
$kontoinhaber = $user_data->field('kontoinhaber');
$bankname = $user_data->field('bank');
$iban = $user_data->field('iban');
$bic = $user_data->field('bic');
$steuer = explode(':',$user_data->field('steuernummer'));
$steuer_nr = $steuer[0] == 'steuernr' ? 'selected':'';
$ust_nr = $steuer[0] == 'ustnr' ? 'selected':'';

$steuernr = $steuer[1];

$anrede = $user_data->field('anrede');
$selected_herr = $anrede == 1 ? 'selected':'';
$selected_frau = $anrede == 2 ? 'selected':'';

$email_user = explode('@',$user_data->field('email'));

$steuerpflicht = $user_data->field('steuerpflicht');
$st_1 = $steuerpflicht == 1 ? 'selected':'';
$st_0 = $steuerpflicht == 0 ? 'selected':'';

if(empty($user_data->field('email')) || $user_data->field('email')=='NULL') {
    $eu = new Entity_mapper($app->db(), 'user_login/1.0/config.xml');
    $user_login_data = $eu->find_by_fields(['uid' => $app->user]);
    $email_user_login = explode('@',$user_login_data[0]->field('uname'));

    $email_first = $email_user_login[0];
    $email_second = $email_user_login[1];
}
else {
    $email_first = $email_user[0];
    $email_second = $email_user[1];
}

$anzahl_affiliate = number_format($anzahl_affiliate, 0,',','.');

$user_id = $user_data->id();
?>
<style>
    @media (max-width: 979px) and (min-width: 480px){
        button[type="submit"].btn_03 {
            margin-left: 20%;
        }
    }

</style>
<main>
    <div class="container-fluid">
        <?php
        $activeSetting = 4;
        include "inc/naviSettings.php";
        ?>

        <section>
            <div class="grid_12">
                <div class="grid_3">
                    <form method="post" id="check_daten" enctype="multipart/form-data">
                    <?php
                    echo "<div class='box-change-img'>";
                    //echo "<img src='$src_img' alt='Avatar Partner' />";
                    //echo "<button type='button' class='width100'>Bild ändern</button>";
                    ?>
                    <div class="slim"
                         data-ratio="1:1"
                         data-did-remove="imageRemoved"
                         data-max-file-size = "1"
                         data-label = "<strong>Drag & Drop</strong> </br> </br> <p class='small80'>Ziehen Sie Ihr Bild hier hin </br> oder klicken Sie für einen Upload!</p>"
                         data-button-cancel-label="Abbruch"
                         data-button-confirm-label="Übernehmen"
                         data-button-cancel-title = "Abbruch"
                         data-button-confirm-title = "Übernehmen"
                         data-button-edit-title = "Bearbeiten"
                         data-button-remove-title = "Löschen"
                         data-button-upload-title = "Speichern"
                         data-button-rotate-title = "Drehen"
                         data-service = "upload_image">
                        <?php
                        if(!empty($src_img)){
                            echo "<img src=\"$src_img\" alt=\"Avatar Partner\"/>";
                        }
                        ?>
                        <input type="file" name="user_bild"/>
                    </div>
                    <input type="hidden" name="user_id" id="user_id" value="<?php echo $user_id; ?>" required>
                    <input type="hidden" name="bild_name" id="bild_name" value="<?php echo $user_data->field('bild_name'); ?>">
                    </form>
                    <?php
                    echo "</div>";

                    echo "<div class='infobox_zahlen'>";
                    echo "<div>";
                    echo "<div class='icon_people-two'></div>";
                    echo "</div>";
                    echo "<div>";
                    echo "<strong id='number_investors'></strong>";
                    echo "<p>Investoren</p>";
                    echo "</div>";
                    echo "</div>";

                    echo "<div class='infobox_zahlen' id='info-affiliates'>";
                        echo "<div>";
                                echo "<div class='icon_people'></div>";
                        echo "</div>";
                        echo "<div>";
                            echo "<strong id='number_affiliates'></strong>";
                            echo "<p>Affiliate-Partner</p>";
                        echo "</div>";
                    echo "</div>";
                    ?>
                </div> <!-- ENDE grid_3-->

                <div class="grid_9">
                    <div class="toggle-container">
                        <div class="toggle-navi flex">
                            <div class="flex_center js-toggle-element active" data-nr="1">
                                <div class="icon_man size30"></div>
                                <p>Persönlich</p>
                            </div>
                            <div class="flex_center js-toggle-element" data-nr="2">
                                <div class="icon_house size30"></div>
                                <p>Kontakt</p>
                            </div>
                            <div class="flex_center js-toggle-element" data-nr="3">
                                <div class="icon_credit size30"></div>
                                <p>Bank / Steuer</p>
                            </div>
                            <div class="flex_center js-toggle-element" data-nr="4">
                                <div class="icon_password size30"></div>
                                <p>Passwort</p>
                            </div>
                        </div> <!-- ENDE toggle-navi-->

                        <div class="toggle-elemente clear">
                            <div id="message" style="display:none;"></div>
                            <div class="toggle-element-1 grid_12 active">
                                <form method="post" id="form_persoenlich">
                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="firma">Firma</label>
                                        </div>
                                        <div class="grid_9">
                                            <input type="text" name="firma" id="firma" value="<?php echo $firma; ?>" placeholder="Firma">
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="anrede" name="anrede">Anrede</label>
                                        </div>
                                        <div class="grid_9">
                                            <select name="anrede" id="anrede">
                                                <option value="1" <?php echo $selected_herr; ?> >Herr</option>
                                                <option value="2" <?php echo $selected_frau; ?> >Frau</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="vorname">Vorname</label>
                                        </div>
                                        <div class="grid_9">
                                            <input type="text" name="vorname" id="vorname" value="<?php echo $vorname; ?>" placeholder="Vorname" required>
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="nachname">Nachname</label>
                                        </div>
                                        <div class="grid_9">
                                            <input type="text" name="nachname" id="nachname" value="<?php echo $nachname; ?>" placeholder="Nachname" required>
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="email">E-Mail Adresse</label>
                                        </div>
                                        <div class="grid_9 input_email">
                                            <input type="text" name="email-benutzer" id="email_benutzer" class="float-left" autocomplete="off" aria-autocomplete="none" value="<?php echo $email_first; ?>" required>
                                            <input type="text" name="email-domain" id="email_domain" class="float-left label_email" autocomplete="off" aria-autocomplete="none" value="<?php echo $email_second; ?>" required>
                                            <div></div>
                                        </div>
                                    </div>

                                    <div class="grid_12">
                                        <div class="inline-block pad15-t">
                                            <a class="tooltip" title="lorem ipsum amet"></a>
                                        </div>

                                        <button type="submit" class="float-right btn_03">
                                            <span class="content">Speichern</span>
                                            <span class="progress"><span></span></span>
                                        </button>
                                    </div>
                                </form>
                            </div>

                            <div class="toggle-element-2 grid_12">
                                <form method="post" id="form_kontakt">
                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label>Straße / Nr.</label>
                                        </div>
                                        <div class="grid_9 input_strasse_nr">
                                            <input type="text" name="strasse" id="strasse" value="<?php echo $strasse; ?>" placeholder="Straße" required>
                                            <input type="text" name="hausnr" id="hausnr" value="<?php echo $hausnr; ?>" placeholder="Hausnr." required>
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label>PLZ / Ort</label>
                                        </div>
                                        <div class="grid_9 input_plz_ort">
                                            <input type="text" name="plz" id="plz" value="<?php echo $plz; ?>" placeholder="PLZ" maxlength="5" required>
                                            <input type="text" name="ort" id="ort" value="<?php echo $ort; ?>" placeholder="Ort" required>
                                            <div></div>
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="mobil">Mobil</label>
                                        </div>
                                        <div class="grid_9 input_tel">
                                            <div></div>
                                            <input type="text" name="mobil" id="mobil" value="<?php echo $mobil; ?>" placeholder="Mobil" required>
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="telefon">Telefon</label>
                                        </div>
                                        <div class="grid_9 input_tel">
                                            <div></div>
                                            <input type="text" name="telefon" id="telefon" value="<?php echo $telefon; ?>" placeholder="Telefon (optional)" >
                                        </div>
                                    </div>
                                    <div class="grid_12">
                                        <button type="submit" class="float-right btn_03">
                                            <span class="content">Speichern</span>
                                            <span class="progress"><span></span></span>
                                        </button>
                                    </div>
                                </form>
                            </div>

                            <div class="toggle-element-3 grid_12">
                                <form method="post" id="form_bank">
                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="kontoinhaber">Kontoinhaber</label>
                                        </div>
                                        <div class="grid_9">
                                            <input type="text" name="kontoinhaber" id="kontoinhaber" value="<?php echo $kontoinhaber; ?>" placeholder="Name des Kontoinhabers" required>
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="bankname">Bankname</label>
                                        </div>
                                        <div class="grid_9">
                                            <input type="text" name="bankname" id="bankname" value="<?php echo $bankname; ?>" placeholder="Bankname" required>
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="iban">IBAN</label>
                                        </div>
                                        <div class="grid_9">
                                            <input type="text" name="iban" id="iban" class="js-iban-format uppercase" value="<?php echo $iban; ?>" placeholder="IBAN" required>
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="bic">BIC</label>
                                        </div>
                                        <div class="grid_9">
                                            <input type="text" name="bic" id="bic" value="<?php echo $bic; ?>" placeholder="BIC" required>
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="steuernr">Steuernummer</label>
                                        </div>
                                        <div class="grid_9 input_steuernr">
                                            <select id="steuernr_art" name="steuernr_art">
                                                <option value="Steuernummer" <?php echo $steuer_nr; ?> >Steuernummer</option>
                                                <option value="USt-IdNr" <?php echo $ust_nr; ?> >USt-IdNr.</option>
                                            </select>
                                            <input type="text" name="steuernr" id="steuernr" value="<?php echo $steuernr; ?>" placeholder="Nummer" required>
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label>USt-Regelung</label>
                                        </div>
                                        <div class="grid_9">
                                            <select id="steuerpflicht" name="steuerpflicht">
                                                <option value="1" <?php echo $st_1; ?> >Steuerausweis mit USt</option>
                                                <option value="0" <?php echo $st_0; ?> >kein Steuerausweis - Kleinunternehmerregelung(§ 19 UStG)</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="grid_12">


                                        <button type="submit" class="float-right btn_03">
                                            <span class="content">Speichern</span>
                                            <span class="progress"><span></span></span>
                                        </button>

                                        <button id='btn-vertrag' type="button" class="float-left tablet-wide-float-right btn_01" onclick="window.location.href='http://wincent-online.de/intern/mitarbeitervertrag.html?token=<?php echo $token?>'">
                                            <span class="content">zum Partnervertrag</span>
                                        </button>
                                    </div>

                                </form>
                            </div>

                            <div class="toggle-element-4 grid_12">
                                <form method="post" id="form_passwort">
                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="oldPassword">Altes Passwort</label>
                                        </div>
                                        <div class="grid_9">
                                            <input type="password" name="oldPassword" id="oldPassword" placeholder="Altes Passwort" required>
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="newPassword">Neues Passwort</label>
                                        </div>
                                        <div class="grid_9 input_password">
                                            <input type="password" name="newPassword" id="newPassword" placeholder="Neues Passwort" required>
                                            <div id="js-show-password"></div>
                                        </div>
                                    </div>

                                    <div class="grid_12 marg150 flex_center">
                                        <div class="grid_3">
                                            <label for="newPassword2">Passwort wiederholen</label>
                                        </div>
                                        <div class="grid_9">
                                            <input type="password" name="newPassword2" id="newPassword2" placeholder="Neues Passwort wiederholen" title="Die Passwörter stimmen nicht überein!" required>
                                            <p class="error" id="error_pass">Die Passwörter stimmen nicht überein!</p>
                                        </div>
                                    </div>

                                    <div class="grid_12">
                                        <button type="submit" class="float-right btn_03">
                                            <span class="content">Speichern</span>
                                            <span class="progress"><span></span></span>
                                        </button>
                                    </div>
                                </form>
                            </div>

                        </div><!-- ENDE toggle-elemente-->

                    </div>


                </div> <!-- ENDE grid_9-->
            </div>
        </section>
    </div>

</main>

<script>
    const app = new App(init_settings_account_user);

    function upload_image(formdata, progress, success, failure)
    {
        // "formdata" is a FormData object ready to be send to the server
        // more on working with formdata can be found here:
        // https://developer.mozilla.org/en-US/docs/Web/API/FormData

        // "progress(current, total)" is a function you can call to update the progress indicator
        // it expects the uploaded amount of bytes and the total bytes as parameters
        //progress(500, 1000); // will put the progress indicator at 50%

        // "success(response)" should be called after the upload is done, expects a response object or string
        //success("upload done");

        // "error(message)" should be called in case of upload problems, expects a string
        //failure("something went wrong");
        event = {
            name: 'user.file_upload',
            data: {
                entity_name: 'user',
                entity_id: document.forms[0].elements.user_id.value,
                entity_folder: 'bilder',
                entity_fields: {
                    1: 'bild_pfad',
                    2: 'bild_name',
                }
            }
        };
        formdata.append('event', JSON.stringify(event));

        xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                response = xhr.responseText;
                //obj = JSON.parse(response);
                console.log(response);
            }
            success("upload done");
        };

        xhr.open('POST', 'api', true);
        xhr.send(formdata);

    }

    function imageRemoved(data) {
        var data = new FormData();
        event = {
            name: 'user.file_upload',
            data: {
                entity_name: 'user',
                entity_id: document.forms[0].elements.user_id.value,
                entity_folder: 'bilder',
                entity_fields: {
                    1: 'bild_pfad',
                    2: 'bild_name',
                    bild_name: document.forms[0].elements.bild_name.value,
                }
            }
        };
        data.append('event', JSON.stringify(event));

        xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {
                response = xhr.responseText;
                //obj = JSON.parse(response);
                console.log(response);
            }
        };

        xhr.open('POST', 'api', true);
        xhr.send(data);
    }

    <?php
    include "../app-foundation/js/src/formular.js";
    ?>

</script>


<?php
include "inc/footer.php";
?>



