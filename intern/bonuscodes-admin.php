<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(5);
$token = $_GET['token'];

?>
<main>
    <div class="container-fluid">
        <section class="pad300 grid_12 flex_center">
            <div class="pad10 size80 bg-intern border-radius">
                <div class="icon_settings weiss"></div>
            </div>
            <div class="inline-block max-content pad015">
                <a class="tooltip" title="Hier können Sie die Bonuscodes anpassen und aktivieren oder deaktivieren. Wählen Sie zum Anpassen ein Projekt aus."></a>
                <h3>Adminbereich Bonuscodes</h3>
            </div>
        </section>

        <section class="grid_12">
            <form id="form_bonuscode" class="responsive_tablet_wide">
                <div class="grid_5 marg150 flex_center">
                    <div class="grid_3">
                        <label for="project">Projekt</label>
                    </div>
                    <div class="grid_9">
                        <select name="project" id="project" required>
                            <option value="">Projekt auswählen</option>
                        </select>
                    </div>
                </div>
                <div class="grid_7 preffix_7"></div>

                <div class="grid_5 marg150 flex_center">
                    <div class="grid_3">
                        <label for="beginn">Beginn</label>
                    </div>
                    <div class="grid_9">
                        <input class="js-datepicker" type="text" name="beginn" id="beginn" value="" placeholder="Datum ab" autocomplete="off" aria-autocomplete="none" required>
                    </div>
                </div>

                <div class="grid_5 preffix_1 marg150 flex_center">
                    <div class="grid_3">
                        <label for="ende">Ende</label>
                    </div>
                    <div class="grid_9">
                        <input class="js-datepicker" type="text" name="ende" id="ende" value="" placeholder="Datum bis" autocomplete="off" aria-autocomplete="none" required>
                    </div>
                </div>

                <div class="grid_5 marg150 flex_center">
                    <div class="grid_3">
                        <label for="bonuscode">Bonuscode</label>
                    </div>
                    <div class="grid_9">
                        <input type="text" name="bonuscode" id="bonuscode" value="" placeholder="Bonuscode" required>
                    </div>
                </div>

                <div class="grid_5 preffix_1 marg150 flex_center">
                    <div class="grid_3">
                        <label for="hoehe">Bonushöhe</label>
                    </div>
                    <div class="grid_9 input_individuell">
                        <input type="text" name="hoehe" id="hoehe" value="" placeholder="Bonuscodehöhe in %" pattern="[0-9]{1,2},{0,1}[0-9]{0,4}" title="Bitte Wert mit Komma angeben und ohne %-Zeichen. Bsp.: 1,25" required>
                        <div data-content="%"></div>
                    </div>
                </div>

                <div class="grid_5 marg150 flex_center">
                    <div class="grid_3">
                        <label for="bezeichnung">Bezeichnung</label>
                    </div>
                    <div class="grid_9">
                        <select name="bezeichnung" id="bezeichnung" required>
                            <option value="1">Erstinvestition</option>
                        </select>
                    </div>
                </div>

                <div class="grid_5 preffix_1 marg300 flex_center">
                    <input type="checkbox" name="bonus_activ" id="bonus_activ">
                    <label for="bonus_activ">Bonus aktiv</label>
                </div>

                <div class="grid_11">
                    <button type="submit" class="btn_03 float-right">
                        <span class="content">Speichern</span>
                        <span class="progress"><span></span></span>
                    </button>
                </div>

            </form>
        </section>
    </div>

</main>

<script>
    const app = new App(init_bonuscodes_admin);
</script>

<?php
include "inc/footer.php";
?>



