<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(5);
$token = $_GET['token'];

?>
<main>
    <div class="container-fluid">
        <section class="pad300 grid_12">
            <div class="pad10 size80 bg-intern border-radius">
                <div class="icon_bonuscodes weiss"></div>
            </div>
            <div class="inline-block pad015">
                <a class="tooltip" title="Auf dieser Seite sehen Sie alle aktiven und gültigen Bonuscodes. Über das Kopieren-Symbol in der letzten Spalte können Sie sich den Bonuscode in Ihre Zwischenablage legen."></a>
                <h3>Bonuscodes</h3>
            </div>
        </section>
        <section class="grid_12 pad15-b" id="admin_link">
            <div class="add-table-admin">
                <a href="<?php echo "bonuscodes-admin.html?token=".$token;?>"><i class="fas fa-plus-square"></i></a>
            </div>
        </section>

        <section class="grid_12">
            <div id="js_table_container"></div>
        </section>
    </div>

</main>

<script>
    const app = new App(init_bonuscodes);
</script>

<?php
include "inc/footer.php";
?>



