<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(1);

$token = $_GET['token'];
?>
<main>
    <div class="container-fluid">
        <section class="grid_12">
            <div class="pad300 chart_info flex">
                <div class="grid_3">
                    <div>
                        <div class="size50 icon_visitors"></div>
                        <div>
                            <p class="small80">Besucher</p>
                            <b></b>
                        </div>
                    </div>
                    <canvas class="filledLineChart chartcolor" id="chart_visitors"></canvas>
                </div>
                <div class="grid_3">
                    <div>
                        <div class="size50 icon_investment"></div>
                        <div>
                            <p class="small80">Investment</p>
                            <b></b>
                        </div>
                    </div>
                    <canvas class="filledLineChart chartcolor" id="chart_investment"></canvas>
                </div>
                <div class="grid_3">
                    <div>
                        <div class="size50 icon_hand-growth"></div>
                        <div>
                            <p class="small80">Gesamtzahlungen</p>
                            <b></b>
                        </div>
                    </div>
                    <canvas class="filledLineChart chartcolor" id="chart_total_payments"></canvas>
                </div>
                <div class="grid_3">
                    <div>
                        <div class="size50 icon_buyer"></div>
                        <div>
                            <p class="small80">Zahlungen <span id="this_year"></span></p>
                            <b></b>
                        </div>
                    </div>
                    <canvas class="filledLineChart chartcolor" id="chart_total_payments_this_year"></canvas>
                </div>
            </div>
        </section>

        <section class="grid_12">
            <div id="admin_link">
                <div class="grid_12">
                    <div class="add-infobox-admin pad15-b">
                        <a href="<?php echo "infoboxen-admin.html?token=".$token;?>"><i class="fas fa-plus-square"></i></a>
                    </div>
                </div>
            </div>
            <iframe src="iframe/iframe-infoboxen.php?token=<?php echo $token; ?>" frameborder="0" scrolling="no" width="100%" height="0"></iframe>
        </section>

        <section class="grid_12">
                <div class="grid_8 border pad10">
                    <h4 class="inline-block">Investment</h4>
                    <canvas class="twoLineChart" id="chart_last_seven_days"></canvas>
                    <div class="chartcolor"></div>
                    <div class="chartcolorHighlight"></div>
                </div>
                <div class="grid_4 infobox_chart">
                    <div>
                        <div class="round">
                            <div class="icon_people-two"></div>
                        </div>
                        <p><span id="number_investors"></span>  Investoren</p>
                    </div>
                    <div>
                        <div>
                            <canvas class="doughnutChart chartcolor" data-chart="20,80"></canvas>
                        </div>
                        <p><span id="number_own_sales"></span>  Eigenumsatz</p>
                    </div>
                    <div>
                        <div>
                            <canvas class="doughnutChart chartcolor" data-chart="80,20"></canvas>
                        </div>
                        <p><span id="number_partner_sales"></span>  Partnerumsatz</p>
                    </div>
                </div>
        </section>

    </div>
</main>

<script>
    const app = new App(initDashboard);
</script>

<?php
include "inc/footer.php";
?>



