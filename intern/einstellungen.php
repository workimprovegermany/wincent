<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(7);

?>
<main>
    <div class="container-fluid">
        <?php
        $activeSetting = 3;
        include "inc/naviSettings.php";
        ?>
        <hr>
        <section class="pad300 grid_12">
            <?php
            $token = $_GET['token'];
            $tiles = array();
            array_push($tiles, new Tile('mitarbeitervertrag.html?token='.$token, 'Mitarbeitervertrag', 'icon_contract'));
            $tv = new Tile_view($tiles);
            $tv->generate_view_tile();
            ?>
            <div id="admin_link">
                <?php
                $tiles = array();
                array_push($tiles, new Tile('provision-admin.html?token='.$token, 'Provistion ändern', 'icon_provision'));
                array_push($tiles, new Tile('rechteverwaltung.html?token='.$token, 'Rechteverwaltung', 'icon_security'));
                $tv = new Tile_view($tiles);
                $tv->generate_view_tile();
                ?>
            </div>
        </section>
    </div>
</main>
<script>
    const app = new App();
</script>

<?php
include "inc/footer.php";
?>
