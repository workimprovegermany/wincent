<!doctype html>
<html lang="de">
<?php

require_once("../../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

$user_id = $user_data->id();
include "../inc/head-sub.php";

?>

<body>
<main class="iframe" id="container-infoboxen">
</main>

<script>

    initInfobalkenIFrame();
    setTimeout(function () {
        window.parent.postMessage({"height": $(document).height()}, "*");
    }, 500);

    $(window).on('resize', function (event) {
        window.parent.postMessage({"height": $(document).height()}, "*");
    });
</script>

</body>
</html>