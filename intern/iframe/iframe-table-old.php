<!doctype html>
<html lang="de">
<?php
include "../inc/head-table.php";

$dataUrl = $_GET['dataUrl'];
?>

<body>
<main class="iframe">
            <div id="toolbar"></div>
            <table
                id="table"
                data-toolbar="#toolbar"
                data-search="true"
                data-show-refresh="true"
                data-detail-formatter="detailFormatter"
                data-minimum-count-columns="2"
                data-pagination="true"
                data-id-field="id"
                data-page-list="[10, 25, 50, 100, ALL]"
                data-show-footer="false"
                data-side-pagination="server"
                data-url= <?php echo $dataUrl; ?>
                data-response-handler="responseHandler">
                <!--data-show-columns="true"
                data-show-export="true"-->
            </table>
</main>


<script>
    var $table = $('#table')
    var selections = []

    function getIdSelections() {
        return $.map($table.bootstrapTable('getSelections'), function (row) {
            return row.id
        })
    }

    function responseHandler(res) {
        $.each(res.rows, function (i, row) {
            row.state = $.inArray(row.id, selections) !== -1
        })
        return res
    }

    function detailFormatter(index, row) {
        var html = []
        $.each(row, function (key, value) {
            html.push('<p><b>' + key + ':</b> ' + value + '</p>')
        })
        return html.join('')
    }

    function operateFormatter(value, row, index) {
        return [
            '<a class="like" href="javascript:void(0)" title="Like">',
            '<i class="glyphicon glyphicon-heart"></i>',
            '</a>  ',
            '<a class="remove" href="javascript:void(0)" title="Remove">',
            '<i class="glyphicon glyphicon-remove"></i>',
            '</a>'
        ].join('')
    }

    window.operateEvents = {
        'click .like': function (e, value, row, index) {
            alert('You click like action, row: ' + JSON.stringify(row))
        },
        'click .remove': function (e, value, row, index) {
            $table.bootstrapTable('remove', {
                field: 'id',
                values: [row.id]
            })
        }
    }

    function totalTextFormatter(data) {
        return 'Total'
    }

    function totalNameFormatter(data) {
        return data.length
    }

    function totalPriceFormatter(data) {
        var total = 0
        $.each(data, function (i, row) {
            total += +(row.price.substring(1))
        })
        return '$' + total
    }

    var columns = [
        {
            title: 'Item ID',
            field: 'id',
            align: 'center',
            valign: 'middle',
            sortable: true,
            footerFormatter: totalTextFormatter
        }, {
            field: 'name',
            title: 'Item Name',
            sortable: true,
            footerFormatter: totalNameFormatter,
            align: 'center'
        }, {
            field: 'price',
            title: 'Item Price',
            sortable: true,
            align: 'center',
            footerFormatter: totalPriceFormatter
        }, {
            field: 'operate',
            title: 'Item Operate',
            align: 'center',
            events: window.operateEvents,
            formatter: operateFormatter
        }];

    function initTable() {
        $table.bootstrapTable({
            columns: columns,
            formatLoadingMessage: function () {return 'Daten werden geladen...';},
            formatSearch: function () {return 'Suchen';},
            formatNoMatches: function () {return 'Keine Daten gefunden';},
            formatShowingRows: function (pageFrom, pageTo, totalRows) {return  pageFrom + ' bis ' + pageTo + ' von ' + totalRows + ' Zeilen ';},
            formatRecordsPerPage: function (pageNumber) {return pageNumber + ' Zeilen pro Seite';}

        })
        $table.on('check.bs.table uncheck.bs.table ' +
            'check-all.bs.table uncheck-all.bs.table',
            function () {

                // save your data, here just save the current page
                selections = getIdSelections()
                // push or splice the selections if you want to save all data selections
            })
        $table.on('expand-row.bs.table', function (e, index, row, $detail) {
            if (index % 2 === 1) {
                $detail.html('Loading from ajax request...')
                $.get('LICENSE', function (res) {
                    $detail.html(res.replace(/\n/g, '<br>'))
                })
            }
        });

        $table.on('all.bs.table', function (e, name, args) {
            console.log(name, args)
        });

        $table.on('load-success.bs.table', function (data) {
            window.parent.postMessage({"height": $(document).height()}, "*")
        });


    }

    $(function() {
        initTable()
    });

    //loadDataTable();

    function loadDataTable(){
        xhrSkynet=new XMLHttpRequest();

        xhrSkynet.onreadystatechange=function()
        {
            if (xhrSkynet.readyState===4 && xhrSkynet.status===200)
            {
                let response = xhrSkynet.responseText;
                const obj = JSON.parse(response);
                console.log(obj.data);
            }
        };

        xhrSkynet.open('POST', 'api', true);
        xhrSkynet.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        data = {
            event: {
                name: 'entity.load',
                data: {
                    entity_name: 'skynet_affiliate_conversions',
                    entity_filter: {network_id: 'zinsempfehlung'}
                }
            }
        };

        xhrSkynet.send("event="+JSON.stringify(data.event));

    }
</script>

</body>
</html>