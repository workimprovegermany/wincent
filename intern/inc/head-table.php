<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <link rel="stylesheet" href="../../vendor/dist/vendor.css">
    <link rel="stylesheet" href="../../vendor/dist/vendor-table.css">
    <link rel="stylesheet" href="../../app-foundation/css/dist/af.css">
    <link rel="stylesheet" href="../../css/dist/wincent.css">


    <script src="../../vendor/dist/vendor.min.js"></script>
    <script src="../../vendor/dist/vendor-table.min.js"></script>
    <script src="../../app-foundation/js/dist/af.min.js"></script>

</head>