<?php

switch ($activeSetting) {
    case 1: $navElementActive1 = 'active';
        break;

    case 2: $navElementActive2 = 'active';
        break;

    case 3: $navElementActive3 = 'active';
        break;

    case 4: $navElementActive4 = 'active';
        break;
}

$token = $_GET['token'];

?>

<section>
    <div id="navi_einstellungen" class="pad300 grid_12 clear">
        <a href="abrechnung.html?token=<?php echo $token; ?>" class="flex <?php echo $navElementActive1; ?>">
            <div class="size50 icon_search-euro"></div><p>Abrechnung</p>
        </a>
        <a href="" class="flex <?php echo $navElementActive2; ?>">
            <div class="size50 icon_people"></div><p>Affiliate</p>
        </a>
        <a href="einstellungen.html?token=<?php echo $token; ?>" class="flex <?php echo $navElementActive3; ?>">
            <div class="size50 icon_settings"></div><p>Einstellungen</p>
        </a>
        <a href="account.html?token=<?php echo $token; ?>" class="flex <?php echo $navElementActive4; ?>">
            <div class="size50 icon_user"></div><p>Account</p>
        </a>
    </div>
</section>