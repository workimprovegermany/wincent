<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(1);

$user_id = $user_data->id();

?>

<style>
    table:not([class*='ui-']) th:first-child{
        width: 50%;
    }
    table:not([class*='ui-']) th:not(:first-child){
        width: 25%;
    }
</style>
<main>
    <div class="container-fluid">
        <section class="grid_12 marg300">
            <h3>Informationsboxen</h3>
            <div id="table-infobox"></div>
            <h3 class="pad15-t">Hinweisboxen</h3>
            <div id="table-hinweisbox"></div>
        </section>
        <hr>
        <section class="grid_12">
            <form id="form_infobox">
                <fieldset>
                    <legend>Neue Infobox erstellen: </legend>

                    <div class="grid_12 marg150 flex_center">
                        <div class="grid_3">
                            <label>Art</label>
                        </div>
                        <div class="grid_9">
                            <div class="inline-block relative">
                                <input type="radio" name="art" id="hinweis" value="hinweis" checked>
                                <label for="hinweis">Hinweis</label>
                            </div>
                            <div class="inline-block relative">
                                <input type="radio" name="art" id="info" value="info">
                                <label for="info">Information</label>
                            </div>
                        </div>
                    </div>

                    <div class="grid_12 marg150 flex_center">
                        <div class="grid_3">
                            <label>Text</label>
                        </div>
                        <div class="grid_9">
                            <textarea name="text" id="text" rows="3" placeholder="Infobox Text" required></textarea>
                        </div>
                    </div>

                    <div class="grid_12 flex_center">
                        <div class="grid_3">
                            <label>Gültigkeit</label>
                        </div>
                        <div class="grid_9">
                            <div class="grid_5 marg150">
                                <input type="text" name="datum_ab" id="datum_ab" class="js-datepicker" placeholder="Gültig ab" required autocomplete="off">
                            </div>
                            <div class="grid_5 marg150 preffix_2">
                                <input type="text" name="datum_bis" id="datum_bis" class="js-datepicker" placeholder="Gültig bis" required autocomplete="off">
                            </div>
                        </div>
                    </div>

                    <div class="grid_12 marg150 flex_center">
                        <div class="grid_3">
                            <label>Infobox</label>
                        </div>
                        <div class="grid_9">
                            <div class="infobox_user_note infobox_admin" id="box_art">
                                <i class="fas fa-exclamation" id="box_icon"></i>
                                <p id="box_text">Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.</p>
                                <div class="close"><i class="fas fa-times"></i></div>
                            </div>
                        </div>
                    </div>

                    <div class="grid_12 marg150 flex_center">
                        <div class="grid_3"></div>
                        <div class="grid_9">
                            <p class="small80"><i class="fas fa-exclamation"></i> Bitte prüfen Sie die Daten sorgfältig vor dem Speichern. Es können <b>keine Infoboxen gelöscht</b> werden.</p>
                        </div>
                    </div>

                    <button type="submit" name="btn_submit" class="btn_03 float-right">
                        <span class="content">Speichern</span>
                        <span class="progress"><span></span></span>
                    </button>

                </fieldset>
            </form>
        </section>

        <section class="marg300 grid_12">
            <hr>
            <h3 class="inline-block">Abgelaufene Infoboxen</h3>
            <i class="fas fa-eye big160 float-right pointer hide" id="js-show-more"></i>
            <div class="grid_12 marg150">
                <div id="table-data-old"></div>
            </div>

        </section>
    </div>
</main>

<script type="text/javascript">
    const app = new App(initInfoboxAdmin);
</script>

<?php
include "inc/footer.php";
?>