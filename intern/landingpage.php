<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(3);

?>

<style>

</style>
<main>
    <div class="container-fluid">
        <section class="grid_12 marg300">
            <div id="tabs">
                <ul>
                    <li><a href="#tab-1">partnerzins-online.de</a></li>
                    <li><a href="#tab-2">zinsempfehlung-online.de</a></li>
                </ul>
                <div id="tab-1">
                    <section class="grid_12 marg300">
                        <div class="grid_10 preffix_1 center">
                            <p><div class="size20 icon_copy js_copy_url"></div>&nbsp;&nbsp;<a href="" target="_blank" style="vertical-align: text-bottom;">http://<span class="js-url">partnerzins-online.de</span>/<span class="js-short-id"></span></a></p>
                        </div>
                        <div class="grid_10 preffix_1">
                            <img src="../img/desktop.png" class="mockup_desktop">
                            <div id="js-area-container">
                                <div>
                                    <img src="../img/landingpage/landingpage_1.jpg" />
                                </div>
                                <div data-nr="1">
                                    <hr>
                                    <p class="">Bereich 1</p>
                                    <img src="../img/landingpage/landingpage_2.jpg" />
                                </div>
                                <div data-nr="2">
                                    <hr>
                                    <p class="">Bereich 2</p>
                                    <img src="../img/landingpage/landingpage_3.jpg" />
                                </div>
                                <div data-nr="3">
                                    <hr>
                                    <p class="">Bereich 3</p>
                                    <img src="../img/landingpage/landingpage_4.jpg" />
                                </div>
                                <div data-nr="4">
                                    <hr>
                                    <p class="">Bereich 4</p>
                                    <img src="../img/landingpage/landingpage_5.jpg" />
                                </div>
                                <div data-nr="5">
                                    <hr>
                                    <p class="">Bereich 5</p>
                                    <img src="../img/landingpage/landingpage_6.jpg" />
                                </div>
                                <div data-nr="6">
                                    <hr>
                                    <p class="">Bereich 6</p>
                                    <img src="../img/landingpage/landingpage_7.jpg" />
                                </div>
                                <div>
                                    <img src="../img/landingpage/landingpage_8.jpg" />
                                </div>
                            </div>

                        </div>
                    </section>

                    <section>
                        <form id="form_pers_landingpage">
                            <fieldset>
                                <legend>Bereiche & Video auswählen: </legend>
                                <p>Welche Bereiche Ihrer persönlichen Landingpage möchten Sie angezeigt haben?</p>

                                <div class="grid_4 marg150 flex_center">
                                    <input type="checkbox" name="bereich1" id="bereich1" class="js-toggle-area" data-nr="1" >
                                    <label for="bereich1">Bereich 1</label>
                                </div>
                                <div class="grid_4 marg150 flex_center">
                                    <input type="checkbox" name="bereich2" id="bereich2" class="js-toggle-area" data-nr="2">
                                    <label for="bereich2">Bereich 2</label>
                                </div>
                                <div class="grid_4 marg150 flex_center">
                                    <input type="checkbox" name="bereich3" id="bereich3" class="js-toggle-area" data-nr="3">
                                    <label for="bereich3">Bereich 3</label>
                                </div>
                                <div class="grid_4 marg150 flex_center">
                                    <input type="checkbox" name="bereich4" id="bereich4" class="js-toggle-area" data-nr="4">
                                    <label for="bereich4">Bereich 4</label>
                                </div>
                                <div class="grid_4 marg150 flex_center">
                                    <input type="checkbox" name="bereich5" id="bereich5" class="js-toggle-area" data-nr="5">
                                    <label for="bereich5">Bereich 5</label>
                                </div>
                                <div class="grid_4 marg150 flex_center">
                                    <input type="checkbox" name="bereich6" id="bereich6" class="js-toggle-area" data-nr="6">
                                    <label for="bereich6">Bereich 6</label>
                                </div>
                                <hr>
                                <p>Welches Video möchten Sie auf Ihrer persönlichen Landingpage?</p>
                                <div class="grid_5 marg150 flex_center inline-block">
                                    <input type="radio" name="video" id="video1" value="1" checked>
                                    <label for="video1">Partner werden Version 1</label>
                                    <div class="pad15-t">
                                        <video controls class="border">
                                            <source src="../_media/videos/vermittler/Moneywell_Partner_werden_Intro_Akademie.MP4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                                <div class="grid_5 preffix_1 marg150 flex_center inline-block">
                                    <input type="radio" name="video" id="video2" value="2">
                                    <label for="video2">Partner werden Version 2</label>
                                    <div class="pad15-t">
                                        <video controls class="border">
                                            <source src="../_media/videos/vermittler/Moneywell_Partner_werden_LiebeKollegen02_WA.MP4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>

                                <input type="hidden" name="id" id="id" value="">

                                <button type="submit" name='btn_submit' class="btn_03 float-right">
                                    <span class="content">Speichern</span>
                                    <span class="progress"><span></span></span>
                                </button>
                            </fieldset>
                        </form>
                    </section>
                </div> <!-- ENDE tab-1 -->

                <div id="tab-2">
                    <section class="grid_12 marg300"><div class="grid_10 preffix_1 center">
                            <p><div class="size20 icon_copy js_copy_url"></div>&nbsp;&nbsp;<a href="" target="_blank" style="vertical-align: text-bottom;">http://<span class="js-url">zinsempfehlung-online.de</span>/<span class="js-short-id"></span></a></p>
                        </div>
                        <div class="grid_10 preffix_1">
                            <img src="../img/desktop.png" class="mockup_desktop">
                            <div id="js-area-container-kunde">
                                <div>
                                    <img src="../img/landingpage/zinsempfehlung_div1.png" />
                                </div>
                                <div data-nr="7">
                                    <hr>
                                    <p class="">Bereich 1</p>
                                    <img src="../img/landingpage/zinsempfehlung_div2.png" />
                                </div>
                                <div data-nr="8">
                                    <hr>
                                    <p class="">Bereich 2</p>
                                    <img src="../img/landingpage/zinsempfehlung_div3.png" />
                                </div>
                                <div>
                                    <img src="../img/landingpage/footer.png" />
                                </div>
                            </div>

                        </div>
                    </section>

                    <section>
                        <form id="form_pers_landingpage_kunde">
                            <fieldset>
                                <legend>Bereiche & Video auswählen: </legend>
                                <p>Welche Bereiche Ihrer persönlichen Landingpage möchten Sie angezeigt haben?</p>

                                <div class="grid_12 marg150">
                                    <div class="grid_4 marg150 flex_center">
                                        <input type="checkbox" name="bereich1_kunde" id="bereich1_kunde" class="js-toggle-area" data-nr="7" >
                                        <label for="bereich1_kunde">Bereich 1</label>
                                    </div>
                                    <div class="grid_4 marg150 flex_center">
                                        <input type="checkbox" name="bereich2_kunde" id="bereich2_kunde" class="js-toggle-area" data-nr="8">
                                        <label for="bereich2_kunde">Bereich 2</label>
                                    </div>
                                </div>
                                <hr>
                                <p>Welches Video möchten Sie auf Ihrer persönlichen Kunden-Landingpage?</p>
                                <div class="grid_5 marg150 flex_center inline-block">
                                    <input type="radio" name="video_kunde" id="video1_kunde" value="1" checked>
                                    <label for="video1_kunde">Alles Online</label>
                                    <div class="pad15-t">
                                        <video controls class="border">
                                            <source src="../_media/videos/kunde/Moneywell_AllesOnline_WA.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                                <div class="grid_5 preffix_1 marg150 flex_center inline-block">
                                    <input type="radio" name="video_kunde" id="video2_kunde" value="2">
                                    <label for="video2_kunde">Geld muss arbeiten</label>
                                    <div class="pad15-t">
                                        <video controls class="border">
                                            <source src="../_media/videos/kunde/Moneywell_GeldMussArbeiten_WA.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                                <div class="grid_5 marg150 flex_center inline-block">
                                    <input type="radio" name="video_kunde" id="video3_kunde" value="3">
                                    <label for="video3_kunde">Bank im Regen</label>
                                    <div class="pad15-t">
                                        <video controls class="border">
                                            <source src="../_media/videos/kunde/Moneywell_LukrativeZinsen_WA.mp4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>
                                <div class="grid_5 preffix_1 marg150 flex_center inline-block">
                                    <input type="radio" name="video_kunde" id="video4_kunde" value="4">
                                    <label for="video4_kunde">Wirtschaftsboom</label>
                                    <div class="pad15-t">
                                        <video controls class="border">
                                            <source src="../_media/videos/kunde/Moneywell_Wirtschaftsboom_WA.MP4" type="video/mp4">
                                        </video>
                                    </div>
                                </div>

                                <input type="hidden" name="id" id="id" value="">

                                <button type="submit" name='btn_submit_kunde' class="btn_03 float-right">
                                    <span class="content">Speichern</span>
                                    <span class="progress"><span></span></span>
                                </button>
                            </fieldset>
                        </form>
                    </section>
                </div> <!-- ENDE tab-2 -->
            </div><!-- ENDE tabs -->

        </section>

    </div>

</main>

<script>
    const app = new App(init_form_pers_landing);
</script>

<?php
include "inc/footer.php";
?>



