<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(7);

?>
<main>
    <div class="container-fluid">
        <?php
        $activeSetting = 3;
        include "inc/naviSettings.php";
        ?>
        <hr>

        <section class="pad300 grid_12">
            <div class="pad10 size80 bg-intern border-radius">
                <div class="icon_contract"></div>
            </div>
            <div class="inline-block pad015">
                <a class="tooltip" title="lorem ipsum amet"></a>
                <h3>Mitarbeitervertrag</h3>
            </div>
        </section>
        <section class="pad150 grid_12">

            <form method="post" id="form_employee_contract">
                <div class="grid_6">
                    <a href="" target="_blank" class="flex_center"><div class="size40"><div class="icon_pdf_download" style="background-position: -4px 0;"></div></div> Muster Allgemeine Vertragsbedingungen</a>
                    <a href="" target="_blank" class="flex_center pad150"><div class="size40"><div class="icon_pdf_download" style="background-position: -4px 0;"></div></div> Muster Vergütungsvereinbarung</a>
                    <p class="big130">Tippgeberprovision: <input type="text" id="provision" name="provision" class="input_as_text"></p>
                    <p class="big130 pad150">Tippgeber-Overhead: <input type="text" name="verguetung" id="verguetung" class="input_as_text"></span></p>
                    <hr>
                </div>
                <div class="grid_12 marg150 flex">
                    <input type="checkbox" name="angaben_einverstanden" id="angaben_einverstanden" required>
                    <label for="angaben_einverstanden">Ja, ich habe die Angaben gelesen und bin damit einverstanden</label>

                    <div class="grid_12 marg300">
                        <div class="pruefen signature">
                            <label>Unterschrift <span data-content="vorname"></span> <span data-content="nachname"></span></label>
                            <div class="sigPad" id="sigArea_1">
                                <canvas class="pad" width="450" height="200" id="sig_1"></canvas>
                                <input type="hidden" name="sig" id="sig" class="output" required >

                                <div>
                                    <button class="clearButton"><span class="fas fa-trash-alt"></span</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button" disabled class="btn_03 js-toggle-disabled">Vertragsvorschau</button>
                    <button type="submit" disabled class="btn_03 js-toggle-disabled">Vertrag erstellen</button>
                </div>
            </form>
        </section>
    </div>
</main>
<script>
    const app = new App(init_employee_contract);
</script>

<?php
include "inc/footer.php";
?>
