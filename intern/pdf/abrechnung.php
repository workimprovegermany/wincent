<?php
require('fpdf.php');

//$sess_id = session_id();
$sess_id = $_GET['id'];

//header( "refresh:0.1;url=../bat/rd-mailform_bestellung.php?id=anfrage_passwort&session=$sess_id" );

//header( "refresh:0.1;url=../zugangsdaten.html?id=$sess_id" );
//zugangsdaten-neu
//require("global_funcs.php");
class PDF extends FPDF 
{


//   ============================================================================

function combo_1 ($v1,$v2,$v3,$v4,$v5,$v6,$v7,$v8,$v9)
{ 
  $this->SetFont('Arial','',11);  		        
  $this->SetDrawColor(0,0,0);  		   
  $this->SetFillColor(255,255,255);
  $this->SetLineWidth(0.0);  				
  $this->SetXY(15,22);
  $this->Cell(1,0,"",0,0,'L',1);   	

  $this->SetLineWidth(0.4);  				
  $this->SetFont('Arial','B',6);
  $this->SetTextColor(35,86,100);
  //$this->SetTextColor(100,75,30);
  $this->SetXY(15,38);
  $this->Cell(100,4,"Moneywell Vertriebsgesellschaft mbH | Englische Planke 2 | 20459 Hamburg",0,0,'L',1);
  $this->SetLineWidth(0.1);  				
 // $this->Line(15,42.5,100,42.5);
	
// Kundenanschrift	
	
  $this->SetFont('Arial','',10);
  $this->SetTextColor(0,0,0);
  $this->SetXY(15,47);

  $this->Cell(100,5,iconv("UTF-8", "ISO-8859-1","$v1"),0,0,'L',1);
  $this->SetFont('Arial','',10);
  $this->SetXY(15,53);
  $this->Cell(100,5,iconv("UTF-8", "ISO-8859-1","$v2"),0,0,'L',1);
  $this->SetXY(15,59);
  $this->Cell(100,5,iconv("UTF-8", "ISO-8859-1","$v3"),0,0,'L',1);
  $this->SetXY(15,65);
  $this->Cell(100,5,iconv("UTF-8", "ISO-8859-1","$v4"),0,0,'L',1);


    $this->SetFont('Arial','B',10);
    $this->SetXY(130,47);
    $this->Cell(100,5,"Partnerdaten:",0,0,'L',1);
    $this->SetFont('Arial','',10);
    $this->SetXY(130,53);
    $this->Cell(35,5,iconv( "UTF-8", "ISO-8859-1","Tippgebernummer"),0,0,'L',1);
    $this->Cell(35,5,iconv( "UTF-8", "ISO-8859-1","$v5"),0,0,'L',1);
    $this->SetXY(130,58);
    $this->Cell(35,5,iconv( "UTF-8", "ISO-8859-1","$v6"),0,0,'L',1);
    $this->Cell(35,5,iconv( "UTF-8", "ISO-8859-1","$v7"),0,0,'L',1);

    $this->SetXY(130,66);
    $this->Multicell(70,4,iconv( "UTF-8", "ISO-8859-1","Bei Fragen bitte unbedingt Beleg- und Tippgeber-Nr. mit angeben!"));

    $this->SetXY(130,77);
    $this->Cell(35,5,iconv( "UTF-8", "ISO-8859-1","Beleg-Nr."),0,0,'L',1);
    $this->Cell(35,5,iconv( "UTF-8", "ISO-8859-1","P20190118 $v8"),0,0,'L',1);

    $this->SetXY(130,82);
    $this->Cell(35,5,iconv( "UTF-8", "ISO-8859-1","Belegdatum"),0,0,'L',1);
    $this->Cell(35,5,iconv( "UTF-8", "ISO-8859-1","$v9"),0,0,'L',1);

    // Rechnungsnummer
    $this->SetFont('Arial','B',10);
    $this->SetXY(15,90);
    $this->Cell(100,5,"PROVISIONGUTSCHRIFT: P20190104 $v8",0,0,'L',1);

}

function combo_2 ($v1,$v2,$v3,$v4,$v5,$v6,$v7,$v8,$v9,$v10,$v11,$v12,$v13,$v14,$v15)
{
  $this->SetFont('Arial','',10);  		        
  $this->SetDrawColor(0,0,0);  		   
  $this->SetTextColor(0,0,0); 		 	   
  $this->SetFillColor(255,255,255);  	
  $this->SetLineWidth(0.0);

    // Anrede
    $this->SetFont('Arial','',10);
    $this->SetXY(15,105);
    $this->Cell(50,5,iconv( "UTF-8", "ISO-8859-1","$v2,"),0,0,'L',1);

    $this->SetXY(15,114);
    $this->MultiCell(185,5,iconv( "UTF-8", "ISO-8859-1","für Ihre Vermittlungsleistungen erlauben wir uns Ihnen diese Abrechnung zu übersenden."));
    $this->SetXY(15,119);
    $this->MultiCell(185,5,iconv( "UTF-8", "ISO-8859-1","Die Aufstellung der Einzelposten finden Sie in der Anlage ab Seite 2."));

    $this->SetXY(15,130);
    $this->Cell(106,5,"Buchungsart",0,0,'L',1);
    $this->Cell(27,5,"Netto",0,0,'L',1);
    $this->Cell(25,5,"USt. (19%)",0,0,'L',1);
    $this->Cell(27,5,"Brutto",0,0,'R',1);

    $this->SetLineWidth(0.1);
    $this->Line(16,135,200,135);
    define('EURO',chr(128));
    $this->ln(10);
    $this->SetX(15);
    $this->Cell(106,5,"Superprovision $v3",0,0,'L',1);
    $this->Cell(27,5,"$v4 108,00 ".EURO,0,0,'L',1);
    $this->Cell(25,5,"$v5 ",0,0,'L',1);
    $this->Cell(27,5,"$v6 128,52 ".EURO,0,0,'R',1);
    {
        $w = $this->GetY();
        $this->SetXY(15,$w+8);
        $this->Cell(106,5,"Strukturprovision",0,0,'L',1);
        $this->Cell(27,5," $v7 108,00 ".EURO,0,0,'L',1);
        $this->Cell(25,5,"$v8 ",0,0,'L',1);
        $this->Cell(27,5,"$v9 128,52 ".EURO,0,0,'R',1);
    }

    $wa = $this->GetY();
    $this->Line(16,$wa+10,200,$wa+10);
    $this->ln(11);
    $this->SetX(15);
    $this->Cell(40,5,"Abrechnungsbetrag",0,0,'L',1);
    //hier Variable
    $this->Cell(93,5,iconv( "UTF-8", "ISO-8859-1","$v14"),0,0,'L',1);

    {
    $this->Cell(25,5,"",0,0,'L',1);
    }
    $this->Cell(27,5,"$v10 864,00 ".EURO,0,0,'R',1);

    {
    $this->ln(5);
    $this->SetX(15);
    $this->Cell(40,5,"",0,0,'L',1);
    $this->Cell(93,5,iconv( "UTF-8", "ISO-8859-1","$v15"),0,0,'L',1);
    $this->Cell(25,5,"",0,0,'L',1);
    $this->Cell(27,5,"",0,0,'R',1);
    }


    $this->SetXY(15,178);
    $this->Cell(185,5,iconv( "UTF-8", "ISO-8859-1","Der Betrag in Höhe von $v10 276,08 EURO wird in den nächsten Tagen auf Ihr folgendes Konto überwiesen:"),0,0,'L',1);

    $this->SetXY(15,183);
    $this->Cell(185,5,iconv( "UTF-8", "ISO-8859-1","IBAN $v11; BIC $v12"),0,0,'L',1);

    $this->SetXY(15,195);
    $this->MultiCell(185,5,iconv( "UTF-8", "ISO-8859-1","Bei Fragen bitte unbedingt Beleg und Tippgeber-Nr. mit angeben!"));

    $this->SetXY(15,230);
    $this->MultiCell(185,5,iconv( "UTF-8", "ISO-8859-1","*Sie haben uns gemeldet, dass Sie $v13 umsatzsteuerpflichtig sind."));

}

function combo_3 ($v1)
{
    $this->SetFont('Arial','B',10);
    $this->SetDrawColor(0,0,0);
    $this->SetTextColor(0,0,0);
    $this->SetFillColor(255,255,255);
    $this->SetLineWidth(0.0);

    $this->SetXY(15,45);
    $this->Cell(45,5,iconv( "UTF-8", "ISO-8859-1","PROVISIONGUTSCHRIFT,"),0,0,'L',1);
    $this->SetFont('Arial','',10);
    $this->Cell(26,5,iconv( "UTF-8", "ISO-8859-1","Einzelauflistung"),0,0,'L',1);
    $this->SetFont('Arial','B',10);
    $this->Cell(50,5,iconv( "UTF-8", "ISO-8859-1","$v1"),0,0,'L',1);

}

    function combo_4 ($v1)
    {
        $this->SetFont('Arial','B',10);
        $this->SetDrawColor(0,0,0);
        $this->SetTextColor(0,0,0);
        $this->SetFillColor(255,255,255);
        $this->SetLineWidth(0.0);

        $this->ln(10);
        $this->SetFont('Arial','',10);
        $this->SetX(15);
        $this->Cell(45,5,iconv( "UTF-8", "ISO-8859-1","$v1"),0,0,'L',1);

    }


    function combo_5 ()
    {
        $this->SetFont('Arial','B',10);
        $this->SetDrawColor(0,0,0);
        $this->SetTextColor(0,0,0);
        $this->SetFillColor(255,255,255);
        $this->SetLineWidth(0.0);

        $this->ln(10);
        $this->SetX(15);
        $this->SetFont('Arial','B',7);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1","Afilliate"),1,0,'L',1);
        $this->Cell(23,5,iconv( "UTF-8", "ISO-8859-1","Zeichnungsdatum"),1,0,'L',1);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1","Kunde"),1,0,'L',1);
        $this->Cell(24,5,iconv( "UTF-8", "ISO-8859-1","Vertragsnummer"),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1","Betrag"),1,0,'L',1);
        $this->Cell(30,5,iconv( "UTF-8", "ISO-8859-1","Investment Projekt"),1,0,'L',1);
        $this->Cell(22,5,iconv( "UTF-8", "ISO-8859-1","Conversion-Wert"),1,0,'L',1);

    }

    function combo_6 ($v1,$v2,$v3,$v4,$v5,$v6,$v7)
    {
        $this->SetFont('Arial','B',10);
        $this->SetDrawColor(0,0,0);
        $this->SetTextColor(0,0,0);
        $this->SetFillColor(255,255,255);
        $this->SetLineWidth(0.0);

        $this->SetFont('Arial','',7);
        define('EURO',chr(128));

        $this->ln();
        $this->SetX(15);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1","$v1"),1,0,'L',1);
        $this->Cell(23,5,iconv( "UTF-8", "ISO-8859-1","$v2 30-01-2019"),1,0,'L',1);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1","$v3 Kathrin Krais"),1,0,'L',1);
        $this->Cell(24,5,iconv( "UTF-8", "ISO-8859-1","$v4 MYW7643568077"),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1","$v5 2.000,00 ").EURO,1,0,'R',1);
        $this->Cell(30,5,iconv( "UTF-8", "ISO-8859-1","$v6 Logistikzins Nr. 02"),1,0,'L',1);
        $this->Cell(22,5,iconv( "UTF-8", "ISO-8859-1","$v7 54,00 ").EURO,1,0,'R',1);

        $this->ln();
        $this->SetX(15);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1","$v1"),1,0,'L',1);
        $this->Cell(23,5,iconv( "UTF-8", "ISO-8859-1","$v2 30-01-2019"),1,0,'L',1);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1","$v3 Cornelius Link"),1,0,'L',1);
        $this->Cell(24,5,iconv( "UTF-8", "ISO-8859-1","$v4 MYW7643568077"),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1","$v5 10.000,00 ").EURO,1,0,'R',1);
        $this->Cell(30,5,iconv( "UTF-8", "ISO-8859-1","$v6 Logistikzins Nr. 02"),1,0,'L',1);
        $this->Cell(22,5,iconv( "UTF-8", "ISO-8859-1","$v7 270,00 ").EURO,1,0,'R',1);

        $this->ln();
        $this->SetX(15);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1","$v1"),1,0,'L',1);
        $this->Cell(23,5,iconv( "UTF-8", "ISO-8859-1","$v2 30-01-2019"),1,0,'L',1);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1","$v3 Stefan Schanz"),1,0,'L',1);
        $this->Cell(24,5,iconv( "UTF-8", "ISO-8859-1","$v4 MYW7643568077"),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1","$v5 2.000,00 ").EURO,1,0,'R',1);
        $this->Cell(30,5,iconv( "UTF-8", "ISO-8859-1","$v6 Logistikzins Nr. 02"),1,0,'L',1);
        $this->Cell(22,5,iconv( "UTF-8", "ISO-8859-1","$v7 54,00 ").EURO,1,0,'R',1);

        $this->ln();
        $this->SetX(15);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1","$v1"),1,0,'L',1);
        $this->Cell(23,5,iconv( "UTF-8", "ISO-8859-1","$v2 30-01-2019"),1,0,'L',1);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1","$v3 Andrea Le Claire Leibfritz"),1,0,'L',1);
        $this->Cell(24,5,iconv( "UTF-8", "ISO-8859-1","$v4 MYW7643568077"),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1","$v5 2.000,00 ").EURO,1,0,'R',1);
        $this->Cell(30,5,iconv( "UTF-8", "ISO-8859-1","$v6 Logistikzins Nr. 02"),1,0,'L',1);
        $this->Cell(22,5,iconv( "UTF-8", "ISO-8859-1","$v7 54,00 ").EURO,1,0,'R',1);

    }

    function combo_7 ($v1)
    {
        $this->SetFont('Arial','B',10);
        $this->SetDrawColor(0,0,0);
        $this->SetTextColor(0,0,0);
        $this->SetFillColor(255,255,255);
        $this->SetLineWidth(0.0);

        $this->SetFont('Arial','',7);


        $this->ln();
        $this->SetX(15);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(23,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(24,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'R',1);
        $this->Cell(30,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(22,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'R',1);

        $this->ln();
        $this->SetFont('Arial','B',7);
        $this->SetX(15);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(23,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(24,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'R',1);
        $this->Cell(30,5,iconv( "UTF-8", "ISO-8859-1","Gesamt netto"),1,0,'L',1);
        $this->Cell(22,5,iconv( "UTF-8", "ISO-8859-1","$v1 432,00 ").EURO,1,0,'R',1);

    }


    function combo_8 ()
    {
        $this->SetFont('Arial','B',10);
        $this->SetDrawColor(0,0,0);
        $this->SetTextColor(0,0,0);
        $this->SetFillColor(255,255,255);
        $this->SetLineWidth(0.0);


        $this->ln(20);
        $this->SetFont('Arial','',10);
        $this->SetX(15);
        $this->MultiCell(185,5,iconv( "UTF-8", "ISO-8859-1","Hinweis: In dieser Provisionsabrechnung finden nur die Vergütungsansprüche Berücksichtigung, die gemäß §6 des Tippgeber-Vertrages die Auszahlungsvoraussetzungen erfüllen."));

    }


    function combo_9 ()
    {
        $this->SetFont('Arial','B',10);
        $this->SetDrawColor(0,0,0);
        $this->SetTextColor(0,0,0);
        $this->SetFillColor(255,255,255);
        $this->SetLineWidth(0.0);

        $this->ln(10);
        $this->SetX(15);
        $this->SetFont('Arial','B',7);
        $this->Cell(26,5,iconv( "UTF-8", "ISO-8859-1","Supervermittler"),1,0,'L',1);
        $this->Cell(26,5,iconv( "UTF-8", "ISO-8859-1","Afilliate"),1,0,'L',1);
        $this->Cell(15,5,iconv( "UTF-8", "ISO-8859-1","Datum"),1,0,'L',1);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1","Kunde"),1,0,'L',1);
        $this->Cell(24,5,iconv( "UTF-8", "ISO-8859-1","Vertragsnummer"),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1","Betrag"),1,0,'L',1);
        $this->Cell(25,5,iconv( "UTF-8", "ISO-8859-1","Investment Projekt"),1,0,'L',1);
        $this->Cell(20,5,iconv( "UTF-8", "ISO-8859-1","Superprovision"),1,0,'L',1);

    }

    function combo_10 ($v1,$v2,$v3,$v4,$v5,$v6,$v7)
    {
        $this->SetFont('Arial','B',10);
        $this->SetDrawColor(0,0,0);
        $this->SetTextColor(0,0,0);
        $this->SetFillColor(255,255,255);
        $this->SetLineWidth(0.0);

        $this->SetFont('Arial','',7);
        define('EURO',chr(128));
        $this->ln();
        $this->SetX(15);
        $this->Cell(26,5,iconv( "UTF-8", "ISO-8859-1","$v1"),1,0,'L',1);
        $this->Cell(26,5,iconv( "UTF-8", "ISO-8859-1","$v1"),1,0,'L',1);
        $this->Cell(15,5,iconv( "UTF-8", "ISO-8859-1","$v2 30-01-2019"),1,0,'L',1);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1","$v3 Kathrin Krais"),1,0,'L',1);
        $this->Cell(24,5,iconv( "UTF-8", "ISO-8859-1","$v4 MYW7643568077"),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1","$v5 2.000,00 ").EURO,1,0,'R',1);
        $this->Cell(25,5,iconv( "UTF-8", "ISO-8859-1","$v6 Logistikzins Nr. 02"),1,0,'L',1);
        $this->Cell(20,5,iconv( "UTF-8", "ISO-8859-1","$v7 54,00 ").EURO,1,0,'R',1);
    }

    function combo_11 ($v1)
    {
        $this->SetFont('Arial','B',10);
        $this->SetDrawColor(0,0,0);
        $this->SetTextColor(0,0,0);
        $this->SetFillColor(255,255,255);
        $this->SetLineWidth(0.0);

        $this->SetFont('Arial','',7);


        $this->ln();
        $this->SetX(15);
        $this->Cell(26,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(26,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(15,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(24,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'R',1);
        $this->Cell(25,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(20,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'R',1);

        $this->ln();
        $this->SetFont('Arial','B',7);
        $this->SetX(15);
        $this->Cell(26,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(26,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(15,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(24,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'R',1);
        $this->Cell(25,5,iconv( "UTF-8", "ISO-8859-1","Gesamt netto"),1,0,'L',1);
        $this->Cell(20,5,iconv( "UTF-8", "ISO-8859-1","$v1 432,00 ").EURO,1,0,'R',1);

    }


    function combo_12 ()
    {
        $this->SetFont('Arial','B',10);
        $this->SetDrawColor(0,0,0);
        $this->SetTextColor(0,0,0);
        $this->SetFillColor(255,255,255);
        $this->SetLineWidth(0.0);

        $this->ln(10);
        $w = $this->GetY();

        $this->SetX(15);
        $this->SetFont('Arial','B',7);
        $this->MultiCell(16,5,iconv( "UTF-8", "ISO-8859-1","Struktur- vermittler"),1);
        $this->SetXY(31,$w);
        $this->MultiCell(16,5,iconv( "UTF-8", "ISO-8859-1","Superv- vermittler"),1);
        $this->SetXY(47,$w);
        $this->Cell(26,10,iconv( "UTF-8", "ISO-8859-1","Afilliate"),1,0,'L',1);
        $this->Cell(15,10,iconv( "UTF-8", "ISO-8859-1","Datum"),1,0,'L',1);
        $this->Cell(37,10,iconv( "UTF-8", "ISO-8859-1","Kunde"),1,0,'L',1);
        $this->Cell(24,10,iconv( "UTF-8", "ISO-8859-1","Vertragsnummer"),1,0,'L',1);
        $this->Cell(16,10,iconv( "UTF-8", "ISO-8859-1","Betrag"),1,0,'L',1);
        $this->Cell(25,10,iconv( "UTF-8", "ISO-8859-1","Investment Projekt"),1,0,'L',1);
        $this->MultiCell(14,5,iconv( "UTF-8", "ISO-8859-1","Struktur- provision"),1);

    }

    function combo_13 ($v1,$v2,$v3,$v4,$v5,$v6,$v7)
    {
        $this->SetFont('Arial','B',10);
        $this->SetDrawColor(0,0,0);
        $this->SetTextColor(0,0,0);
        $this->SetFillColor(255,255,255);
        $this->SetLineWidth(0.0);
        $w = $this->GetY();

        $this->SetFont('Arial','',7);
        define('EURO',chr(128));
        //$this->ln();
        $this->SetX(15);
        $this->MultiCell(16,5,iconv( "UTF-8", "ISO-8859-1","$v1"),1);
        $this->SetXY(31,$w);
        $this->MultiCell(16,5,iconv( "UTF-8", "ISO-8859-1","$v1"),1);
        $this->SetXY(47,$w);
        $this->Cell(26,10,iconv( "UTF-8", "ISO-8859-1","$v1"),1,0,'L',1);
        $this->Cell(15,10,iconv( "UTF-8", "ISO-8859-1","$v2 30-01-2019"),1,0,'L',1);
        $this->Cell(37,10,iconv( "UTF-8", "ISO-8859-1","$v3 Kathrin Krais"),1,0,'L',1);
        $this->Cell(24,10,iconv( "UTF-8", "ISO-8859-1","$v4 MYW7643568077"),1,0,'L',1);
        $this->Cell(16,10,iconv( "UTF-8", "ISO-8859-1","$v5 2.000,00 ").EURO,1,0,'R',1);
        $this->Cell(25,10,iconv( "UTF-8", "ISO-8859-1","$v6 Logistikzins Nr. 02"),1,0,'L',1);
        $this->Cell(14,10,iconv( "UTF-8", "ISO-8859-1","$v7 54,00 ").EURO,1,0,'R',1);
    }


    function combo_14 ($v1)
    {
        $this->SetFont('Arial','B',10);
        $this->SetDrawColor(0,0,0);
        $this->SetTextColor(0,0,0);
        $this->SetFillColor(255,255,255);
        $this->SetLineWidth(0.0);

        $this->SetFont('Arial','',7);


        $this->ln();
        $this->SetX(15);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(26,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(15,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(24,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'R',1);
        $this->Cell(25,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(14,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'R',1);

        $this->ln();
        $this->SetFont('Arial','B',7);
        $this->SetX(15);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(26,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(15,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(37,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(24,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'L',1);
        $this->Cell(16,5,iconv( "UTF-8", "ISO-8859-1",""),1,0,'R',1);
        $this->Cell(25,5,iconv( "UTF-8", "ISO-8859-1","Gesamt netto"),1,0,'L',1);
        $this->Cell(14,5,iconv( "UTF-8", "ISO-8859-1","$v1 432,00 ").EURO,1,0,'R',1);

    }


// Funktion Seitenfuss BEGINN  ====================================================================
function Footer() 
{
  // Page footer
  $akt_date_ = date("d.m.Y");
  $akt_date_uhr = date("H:i");
  if($this->PageNo() == 1) {
  $this->SetXY(15,-35);
  $this->SetDrawColor(0,0,0);  		         // Blaue Linie
  $this->SetFillColor(255,255,255); 		     // Blauer Hintergrund

    $this->SetTextColor(35,86,100);
    $this->SetFont('Arial','B',9);
    $this->Cell(124,6,iconv("UTF-8", "ISO-8859-1","Moneywell Vertriebsgesellschaft mbH"),0,0,'L',1);
    $this->SetFont('Arial','B',7);
    $this->SetTextColor(100,75,30);
    $this->Cell(30,6,iconv("UTF-8", "ISO-8859-1","Geschäftsführung"),0,0,'L',1);
    $this->SetTextColor(35,86,100);
    $this->Cell(37,6,iconv("UTF-8", "ISO-8859-1","Amtsgericht Hamburg"),0,0,'L',1);


    $this->SetXY(15,-30);
    $this->SetFont('Arial','B',7);
    $this->SetTextColor(35,86,100);
    $this->Cell(32,6,iconv("UTF-8", "ISO-8859-1","Englische Planke 2"),0,0,'L',1);
    $this->Cell(47,6,iconv("UTF-8", "ISO-8859-1","Tel. +49 (0) 911 323 919 - 65"),0,0,'L',1);
    $this->Cell(45,6,iconv("UTF-8", "ISO-8859-1","info@moneywell-vertrieb.de"),0,0,'L',1);
    $this->Cell(30,6,iconv("UTF-8", "ISO-8859-1","Andre Wreth"),0,0,'L',1);
    $this->Cell(37,6,iconv("UTF-8", "ISO-8859-1","HRB 148551"),0,0,'l',1);

    $this->SetXY(15,-26);
    $this->Cell(32,6,iconv("UTF-8", "ISO-8859-1","20459 Hamburg"),0,0,'L',1);
    $this->Cell(47,6,iconv("UTF-8", "ISO-8859-1","Fax +49 (0) 911 323 919 - 65"),0,0,'L',1);
    $this->Cell(45,6,iconv("UTF-8", "ISO-8859-1","www.moneywell-vertrieb.de"),0,0,'L',1);
    $this->Cell(30,6,iconv("UTF-8", "ISO-8859-1","Marc Schumann"),0,0,'L',1);
    $this->Cell(37,6,iconv("UTF-8", "ISO-8859-1","Ust.-IdNr. DE 31 42 25 216"),0,0,'l',1);
  }
}

    // Funktion Seitenfuss ENDE  ====================================================================
}  // class ends

require_once("../../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

$firma = $user_data->field('firma');
$vorname = $user_data->field('vorname');
$nachname = $user_data->field('nachname');
$strasse = $user_data->field('strasse');
$hausnr = $user_data->field('hausnummer');
$plz = $user_data->field('plz');
$ort = $user_data->field('ort');
$short_id = $user_data->field('short_id');
$steuerpflicht = $user_data->field('steuerpflicht');

$anrede_id = $user_data->field('anrede');
$anrede_name = $anrede_id == 1 ? 'Herr':'Frau';
$anrede = $anrede_id == 1 ? 'Sehr geehrter Herr':'Sehr geehrte Frau';

$steuer = explode(':',$user_data->field('steuernummer'));
$steuer_typ = $steuer[0];
$steuer_nr = $steuer[1];

$iban = $user_data->field('iban');
$bic = $user_data->field('bic');

if($steuerpflicht == 0) {
   $text1 = 'kein Steuerausweis aufgrund der Anwendung der';
   $text2 = 'Kleinunternehmerregelung (§ 19 UStG)';
   $ust1 = '0,00';
   $ust2 = '0,00';
   $star = '*';
   $nicht = 'nicht';
}
else {
   $text1 = '';
   $text2 = '';
   $nicht ='';
}


$eAf = new Entity_mapper($app->db(), 'abrechnung_test/1.0/config.xml');
$abr_data = $eAf->find_by_fields(['affiliate_param' => $short_id]);
if ($abr_data !== null)
{
    $inv_datum = $abr_data[0]->field('payin_settled_at');
    $inv_vorname = $abr_data[0]->field('investor_first_name');
    $inv_nachname = $abr_data[0]->field('investor_last_name');
    $inv_vertragsnummer = $abr_data[0]->field('reference_nr');
    $inv_betrag = $abr_data[0]->field('betrag');
    $inv_projekt = $abr_data[0]->field('emission_reference');

}

$heute = date('d.m.Y');
// start pdf creation
$pdf      = new PDF();

//  Seite 1   BEGINN   ==============================================================================
// create Header

$pdf->AddPage('P');

$image = "img/logo-moneywell.png";
$pdf->Image($image, 125, 0, 90,50);



$pdf->combo_1  //
(
    $firma."", // Firma
    $anrede_name." ".$vorname." ".$nachname, // Anrede, Name
    $strasse." ".$hausnr, // Strasse
    $plz." ".$ort, // Stadt
    $short_id."", // Tippgebernummer // start pdf creation
    $steuer_typ."", // Steuer/USt-ID
    $steuer_nr."", // Steuernummer
 "", // Belegnummer
 $heute.""  // Belegdatum
);
$pdf->combo_2  //
(
    "", // If overhead
    $anrede." ".$nachname, // Anrede Name
    "", // Buchungsart
    "", // Netto1
    $ust1." ".chr(128).$star, // USt.1
    "", // Brutto1
    "", // $v7 Netto2
    $ust2." ".chr(128).$star, // $v8 USt.2
    "", // $v9 Brutto2
    "", //  Brutto gesamt
    $iban."", // IBAN
    $bic."", // BIC
    $nicht."",  // (nicht)
    $text1."", //
    $text2."" //

);

//  Seite 2   BEGINN   ==============================================================================
$pdf->AddPage('P');
$image = "img/logo-moneywell.png";
$pdf->Image($image, 125, 0, 90,50);

$pdf->combo_3  //
(
    "P20190118"
);

// BEGINN 1.
$pdf->combo_5  //
(
);

$pdf->combo_6  //
(
    $vorname." ".$nachname,
    "",
    "",
    "",
    "",
    "",
    ""
);
$pdf->combo_7  //
(
    ""
);
// ENDE 1.



// BEGINN 2. Superprovision
$pdf->combo_4  //
(
    "Superprovision"
);
$pdf->combo_9 ();

$pdf->combo_10  //
(
    $vorname." ".$nachname,
    "",
    "",
    "",
    "",
    "",
    ""
);
$pdf->combo_11  //
(
    ""
);
// ENDE 2


// BEGINN 3. Strukturprovision

$pdf->combo_4  //
(
    "Strukturprovision"
);
$pdf->combo_12 ();
$pdf->combo_13  //
(
    $vorname." ".$nachname,
    "",
    "",
    "",
    "",
    "",
    ""
);

$pdf->combo_14  //
(
    ""
);
// ENDE 3

$pdf->combo_8();



$pdf->Output();

?>

