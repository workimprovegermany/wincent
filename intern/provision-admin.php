<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(7);
$token = $_GET['token'];

?>
<style>
    #overlay table tr td:nth-last-child(1), #overlay table tr td:nth-last-child(2){
        text-align: center;
    }

    @media (max-width: 979px){
        #overlay table tr td:first-child input {
            max-width: 85%;
        }

        #overlay>div {
            width: 90%;
        }
    }
</style>
<main>
    <div class="container-fluid">
        <?php
        $activeSetting = 3;
        include "inc/naviSettings.php";
        ?>
        <hr>

        <section class="pad300 pad15-b grid_12">
            <div class="pad10 size80 bg-intern border-radius">
                <div class="icon_provision weiss"></div>
            </div>
            <div class="inline-block pad015">
                <a class="tooltip" title="lorem ipsum amet"></a>
                <h3>Provision ändern</h3>
            </div>
        </section>
        <section class="pad15-b grid_12">
            <iframe src="iframe/iframe-table.php?token=<?php echo $token; ?>" frameborder="0" scrolling="no" width="100%" height="720px" id="iframe-table"></iframe>
        </section>
    </div>

    <div id="overlay">
        <div class="container-fluid">
            <i class="far fa-times-circle" id="js-close-overlay"></i>
            <form id="form_provision">
                <fieldset>
                    <legend>Provision bearbeiten:</legend>
                    <div class="grid_12 marg150 flex_center">
                        <div class="grid_3">
                            <label for="name">Name</label>
                        </div>
                        <div class="grid_2">
                            <input type="text" name="shorty" id="shorty" value="" disabled>
                        </div>
                        <div class="grid_7">
                            <input type="text" name="name" id="name" value="" disabled>
                            <input type="hidden" id="user_id">
                        </div>
                    </div>
                    <div class="grid_12 marg150" id="container_table_edit_provision">

                    </div>
                    <div class="grid_12">
                        <div class="add-provision">
                            <a href="#" id="add_provision"><i class="fas fa-plus-square"></i></a>
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</main>
<script>
    const app = new App(init_provision_admin);

    <?php
    include "../app-foundation/js/src/provision-admin.js";
    ?>
</script>

<?php
include "inc/footer.php";
?>
