<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(7);

?>
<style>
    table tr td:not(:first-child):not(:nth-child(2)) {
        text-align: center;
    }
</style>
<main>
    <div class="container-fluid">
        <?php
        $activeSetting = 3;
        include "inc/naviSettings.php";
        ?>
        <hr>
        <section class="pad300 pad15-b grid_12 flex_center">
            <div class="pad10 size80 bg-intern border-radius">
                <div class="icon_security weiss"></div>
            </div>
            <div class="inline-block pad015 max-content">
                <a class="tooltip" title="Hier können Sie neue Rollen anlegen und nicht mehr gewünschte löschen."></a>
                <h3>Rollen verwalten</h3>
            </div>
        </section>
        <section class="grid_12" id="admin_link">
            <div class="add-table-admin">
                <a href="" id="js_add_role"><i class="fas fa-plus-square"></i></a>
            </div>
        </section>
        <section class="pad300 grid_12">
            <div id="js_table_container"></div>
        </section>
    </div>

    <div id="overlay">
        <div class="container-fluid">
            <i class="far fa-times-circle" id="js-close-overlay"></i>
            <form id="form_add_role">
                <fieldset>
                    <legend>Neue Rolle anlegen:</legend>
                    <div class="grid_12 marg150 flex_center">
                        <div class="grid_3">
                            <label for="name">Name</label>
                        </div>
                        <div class="grid_9">
                            <input type="text" name="name" id="name" value="" autocomplete="off" required>
                        </div>
                    </div>
                    <div class="grid_12 marg150 flex_center">
                        <div class="grid_3">
                            <label for="beschreibung">Beschreibung</label>
                        </div>
                        <div class="grid_9">
                            <input type="text" name="beschreibung" id="beschreibung" value="" autocomplete="off" required>
                        </div>
                    </div>
                    <button type="submit" class="btn_03 float-right">
                        <span class="content">Rolle anlegen</span>
                        <span class="progress"><span></span></span>
                    </button>
                </fieldset>
            </form>
        </div>

    </div>
</main>
<script>
    const app = new App(init_role);
</script>

<?php
include "inc/footer.php";
?>
