<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(2);

$token = $_GET['token'];


?>
<main>
    <div class="container-fluid">
        <section class="grid_12">
            <div class="pad300 chart_info flex">
                <div class="grid_3">
                    <div>
                        <div class="size50 icon_investment"></div>
                        <div>
                            <p class="small80">Investment</p>
                            <b>20.768</b>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div>
                        <div class="size50 icon_hand-growth"></div>
                        <div>
                            <p class="small80">Gesamtzahlungen</p>
                            <b>82.768</b>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div>
                        <div class="size50 icon_buyer"></div>
                        <div>
                            <p class="small80">Zahlungen 2019</p>
                            <b>29.768</b>
                        </div>
                    </div>
                </div>
                <div class="grid_3">
                    <div class="grid_6">
                        <p class="block margauto">
                            <span class="small80">Aktuell (Brutto)</span>
                            <br>
                            <strong class="big160 pad150 block"><span class="counter" data-count="9254">0</span> €</strong>
                        </p>
                    </div>
                    <div class="grid_6">
                        <p class="block margauto">
                            <span class="small80">Aktuell (Netto)</span>
                            <br><strong class="big160 pad150 block"><span class="counter" data-count="3254">0</span> €</strong>
                        </p>
                    </div>

                </div>
            </div>
        </section>

        <section class="grid_12">
            <iframe src="iframe/iframe-table-old.php?<?php echo 'token='.$token; ?>&dataUrl=https://examples.wenzhixin.net.cn/examples/bootstrap_table/data" frameborder="0" scrolling="no" width="100%" height="300px" id="iframe-table"></iframe>
        </section>

    </div>

</main>
<script>
    const app = new App();
</script>

<?php
include "inc/footer.php";
?>



