<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(6);

?>
<style>
    ul.kacheln a:first-child {
        display: none;
    }
</style>
<main>
    <div class="container-fluid">
        <section class="pad300 grid_12">
            <?php
            $token = $_GET['token'];
            $tiles = array();
            array_push($tiles, new Tile('support-admin.html?type=support&token='.$token, 'Support', 'icon_support js-num-support js-support-tab'));
            array_push($tiles, new Tile('support-admin.html?type=vertrieb&token='.$token, 'Vertrieb', 'icon_business js-num-sales'));

            $tv = new Tile_view($tiles);
            $tv->generate_view_tile();
            ?>
        </section>
    </div>
</main>
<script>
    const app = new App(init_support_admin_menu);
</script>

<?php
include "inc/footer.php";
?>

