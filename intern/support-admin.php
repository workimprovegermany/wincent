<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(6);

$type = $_GET['type'];

?>
    <style>
        .support_fenster div[class*=support_].gelesen div:before {
            content: none;
        }
    </style>

    <main>
        <div class="container-fluid">
            <section class="grid_4 marg300">
                <ul id="chat_tabs">
                </ul>
            </section>
            <section class="grid_8 admin">
                <i class="fas fa-arrow-left" id="js-back-to-tabs"></i>
                <div class="support_fenster  grid_12">
                    <div class="grid_12" id="support_chat_fenster">

                    </div>
                    <div class="grid_12">
                        <form id="form-support-message" name="form-support-message">
                            <textarea name="support_nachricht" id="support_nachricht" rows="6" placeholder="Nachricht" required></textarea>
                            <button type="submit" class="btn_03 float-right">
                                <span class="content">Senden</span>
                                <span class="progress"><span></span></span>
                            </button>
                        </form>
                    </div>
                </div>

            </section>
        </div>

    </main>

    <script>
        const app = new App(initSupportAdmin);
    </script>

<?php
if(empty($type))
{
?>
    <script>
        let url = new URL(window.location.href),
            token = url.searchParams.get("token");
        window.location.replace(window.location.origin+'/intern/support-admin-menu.html?token='+token);
    </script>

<?php
}
?>

<?php
include "inc/footer.php";
?>
