<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(6);

?>
    <script>

        </script>
    <style>
        .support_fenster div[class*=support_].antwort_gelesen div:before {
            content: none;
        }
        @media(max-width: 479px){
            p.inline-block {
                display: block;
            }
        }
    </style>
    <main>
        <div class="container-fluid">
            <section class="grid_12">
                <div class="support_fenster grid_12">
                    <div class="grid_12" id="support_chat_fenster">

                    </div>
                    <div class="grid_12">
                        <form id="form-support-message" name="form-support-message">
                            <textarea name="support_nachricht" id="support_nachricht" rows="6" placeholder="Nachricht" required></textarea>
                            <div class="grid_12 pad15-t">
                                <p class="inline-block">Nachricht senden an:&nbsp;&nbsp;&nbsp;</p>
                                <div class="inline-block relative">
                                    <input type="radio" name="rd-empfaenger" id="rd-support" value="support" checked >
                                    <label for="rd-support">Support</label>
                                </div>
                                <div class="inline-block relative">
                                    <input type="radio" name="rd-empfaenger" id="rd-vertrieb" value="vertrieb" >
                                    <label for="rd-vertrieb">Vertrieb</label>
                                </div>

                            </div>
                            <button type="submit" class="btn_03 float-right">
                                <span class="content">Senden</span>
                                <span class="progress"><span></span></span>
                            </button>
                        </form>
                    </div>
                </div>

            </section>
        </div>

    </main>
    <script>
        const app = new App(initSupportUser);





    </script>

<?php
include "inc/footer.php";
?>
