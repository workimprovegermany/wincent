<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(7);

?>
<style>
    table tr td:last-child {
        text-align: center;
    }


</style>
<main>
    <div class="container-fluid">
        <?php
        $activeSetting = 3;
        include "inc/naviSettings.php";
        ?>
        <hr>
        <section class="pad300 pad15-b grid_12 flex_center">
            <div class="pad10 size80 bg-intern border-radius">
                <div class="icon_security weiss"></div>
            </div>
            <div class="inline-block pad015 max-content">
                <a class="tooltip" title="Hier können Sie die Rollen der einzelnen User verändern."></a>
                <h3>Userrollen verwalten</h3>
            </div>
        </section>

        <section class="pad300 grid_12">
            <div id="js_table_container"></div>
            <iframe src="iframe/iframe-table.php?token=<?php echo $token; ?>" frameborder="0" scrolling="no" width="100%" height="300px" id="iframe-table"></iframe>
        </section>
    </div>

</main>
<script>
    const app = new App(init_user_role);
</script>

<?php
include "inc/footer.php";
?>
