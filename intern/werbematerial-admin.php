<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(4);

$type = $_GET['type'];
?>
    <main>
        <div class="container-fluid">
            <section class="pad300 grid_12 flex_center">
                <div class="pad10 size80 bg-intern border-radius">
                    <div class="icon_settings weiss"></div>
                </div>
                <div class="inline-block pad015 max-content">
                    <a class="tooltip" title="lorem ipsum amet"></a>
                    <h3>Adminbereich Werbebanner & Werbematerial</h3>
                </div>
            </section>
            <section class="pad300 grid_12">
                <div id="tabs" class="download-table">
                    <ul>
                        <li><a href="#tab-1">Kunden</a></li>
                        <li><a href="#tab-2">Affiliate Partner</a></li>
                    </ul>
                    <div id="tab-1">
                        <ul class="sortable-list js-sortable-list" id="kunde">
                        </ul>
                    </div>
                    <div id="tab-2">
                        <ul class="sortable-list js-sortable-list" id="partner">
                        </ul>
                    </div>
                </div>
            </section>
            <section>
                <hr>
                <form id="form-werbematerial-admin" enctype="multipart/form-data">
                    <fieldset>
                        <legend>Datei hinzufügen</legend>

                        <div class="grid_12 marg150 flex_center">
                            <div class="grid_3">
                                <label for="table">Tabelle</label>
                            </div>
                            <div class="grid_9">
                                <select name="table" id="table">
                                    <option value="kunde">Kunden</option>
                                    <option value="partner">Affiliate Partner</option>
                                </select>
                            </div>
                        </div>

                        <div class="grid_12 marg150 flex_center">
                            <div class="grid_3">
                                <label for="beschreibung">Beschreibung</label>
                            </div>
                            <div class="grid_9">
                                <input type="text" name="beschreibung" id="beschreibung" placeholder="Beschreibung" required>
                            </div>
                        </div>

                        <div class="grid_12 marg150 flex_center">
                            <div class="grid_3">
                                <label for="format">Dateiformat</label>
                            </div>
                            <div class="grid_9">
                                <select name="format" id="format">
                                    <option value="word" data-accept-format="application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document">Word-Datei</option>
                                    <option value="pdf" data-accept-format="application/pdf">PDF-Datei</option>
                                    <option value="code" data-accept-format="application/msword, application/vnd.openxmlformats-officedocument.wordprocessingml.document, text/*, .rtf">Quellcode</option>
                                    <option value="video" data-accept-format="video/mp4,video/x-m4v,video/*">Video</option>
                                    <option value="jpg" data-accept-format="image/jpeg">Bild (Format jpg)</option>
                                    <option value="png" data-accept-format="image/png">Bild (Format png)</option>
                                </select>
                            </div>
                        </div>

                        <div class="grid_12 marg150 flex_center">
                            <div class="grid_3">
                                <label for="datei">Datei hochladen</label>
                            </div>
                            <div class="grid_9">
                                <input type="file" name="datei" id="datei" placeholder="Datei hochladen" required>
                            </div>
                        </div>

                        <input type="hidden" name="kategorie" id="kategorie" value="<?php echo $type; ?>" required>

                        <button type="submit" class="btn_03 float-right">
                            <span class="content">Hinzufügen</span>
                            <span class="progress"><span></span></span>
                        </button>


                    </fieldset>
                </form>
            </section>
        </div>

        <div id="overlay">
            <div class="container-fluid">
                <i class="far fa-times-circle" id="js-close-overlay"></i>
                <form id="form-edit-data">
                    <fieldset>
                        <legend>Daten bearbeiten:</legend>
                        <div class="grid_12 marg150 flex_center">
                            <div class="grid_3">
                                <label for="edit_table">Tabelle</label>
                            </div>
                            <div class="grid_9">
                                <select name="edit_table" id="edit_table">
                                    <option value="kunde">Kunden</option>
                                    <option value="partner">Affiliate Partner</option>
                                </select>
                            </div>
                        </div>
                        <div class="grid_12 marg150 flex_center">
                            <div class="grid_3">
                                <label for="edit_beschreibung">Beschreibung</label>
                            </div>
                            <div class="grid_9">
                                <input type="text" name="edit_beschreibung" id="edit_beschreibung" value="" required>
                            </div>
                        </div>
                        <div class="grid_12 marg150 flex_center">
                            <div class="grid_3">
                                <label for="edit_format">Dateiformat</label>
                            </div>
                            <div class="grid_9">
                                <input type="text" name="edit_format" id="edit_format" value="" disabled>
                            </div>
                        </div>
                        <div class="grid_12 marg150 flex_center">
                            <div class="grid_3">
                                <label for="edit_datei">Dateiname</label>
                            </div>
                            <div class="grid_9">
                                <input type="text" name="edit_datei" id="edit_datei" value="" disabled>
                            </div>
                        </div>

                        <input type="hidden" name="element_id" id="element_id" />
                        <button type="submit" class="btn_03 float-right">
                            <span class="content">Daten ändern</span>
                            <span class="progress"><span></span></span>
                        </button>
                    </fieldset>
                </form>
            </div>

        </div>
    </main>

    <script>
        const app = new App(initAdvertisingMediaAdmin);
    </script>

<?php
include "inc/footer.php";
?>