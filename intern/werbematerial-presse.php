<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(4);

$token = $_GET['token'];
$urlParameter = '?type=presse&token='.$token;
?>
    <main>
        <div class="container-fluid">
            <section class="pad300 grid_12">
                <div class="pad10 size80 bg-intern border-radius">
                    <div class="icon_newspaper weiss"></div>
                </div>
                <div class="inline-block pad015">
                    <a class="tooltip" title="lorem ipsum amet"></a>
                    <h3>Presseartikel</h3>
                </div>
            </section>

            <section class="grid_12" id="admin_link">
                <div class="add-table-admin">
                    <a href="<?php echo "werbematerial-admin.html".$urlParameter;?>"><i class="fas fa-plus-square"></i></a>
                </div>
            </section>

            <section class="pad300 grid_12">
                <div id="tabs" class="download-table">
                    <ul>
                        <li><a href="#tab-1">Kunden</a></li>
                        <li><a href="#tab-2">Affiliate Partner</a></li>
                    </ul>
                    <div id="tab-1">

                    </div>
                    <div id="tab-2">

                    </div>
                </div>
            </section>
        </div>
    </main>

    <script>
        const app = new App();
        createTableAdvertisingMedia('presse', 'kunde', 'tab-1', ['Nr.', 'Beschreibung', 'Format', 'Download'] );
        createTableAdvertisingMedia('presse', 'partner', 'tab-2', ['Nr.', 'Beschreibung', 'Format', 'Download'] );
    </script>

<?php
include "inc/footer.php";
?>