<?php
require_once("../WincentApp.php");
$app = WincentApp::get_instance();


$em = new Entity_mapper($app->db(), 'user/1.0/config.xml');
$user_data = $em->find_by_id($app->user);

include "inc/head.php";
include "inc/header.php";
getHeader(4);
?>
<main>
    <div class="container-fluid">
        <section class="pad300 grid_12">
            <?php
            $token = $_GET['token'];
            $tiles = array();
            array_push($tiles, new Tile('werbematerial-email.html?token='.$token, 'E-Mail Vorlagen', 'icon_email'));
            array_push($tiles, new Tile('werbematerial-videos.html?token='.$token, 'Videos', 'icon_video'));
            array_push($tiles, new Tile('werbematerial-werbebanner.html?token='.$token, 'Werbebanner', 'icon_homepage'));
            array_push($tiles, new Tile('werbematerial-presseartikel.html?token='.$token, 'Presseartikel', 'icon_newspaper'));
            array_push($tiles, new Tile('werbematerial-facebook.html?token='.$token, 'Facebook', 'icon_facebook'));
            array_push($tiles, new Tile('werbematerial-instagram.html?token='.$token, 'Instagram','icon_instagram'));


            $tv = new Tile_view($tiles);
            $tv->generate_view_tile();
            ?>
        </section>
    </div>
</main>

<script>
    const app = new App();
</script>

<?php
include "inc/footer.php";
?>
