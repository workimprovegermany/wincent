
function init_abrechnung_view() {

    entity_fields = ['investor_first_name', 'investor_last_name', 'reference_nr', 'betrag', 'emission_reference'];
    em = new Entity_manager('abrechnung_vorlauf_01', entity_fields);
    entity_filter = {gebucht: 0};
    em.load_by_filter(entity_filter, process_data_abrechnung_view);

    function process_data_abrechnung_view(data) {
        let data_header = ['Kunde', 'Vertragsnummer', 'Betrag', 'Projekt', 'Edit'],
            data_body = get_formatted_data_abrechnung_view_for_table(data);

        createTable(data_header, data_body, 'js_table_container', 'responsive resp_as_accordion');

         add_event_listener_copy_abrechnung();
    }

    function get_formatted_data_abrechnung_view_for_table(data) {
        let new_array = [];
        for (let i = 0; i < data.length; i++) {
            let tmp_array = [];
            tmp_array.push(data[i].investor_first_name + ' ' + data[i].investor_last_name);
            tmp_array.push(data[i].reference_nr);
            tmp_array.push(data[i].betrag.replace('.', ',') + ' €');
           // tmp_array.push('<b><span class="abr_id">'+data[i].id+'</span></b>');
            tmp_array.push(data[i].emission_reference);
            tmp_array.push('<span class="js-edit size30 icon_edit"></span>');

            new_array.push(tmp_array);
        }
        return new_array;
    }




    function add_event_listener_copy_abrechnung() {
        let elemente = document.getElementsByClassName('js-edit');
        for (let i = 0; i < elemente.length; i++) {
            elemente[i].addEventListener('click', function (ev) {
                show_overlay();
                create_table_edit_abrechnung();
            });
        }
       /* for(let i = 0; i < elemente.length; i++){
            elemente[i].addEventListener('click', function (ev) {
                let user_id = this.getAttribute('data-user');

                for(let i = 0; i < elemente.length; i++){
                        document.forms[0].name.value = provision.array_provision[i].user;
                        document.forms[0].shorty.value = provision.array_provision[i].short_id;
                        document.forms[0].user_id.value = provision.array_provision[i].uid;
                        create_table_edit_provision(user_id);
                }
                show_overlay();
            });
        } */

    }

}


function create_table_edit_abrechnung(){
    let data_header = ['Tippgeber', 'Provision', 'Prozent', 'Netto'],
        data_body = '';
    createTable(data_header, data_body,'container_table_edit_abrechnung');
}
