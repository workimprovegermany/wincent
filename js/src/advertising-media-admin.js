function initAdvertisingMediaAdmin(){
    app.check_page_access_restrictions(['DO_EVERYTHING'], window.location.origin);

    const selectFormat = document.getElementById('format');

    setAcceptFormatUpload(selectFormat);

    selectFormat.addEventListener('change', function () {
        setAcceptFormatUpload(this);
    });

    loadData();

    function setAcceptFormatUpload(element) {
        const acceptFormat = element.options[element.selectedIndex].getAttribute('data-accept-format');
        document.getElementById('datei').setAttribute('accept', acceptFormat);
    }

    function loadData(){

        let kategorie = document.getElementById('kategorie').value;
        xhrKunde=new XMLHttpRequest();

        xhrKunde.onreadystatechange=function()
        {
            if (xhrKunde.readyState===4 && xhrKunde.status===200)
            {
                let response = xhrKunde.responseText;
                const obj = JSON.parse(response);
                setListelEments(obj.data, 'kunde');
            }
        };

        xhrKunde.open('POST', 'api', true);
        xhrKunde.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        data = {
            event: {
                name: 'entity.load',
                data: {
                    entity_name: 'advertising_media',
                    entity_filter: {kategorie: kategorie, zielgruppe: 'kunde'}
                }
            }
        };

        xhrKunde.send("event="+JSON.stringify(data.event));

        xhrPartner=new XMLHttpRequest();

        xhrPartner.onreadystatechange=function()
        {
            if (xhrPartner.readyState===4 && xhrPartner.status===200)
            {
                let response = xhrPartner.responseText;
                const obj = JSON.parse(response);
                setListelEments(obj.data, 'partner');
            }
        };

        xhrPartner.open('POST', 'api', true);
        xhrPartner.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        data = {
            event: {
                name: 'entity.load',
                data: {
                    entity_name: 'advertising_media',
                    entity_filter: {kategorie: kategorie, zielgruppe: 'partner'}
                }
            }
        };

        xhrPartner.send("event="+JSON.stringify(data.event));
    }



    function setListelEments(data, zielgruppe){
        let sortedData = sortArrayByPosition(data);
        if(undefined !== sortedData){
            data = sortedData;
        }

        let htmlListElement =   '<li data-id="{{id}}" data-name="{{datei_name}}" data-zielgruppe="{{datei_ziel}}" class="flex">'+
            '<div><i class="fas fa-arrows-alt-v"></i></div>'+
            '<div>{{beschreibung}}</div>'+
            '<div>'+
            '<div class="btn_02 cursor-default {{format}}"><div class="icon_{{format}} weiss"></div></div>'+
            '</div>'+
            '<div>'+
            '<a href="{{datei}}" class="btn_02 animate " download=""><div class="icon_download weiss"></div></a>'+
            '</div>'+
            '<div class="js-edit"><i class="fas fa-edit"></i></div>'+
            '<div class="js-delete"><i class="fas fa-trash-alt"></i></div>'+
            '</li>';

        let htmlListe = "",
            htmlListElementReplaced = "";



        for(let i = 0; i < data.length; i++){
            let beschreibung = data[i].beschreibung;
            if(data[i].format === 'jpg' || data[i].format === 'png'){
                beschreibung = '<img src="'+ data[i].pfad+data[i].name+'" /><p>'+data[i].beschreibung+'</p>';
            } else if(data[i].format === 'video'){
                beschreibung = '<video controls>\n' +
                    '<source src="'+ data[i].pfad+data[i].name+'" type="video/mp4">\n' +
                    '</video><p>'+data[i].beschreibung+'</p>';
            }
            htmlListElementReplaced = htmlListElement.replace('{{beschreibung}}', beschreibung).replace('{{format}}', data[i].format).replace('{{format}}', data[i].format).replace('{{datei}}', data[i].pfad+data[i].name).replace('{{id}}', data[i].id).replace('{{datei_name}}', data[i].name).replace('{{datei_ziel}}', data[i].zielgruppe);
            htmlListe = htmlListe + htmlListElementReplaced;
        }

        document.getElementById(zielgruppe).innerHTML = htmlListe;
        setEventListenerDelete();
        setEventListenerEdit();
    }



    function deleteElement(ev) {
        let check = confirm('Wollen Sie diese Daten wirklich löschen?');
        if (check) {
            let id = ev.target.parentElement.parentElement.getAttribute('data-id');
            let data_name = ev.target.parentElement.parentElement.getAttribute('data-name');
            let data_ziel = ev.target.parentElement.parentElement.getAttribute('data-zielgruppe');

            xhrDelete=new XMLHttpRequest();

            xhrDelete.onreadystatechange=function()
            {
                if (xhrDelete.readyState===4 && xhrDelete.status===200)
                {
                    let response = xhrDelete.responseText;
                    loadData();
                    console.log(response);
                }
            };

            xhrDelete.open('POST', 'api', true);
            xhrDelete.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            data = {
                event: {
                    name: 'entity.delete',
                    data: {
                        entity_name: 'advertising_media',
                        entity_id: id,
                        entity_datei: data_name,
                        entity_ziel: data_ziel
                    }
                }
            };

            xhrDelete.send("event="+JSON.stringify(data.event));
        }
    }

    function setEventListenerDelete(){
        let elemente = document.getElementsByClassName('js-delete');
        for(let i = 0; i < elemente.length; i++){
            elemente[i].addEventListener('click', deleteElement);
        }
    }

    function setEventListenerEdit(){
        let elemente = document.getElementsByClassName('js-edit');
        for(let i = 0; i < elemente.length; i++){
            elemente[i].addEventListener('click', editElement);
        }
    }

    function editElement(ev){
        let id = ev.target.parentElement.parentElement.getAttribute('data-id');
        xhrEdit=new XMLHttpRequest();

        xhrEdit.onreadystatechange=function()
        {
            if (xhrEdit.readyState===4 && xhrEdit.status===200)
            {
                let response = xhrEdit.responseText;
                const obj = JSON.parse(response);
                setDataToEdit(obj.data[0]);
            }
        };

        xhrEdit.open('POST', 'api', true);
        xhrEdit.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        data = {
            event: {
                name: 'entity.load',
                data: {
                    entity_name: 'advertising_media',
                    entity_id: id
                }
            }
        };

        xhrEdit.send("event="+JSON.stringify(data.event));
    }

    function setDataToEdit(data){
        let form = document.getElementById('form-edit-data');
        form.edit_beschreibung.value = data.beschreibung;
        form.edit_table.value = data.zielgruppe;
        form.element_id.value = data.id;
        form.edit_format.value = data.format;
        form.edit_datei.value = data.name;
        document.getElementById('overlay').style.display = 'block';
    }

    document.getElementById('form-edit-data').onsubmit = function(ev){
        ev.preventDefault();
        let newValueBeschreibung = this.edit_beschreibung.value,
            newValueZielgruppe = this.edit_table.value,
            id= this.element_id.value;
        xhrUpdate=new XMLHttpRequest();

        xhrUpdate.onreadystatechange=function()
        {
            if (xhrUpdate.readyState===4 && xhrUpdate.status===200)
            {
                let response = xhrUpdate.responseText;
                const obj = JSON.parse(response);
                loadData();
                document.getElementById('overlay').style.display = 'none';
            }
        };

        xhrUpdate.open('POST', 'api', true);
        xhrUpdate.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        data = {
            event: {
                name: 'entity.update',
                data: {
                    entity_name: 'advertising_media',
                    entity_id: id,
                    entity_fields: {
                        beschreibung: newValueBeschreibung,
                        zielgruppe: newValueZielgruppe
                    }
                }
            }
        };

        xhrUpdate.send("event="+JSON.stringify(data.event));
    };

    document.getElementById('js-close-overlay').addEventListener('click', function (ev) {
        document.getElementById('overlay').style.display = 'none';
    });

    document.getElementById('form-werbematerial-admin').onsubmit = function(ev){
        var valid = this.checkValidity();
        if(valid) {
            ev.preventDefault();
            setButtonStateLoading();
            var file_data = document.getElementById('datei').files[0];
            var formdata = new FormData();
            formdata.append("file", file_data);

            var datei_vor = document.getElementById('datei').files[0].name,
                dateiname = datei_vor.replace(" ",""),
                beschreibung = document.getElementById('beschreibung').value,
                format = document.getElementById('format').value,
                zielgruppe = document.getElementById('table').value,
                kategorie = document.getElementById('kategorie').value;

            event = {
                name: 'admin.werbematerial_save',
                data: {
                    entity_name: 'advertising_media',
                    entity_folder: 'werbematerial',
                    entity_fields: {
                        beschreibung: beschreibung,
                        format: format,
                        name: dateiname,
                        kategorie: kategorie,
                        zielgruppe: zielgruppe
                    }
                }
            };


            formdata.append('event', JSON.stringify(event));

            xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4 ) {
                    response = xhr.responseText;
                    if(xhr.status === 200){
                        token = JSON.parse(response);
                        if(token['name'] === 'entity.saved') {
                            setButtonStateSuccess();
                            loadData();
                            document.forms[0].reset();
                        }
                    } else {
                        setButtonStateError();
                    }

                }
            };

            xhr.open('POST', 'api', true);
            xhr.send(formdata);

        }
    };

    $( ".js-sortable-list" ).sortable({
        update: function( ) {
            updatePosition(this);
        }
    });

    function updatePosition(element){
        let liElemente = element.children;
        for(let i = 0; i < liElemente.length; i++){
            let id = liElemente[i].getAttribute('data-id');

            xhrUpdate=new XMLHttpRequest();

            xhrUpdate.open('POST', 'api', true);
            xhrUpdate.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

            data = {
                event: {
                    name: 'entity.update',
                    data: {
                        entity_name: 'advertising_media',
                        entity_id: id,
                        entity_fields: {
                            position: i+1
                        }
                    }
                }
            };

            xhrUpdate.send("event="+JSON.stringify(data.event));
        }
    }
}
