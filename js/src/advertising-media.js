function createTableAdvertisingMedia(kategorie, zielgruppe, container, dataHeaderArray){
    let htmlTable = '<table>',
        obj = getTabelDataWidthCategory(kategorie, zielgruppe);

    obj = JSON.parse(obj.responseText);

    let data = sortArrayByPosition(obj.data);
    if(undefined === data){
        data = obj.data;
    }

    htmlTable = htmlTable + createTableHeadAdvMedia(dataHeaderArray);
    htmlTable = htmlTable +createTableBodyAdvMedia(data,dataHeaderArray);
    fillHtmlContainer(document.getElementById(container), htmlTable);

}

function getTabelDataWidthCategory(kategorie, zielgruppe) {
    xhr=new XMLHttpRequest();

    xhr.onreadystatechange=function()
    {
        if (xhr.readyState==4 && xhr.status==200)
        {
            let response = xhr.responseText;
            const obj = JSON.parse(response);
            return obj.data;
        }
    };

    xhr.open('POST', 'api', false);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'entity.load',
            data: {
                entity_name: 'advertising_media',
                entity_filter: {kategorie: kategorie, zielgruppe: zielgruppe},
                entity_fields: ['beschreibung', 'format', 'pfad', 'position', 'name']
            }
        }
    };

    xhr.send("event="+JSON.stringify(data.event));
    return xhr;
}

function createTableBodyAdvMedia(dataArray, dataHeaderArray){
    let html = '<tbody>';
    if(dataArray.length !== 0){
        for(let i = 0; i<dataArray.length; i++){
            let format = dataArray[i].format;
            if(format === 'jpg' || format === 'png'){
                html = html + '<tr>'
                    +'<td>'+ (i+1) +'</td>'
                    + '<td> <div class="flex_center"><div><img src="'+dataArray[i].pfad+dataArray[i].name+'" /></div><div><p>'+dataArray[i].beschreibung+'</p></div></div></td>'
                    + '<td><div class="btn_02 cursor-default '+format+'"><div class="icon_'+format+' weiss"></div></div></td>'
                    + '<td><a href="'+dataArray[i].pfad+dataArray[i].name+'" class="btn_02 animate " download=""><div class="icon_download weiss"></div></a></td>'
                    + '</tr>';
            } else if(format === 'video'){
                html = html + '<tr>'
                    +'<td>'+ (i+1) +'</td>'
                    + '<td> <div class="flex_center"><div><video controls><source src="'+dataArray[i].pfad+dataArray[i].name+'"></source></video></div><div><p>'+dataArray[i].beschreibung+'</p></div></div></td>'
                    + '<td><div class="btn_02 cursor-default '+format+'"><div class="icon_'+format+' weiss"></div></div></td>'
                    + '<td><a href="'+dataArray[i].pfad+dataArray[i].name+'" class="btn_02 animate " download=""><div class="icon_download weiss"></div></a></td>'
                    + '</tr>';
            } else {
                html = html + '<tr>'
                    +'<td>'+ (i+1) +'</td>'
                    + '<td><p>'+dataArray[i].beschreibung+'</p></td>'
                    + '<td><div class="btn_02 cursor-default '+format+'"><div class="icon_'+format+' weiss"></div></div></td>'
                    + '<td><a href="'+dataArray[i].pfad+dataArray[i].name+'" class="btn_02 animate " download=""><div class="icon_download weiss"></div></a></td>'
                    + '</tr>';
            }
        }
    } else {
        html = html + '<tr><td colspan="'+ dataHeaderArray.length+'">Keine Daten gefunden</td></tr>';
    }


    html = html + '</tbody></table>';
    return html;
}

function createTableHeadAdvMedia(dataArray){
    let html = '<thead><tr>';
    for(let i = 0; i<dataArray.length; i++){
        html = html + '<th>'+ dataArray[i] +'</th>';
    }
    html = html + '</tr></thead>';
    return html;
}

function fillHtmlContainer(htmlContainer, html){
    htmlContainer.innerHTML = html;
}

function sortArrayByPosition(dataArray){
    dataArray.sort(function(a, b){
        a = a.position;
        b = b.position;
        return a>b ? 1 : a<b ? -1 : 0;
    });
}