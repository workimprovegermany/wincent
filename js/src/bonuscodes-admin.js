function Bonuscode() {
    this.data = [];
}

var bonuscode = null;

function init_bonuscodes_admin()
{
    app.check_page_access_restrictions(['DO_EVERYTHING'], window.location.origin);

    bonuscode = new Bonuscode();
    entity_fields = ['name', 'bonus_bezeichnung', 'bonus_code', 'bonus_code_hoehe', 'bonus_datum_beginn', 'bonus_datum_ende', 'bonus_aktiv', 'bonus_status_id'];
    em = new Entity_manager('skynet_projects', entity_fields);
    entity_filter = {id: '>0'};
    em.load_by_filter(entity_filter, process_data_bonuscodes);


    function process_data_bonuscodes(data){
        data.forEach(function (e) {
            bonuscode.data.push(e);
        });

        let select = document.getElementById('project');
        for(let i = 0; i<data.length; i++){
            let option = document.createElement("option");
            option.text = data[i].name;
            option.value = data[i].id;
            select.add(option);
        }

        select.addEventListener('change', function () {
            set_data_bonuscode(this.options[this.selectedIndex].value);
        });

    }
    
    function set_data_bonuscode(id) {
        _id = id;
        if(!isNotEmpty(_id)){
            document.forms[0].reset();
            return false;
        }
        let data = bonuscode.data.filter(function (e) {
            return _id === e.id;
        });

        data = data[0];

        document.getElementById('beginn').value = get_date_formatted_DD_MM_YYYY_with_point(data.bonus_datum_beginn);
        document.getElementById('ende').value = get_date_formatted_DD_MM_YYYY_with_point(data.bonus_datum_ende);
        document.getElementById('bonuscode').value = data.bonus_code;
        document.getElementById('hoehe').value = data.bonus_code_hoehe.replace('.',',');
        let bonus_aktive = false;
        if(data.bonus_aktiv === '1')
            bonus_aktive = true;

        document.getElementById('bonus_activ').checked = bonus_aktive;
        
        let select = document.getElementById('bezeichnung');
        for(let i = 0; i<select.options.length; i++){
            if(select.options[i].value === data.bonus_status_id){
                select.options[i].selected = true;
                break;
            }

        }


    }

    document.getElementById('form_bonuscode').onsubmit = function(ev) {
        ev.preventDefault();
        setButtonStateLoading();
        let id = document.getElementById('project').options[document.getElementById('project').selectedIndex].value,
            status_id = document.getElementById('bezeichnung').options[document.getElementById('bezeichnung').selectedIndex].value,
            bezeichnung = document.getElementById('bezeichnung').options[document.getElementById('bezeichnung').selectedIndex].innerText,
            beginn = formate_date(document.getElementById('beginn').value, '00:00:00'),
            ende = formate_date(document.getElementById('ende').value, '23:59:59'),
            bonus_aktiv = document.getElementById('bonus_activ').checked;

        em = new Entity_manager('skynet_projects');
        entity_fields = {
            bonus_datum_beginn: beginn,
            bonus_datum_ende: ende,
            bonus_code: document.getElementById('bonuscode').value,
            bonus_code_hoehe: document.getElementById('hoehe').value.replace(',','.'),
            bonus_aktiv: bonus_aktiv,
            bonus_bezeichnung: bezeichnung,
            bonus_status_id: status_id
        };
        em.update(id, entity_fields, setButtonStateSuccess);
    };

    function formate_date(date, time){
        let date_array = date.split('.');
        return date_array[2]+'-'+date_array[1]+'-'+date_array[0]+' '+time;
    }

}