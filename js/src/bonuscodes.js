function init_bonuscodes(){
    entity_fields = ['name', 'bonus_bezeichnung', 'bonus_code', 'bonus_code_hoehe', 'bonus_datum_beginn', 'bonus_datum_ende'];
    em = new Entity_manager('skynet_projects', entity_fields);
    entity_filter = {bonus_aktiv: 1, bonus_datum_ende: '>'+getDateNow()};
    em.load_by_filter(entity_filter, process_data_bonuscodes);


    function process_data_bonuscodes(data){
        let data_header = ['Projektname', 'Beginn', 'Ende', 'Bonuscode', 'Bonushöhe', 'Bonusbezeichnung', ''],
            data_body = get_formatted_data_bonuscode_for_table(data);

        createTable(data_header, data_body, 'js_table_container', 'responsive resp_as_accordion');

        add_event_listener_copy_bonuscode();
    }

    function get_formatted_data_bonuscode_for_table(data){
        let new_array = [];
        for(let i = 0; i < data.length; i++){
            let tmp_array = [];
            tmp_array.push(data[i].name);
            tmp_array.push(getDateFormattedDay(data[i].bonus_datum_beginn));
            tmp_array.push(getDateFormattedDay(data[i].bonus_datum_ende));
            tmp_array.push('<b><span class="bonus">'+data[i].bonus_code+'</span></b>');
            tmp_array.push(data[i].bonus_code_hoehe.replace('.',',')+'%');
            tmp_array.push(data[i].bonus_bezeichnung);
            tmp_array.push('<span class="js_copy_bonus icon_copy size20"></span>');

            new_array.push(tmp_array);
        }
        return new_array;
    }

    function add_event_listener_copy_bonuscode() {
        let elemente = document.getElementsByClassName('js_copy_bonus');
        for(let i = 0; i < elemente.length; i++){
            elemente[i].addEventListener('click', function (ev) {
                this.style.transform = 'scale(1.1)';
                _this = this;
                copyToClipboard(this.parentElement.parentElement.getElementsByClassName('bonus')[0].innerText);
                setTimeout(function () {
                    _this.style.transform = 'scale(1)';
                }, 500);
            });
        }
    }

}