function Dashboard(){
    this.skynet_id = null;
    this.investment_ids = [];
    this.short_id = null;
}

var dashboard = new Dashboard();

function initDashboard() {
    document.getElementById('this_year').innerText = new Date().getFullYear();
    em = new Entity_manager('user');
    entity_filter = {id: app.user_id};
    em.load_by_filter(entity_filter, set_skynet_id);

    function set_skynet_id(data){
        dashboard.skynet_id = data[0].skynet_id;
        dashboard.short_id = data[0].short_id;
        em = new Entity_manager('skynet_affiliate_conversions');
        entity_filter = {affiliate: dashboard.skynet_id};
        em.load_by_filter(entity_filter, set_skynet_investment_id);
    }

    function set_skynet_investment_id(data){
        if(data.length > 0){
            for(let i = 0; i < data.length; i++){
                dashboard.investment_ids.push(data[i].investment_id)
            }
        }
        get_data_for_chart();
    }

    $(".doughnutChart").each(function (ev) {
        setDoughnutChart(this, $(this).data('chart'));
    });
}

function get_data_for_chart(){
    let date_six_month_ago = get_other_date_from_today(0, -5,0),
        array_for_filter = get_array_month_year_til_today(date_six_month_ago);
    if(app.user_can('DO_EVERYTHING')){
        em = new Entity_manager('skynet_admin');
        entity_filter = {_or: {year_and_month: array_for_filter, year: new Date().getFullYear()}};
        em.load_by_filter(entity_filter, set_data_for_chart, array_for_filter);
    } else {
        entity_fields = ['amount', 'checkout_finished_at', 'state'];
        em = new Entity_manager('skynet_investments', entity_fields);
        entity_filter = {id: dashboard.investment_ids};
        em.load_by_filter(entity_filter, set_data_for_chart, array_for_filter);
    }
}

function set_data_for_chart(data, array_last_six_month){

    let array = data.filter(function (element) {
        if(element.state === 'running'){
            for(let i = 0; i < array_last_six_month.length; i++){
                if(app.user_can('DO_EVERYTHING') &&element.year_and_month === array_last_six_month[i])
                    return true;
                if(!app.user_can('DO_EVERYTHING') && element.checkout_finished_at.substr(0,7) === array_last_six_month[i])
                    return true;
            }
            return false;
        }
        return false;
    });

    process_data_for_chart(array, 'chart_investment');

    array = data.filter(function (element) {
        if(element.state === 'running' || element.state === 'pending' ){
            for(let i = 0; i < array_last_six_month.length; i++){
                if(app.user_can('DO_EVERYTHING') && element.year_and_month === array_last_six_month[i])
                    return true;
                if(!app.user_can('DO_EVERYTHING') && element.checkout_finished_at.substr(0,7) === array_last_six_month[i])
                    return true;
            }
            return false;
        }
        return false;
    });
    process_data_for_chart_running_and_pending(array, 'chart_total_payments');

    array = data.filter(function (element) {
        if(app.user_can('DO_EVERYTHING'))
            return (element.state === 'running' && parseInt(element.year) === new Date().getFullYear());

        return (element.state === 'running' && parseInt(element.checkout_finished_at.substr(0,4)) === new Date().getFullYear());
    });
    if(app.user_can('DO_EVERYTHING'))
        process_data_for_chart(array, 'chart_total_payments_this_year');
    else
        process_data_this_year_for_chart_user(array, 'chart_total_payments_this_year');

    get_data_last_seven_days();

}

function process_data_for_chart(data, container_id){
    if(!app.user_can('DO_EVERYTHING'))
        return process_data_for_chart_user(data, container_id);

    let data_chart = '',
        data_labels = '',
        sum = 0;
    for(let i = 0; i < data.length; i++){
        sum += parseInt(data[i].sum);
        if(i === 0){
            data_chart += data[i].sum;
            data_labels += app.array_month_name_short[data[i].month];
        } else {
            data_chart += ','+data[i].sum;
            data_labels += ','+app.array_month_name_short[data[i].month];
        }

    }
    let element = document.getElementById(container_id);
    setFilledLineChart(element, data_chart, data_labels);
    element.parentNode.getElementsByTagName('b')[0].innerText = formatThousandPoint(sum)+' €';
}

function process_data_for_chart_running_and_pending(data, container_id){
    if(!app.user_can('DO_EVERYTHING'))
        return process_data_for_chart_user(data, container_id);

    let data_chart = '',
        data_labels = '',
        sum = 0,
        obj = {};

    for(let i = 0; i < data.length; i++){

        sum += parseInt(data[i].sum);
        let key = 'd_'+data[i].year+'_'+data[i].month;
        if(!obj[key]){
            obj[key] = data[i].sum
        } else {
            obj[key] = parseInt(obj[key]) + parseInt(data[i].sum);
        }
    }

    let tmp_array = [];
    for(let key in obj)
        tmp_array.push([key, obj [key]]);


    for(let i = 0; i < tmp_array.length; i++){
        if(i === 0){
            data_chart += ''+tmp_array[i][1];
            data_labels += app.array_month_name_short[parseInt(tmp_array[i][0].substr(7))];
        } else {
            data_chart += ','+tmp_array[i][1];
            data_labels += ','+app.array_month_name_short[parseInt(tmp_array[i][0].substr(7))];
        }

    }
    let element = document.getElementById(container_id);
    setFilledLineChart(element, data_chart, data_labels);
    element.parentNode.getElementsByTagName('b')[0].innerText = formatThousandPoint(sum)+' €';
}

function get_array_month_year_til_today(date_ago){
    let tmp_array = date_ago.split('-'),
        year_ago = parseInt(tmp_array[0]),
        month_ago = parseInt(tmp_array[1]),
        current_year = new Date().getFullYear(),
        current_month = new Date().getMonth() +1,
        array_data = [];

    for(let y = year_ago; y <= current_year; y++){
        if(y === current_year){
            for(let m = 1; m <= current_month; m++){
                array_data.push(y+'-'+add_leading_null(m));
            }
        } else {
            for(let m = month_ago; m <= 12; m++){
                array_data.push(y+'-'+add_leading_null(m));
            }
        }
    }
    return array_data;
}

function get_data_last_seven_days(){
    let date_seven_days_ago = get_other_date_from_today(-6, 0,0);
    entity_fields = ['checkout_finished_at', 'state', 'amount'];
    em = new Entity_manager('skynet_investments', entity_fields);
    if(app.user_can('DO_EVERYTHING')){
        entity_filter = {checkout_finished_at : '>'+date_seven_days_ago};

    } else {
        entity_filter = {checkout_finished_at : '>'+date_seven_days_ago, id: dashboard.investment_ids};
    }
    em.load_by_filter(entity_filter, process_data_for_investment_all_state);
}

function process_data_for_investment_all_state(data){
    let array_running = data.filter(function (element) {
        return element.state === 'running';
    });

    let data_running = process_data_for_big_chart(array_running);

    let array_pending = data.filter(function (element) {
        return element.state === 'pending';
    });
    let data_pending = process_data_for_big_chart(array_pending);

    let array_canceled = data.filter(function (element) {
        return element.state === 'canceled';
    });
    let data_canceled = process_data_for_big_chart(array_canceled);

    let element = document.getElementById('chart_last_seven_days');

    let data_labels = '',
        array_last_seven_days = get_last_seven_days();

    for(let i = 0; i < array_last_seven_days.length; i++){
        if(i === 0) {
            data_labels += get_short_name_day(array_last_seven_days[i]);
        } else {
            data_labels += ','+get_short_name_day(array_last_seven_days[i]);
        }
    }
    setThreeLineChart(element , data_running, 'Aktiv', data_pending, 'Ausstehend', data_canceled, 'Abgebrochen', data_labels);
    get_number_investors();
}

function process_data_for_big_chart(data){
    let array_last_seven_days = get_last_seven_days(),
        tmp_array = [0,0,0,0,0,0,0];
    if(data.length === 0)
        return format_data_array_for_chart(tmp_array);

    for(let i = 0; i < array_last_seven_days.length; i++){
        for(let x = 0; x < data.length; x++){

            if(data[x].checkout_finished_at.substr(0,10) === array_last_seven_days[i].substr(0,10)){
                tmp_array[i] += parseInt(data[x].amount);
            }
        }
    }

    return format_data_array_for_chart(tmp_array);
}

function format_data_array_for_chart(array){
    let data_chart = '';

    for(let i = 0; i < array.length; i++){
        if(i === 0){
            data_chart += ''+array[i];
        } else {
            data_chart += ','+array[i];
        }
    }

    return data_chart;
}

function get_number_investors(){
    if(app.user_can('DO_EVERYTHING')){
        entity_fields = ['sum'];
        em = new Entity_manager('skynet_admin', entity_fields);
        entity_filter = {state: 'investors'};
        em.load_by_filter(entity_filter, process_number_investors);
    } else {
        entity_fields = ['sum'];
        em = new Entity_manager('skynet_investments', entity_fields);
        entity_filter = {id: dashboard.investment_ids};
        em.load_by_filter(entity_filter, process_number_investors_user);
    }
}

function process_number_investors(data) {
    document.getElementById('number_investors').innerText = formatThousandPoint(data[0].sum);
    get_site_visitors();
}

/*********+ USER ***********/
function process_data_for_chart_user(data, container_id) {

    let data_chart = '',
        data_labels = '',
        sum = 0,
        obj = {};

    for(let i = 0; i < data.length; i++){

        sum += parseInt(data[i].amount);
        let key = app.array_month_name_short[parseInt(data[i].checkout_finished_at.substr(5,7))];
        if(!obj[key]){
            obj[key] = data[i].amount
        } else {
            obj[key] = parseInt(obj[key]) + parseInt(data[i].amount);
        }
    }

    let last_six_month = get_last_six_month();

    let obj_month = {};
    for(let i = 0; i < last_six_month.length; i++){
        let month = app.array_month_name_short[last_six_month[i]];
        obj_month[month] = 0;
    }
    let tmp_array = [];
    for(let key in obj_month){
        if(obj[key])
            obj_month[key] = obj[key];

        tmp_array.push([key, obj_month [key]]);
    }

    for(let i = 0; i < tmp_array.length; i++){
        if(i === 0){
            data_chart += ''+tmp_array[i][1];
            data_labels += tmp_array[i][0];
        } else {
            data_chart += ','+tmp_array[i][1];
            data_labels += ','+tmp_array[i][0];
        }

    }
    let element = document.getElementById(container_id);
    setFilledLineChart(element, data_chart, data_labels);
    element.parentNode.getElementsByTagName('b')[0].innerText = formatThousandPoint(sum)+' €';
}

function get_last_six_month(){
    let tmp_array = [];
    for(let i = 6; i >=0; i--){
        let month_ago = i * (-1),
            date_ago = get_other_date_from_today(0, month_ago,0);
        tmp_array.push(parseInt(date_ago.substr(5,7)));
    }
    return tmp_array;
}

function process_data_this_year_for_chart_user(data, container_id) {
    let data_chart = '',
        data_labels = '',
        sum = 0,
        obj = {};

    for(let i = 0; i < data.length; i++){

        sum += parseInt(data[i].amount);
        let key = app.array_month_name_short[parseInt(data[i].checkout_finished_at.substr(5,7))];
        if(!obj[key]){
            obj[key] = data[i].amount
        } else {
            obj[key] = parseInt(obj[key]) + parseInt(data[i].amount);
        }
    }

    let month_this_year_ago = get_month_this_year_ago();

    let obj_month = {};
    for(let i = 0; i < month_this_year_ago.length; i++){
        let month = app.array_month_name_short[month_this_year_ago[i]];
        obj_month[month] = 0;
    }
    let tmp_array = [];
    for(let key in obj_month){
        if(obj[key])
            obj_month[key] = obj[key];

        tmp_array.push([key, obj_month [key]]);
    }

    for(let i = 0; i < tmp_array.length; i++){
        if(i === 0){
            data_chart += ''+tmp_array[i][1];
            data_labels += tmp_array[i][0];
        } else {
            data_chart += ','+tmp_array[i][1];
            data_labels += ','+tmp_array[i][0];
        }

    }
    let element = document.getElementById(container_id);
    setFilledLineChart(element, data_chart, data_labels);
    element.parentNode.getElementsByTagName('b')[0].innerText = formatThousandPoint(sum)+' €';
}

function get_month_this_year_ago(){
    let tmp_array = [],
        current_month = new Date().getMonth();
    for(let i = current_month; i >= 0; i--){
        let month_ago = i * (-1),
            date_ago = get_other_date_from_today(0, month_ago,0);
        tmp_array.push(parseInt(date_ago.substr(5,7)));
    }
    return tmp_array;
}

function process_number_investors_user(data) {
    document.getElementById('number_investors').innerText = formatThousandPoint(data.length);
    get_site_visitors();
}

/************ SEITENBESUCHER ************/
function get_site_visitors(){
    em = new Entity_manager('user_tracking_zins');
    if(app.user_can('DO_EVERYTHING')){
        entity_filter = {short_id: '>0'};
    } else {
        entity_filter = {short_id: dashboard.short_id};
    }
    em.load_by_filter(entity_filter, set_site_visitors);
}

function set_site_visitors(data){
    let array_last_seven_days = get_last_seven_days(),
        tmp_array = [0,0,0,0,0,0,0],
        data_chart = '',
        number_all_visitors = 0;

    if(data.length === 0) {
        data_chart  = format_data_array_for_chart(tmp_array);
    } else {
        for(let i = 0; i < array_last_seven_days.length; i++){
            let number_visitors = 0;
            for(let x = 0; x < data.length; x++){

                if(data[x].last_mod.substr(0,10) === array_last_seven_days[i].substr(0,10)){
                    number_visitors++;
                    number_all_visitors ++;
                }
            }
            tmp_array[i] = number_visitors;
        }
    }

    data_chart = format_data_array_for_chart(tmp_array);

    let data_labels = '';

    for(let i = 0; i < array_last_seven_days.length; i++){
        if(i === 0) {
            data_labels += get_short_name_day(array_last_seven_days[i]);
        } else {
            data_labels += ','+get_short_name_day(array_last_seven_days[i]);
        }
    }

    let element = document.getElementById('chart_visitors');
    setFilledLineChart(element, data_chart, data_labels);
    element.parentNode.getElementsByTagName('b')[0].innerText = formatThousandPoint(number_all_visitors);

}
