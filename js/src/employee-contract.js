function init_employee_contract() {

    init_event_listener_signature();

    em = new Entity_manager('user');
    entity_filter = {id: app.user_id};
    em.load_by_filter(entity_filter, get_provision);

    function get_provision(data){
        em = new Entity_manager('user_provision_struktur');
        //entity_filter = {uid: data[0].skynet_id};
        entity_filter = {uid: 2};
        em.load_by_filter(entity_filter, set_provision);
    }

    function set_provision(data) {
        let promille = 'Keine Angaben gefunden',
            verguetung = 'Keine Angaben gefunden';
        if(data.length > 0){
            promille = data[0].u_provision.split('.')[0]+ '‰';
            verguetung = data[0].u_provision_overhead.split('.')[0]+ '‰';

        }
        document.getElementById('provision').value = promille;
        document.getElementById('verguetung').value = verguetung;
    }

    $('.sigPad').signaturePad({drawOnly:true, drawBezierCurves:true, lineTop:90});

    document.getElementById('form_employee_contract').onsubmit = function(ev){
        ev.preventDefault();
        let signature = document.getElementById('sig').value;

        if(!signature){
            set_signature_error();
        } else {
            let provision = document.getElementById('provision').value.split('‰')[0],
                verguetung = document.getElementById('verguetung').value,
                canvas_img_data = get_clipped_image_url(document.getElementById('sig_1')),
                img_1 = canvas_img_data.replace(/^data:image\/(png|jpg);base64,/, "");
        }


    };

    function init_event_listener_signature() {
        document.getElementById('sig_1').ontouchend = function (ev) {
            setTimeout(function () {
                if(document.getElementById('sig').value){
                    enable_buttons();
                }
            }, 500);
        };

        document.getElementById('sig_1').onmouseup = function (ev) {
            setTimeout(function () {
                if(document.getElementById('sig').value){
                    enable_buttons();
                }
            }, 500);

        };

        document.getElementsByClassName('clearButton')[0].addEventListener('click', function () {
            disable_buttons();
        });
    }

    function enable_buttons(){
        let buttons = document.getElementsByClassName('js-toggle-disabled');
        remove_attribute(buttons, 'disabled');
    }

    function disable_buttons(){
        let buttons = document.getElementsByClassName('js-toggle-disabled');
        add_attribute(buttons, 'disabled');
    }

    function set_signature_error(){
        document.getElementById('sigArea_1').classList.add('error');

        document.getElementById('sig_1').ontouchend = function (ev) {
            document.getElementById('sigArea_1').classList.remove('error');
        };

        document.getElementById('sig_1').onmouseup = function (ev) {
            document.getElementById('sigArea_1').classList.remove('error');
        };
    }

    function get_clipped_image_url(canvas) {
        let canvas_width=450,
            canvas_height=200,
            clipped_canvas_width = canvas_width - 3,
            clipped_canvas_height = canvas_height - 3,
            canvas_hidden = document.createElement('canvas');

        canvas_hidden.style.display = 'none';
        document.body.appendChild(canvas_hidden);
        canvas_hidden.width = canvas_width-3;
        canvas_hidden.height = canvas_height-3;

        let ctx_hidden = canvas_hidden.getContext('2d');

        ctx_hidden.drawImage(canvas,2,2,clipped_canvas_width,clipped_canvas_height,0,0,clipped_canvas_width,clipped_canvas_height);

        return canvas_hidden.toDataURL('image/png');
    }
}