function initInfoboxAdmin()
{
    app.check_page_access_restrictions(['DO_EVERYTHING'], window.location.origin);

    let box_text = document.getElementById('box_text');
    document.getElementById('text').addEventListener('keyup', function (ev) {
        box_text.innerText = this.value;
    });
    let radios = document.forms[0].elements.art;
    for(let i = 0; i < radios.length; i++){
        radios[i].addEventListener('change', function (ev) {
            let val = document.forms[0].elements.art.value,
                box_art = document.getElementById('box_art'),
                box_icon = document.getElementById('box_icon');

            if(val === 'info'){
                changeClass(box_art, 'infobox_user_info', 'infobox_user_note');
                changeClass(box_icon, 'fa-info', 'fa-exclamation');

            } else {
                changeClass(box_art, 'infobox_user_note', 'infobox_user_info');
                changeClass(box_icon, 'fa-exclamation', 'fa-info');
            }
        })
    }

    document.getElementById('form_infobox').onsubmit = function(ev){
        let valid = this.checkValidity();
        if(valid) {
            ev.preventDefault();
            setButtonStateLoading();

            let art = document.forms[0].elements.art.value,
                text = document.getElementById('text').value,
                datum_ab = getTimestamp(document.getElementById('datum_ab').value),
                datum_bis = getTimestamp(document.getElementById('datum_bis').value);

            //DATEN SENDEN
            $.ajax({
                url: 'api',
                type: 'POST',
                data: {
                    event: {
                        name: 'entity.save',
                        data: {
                            entity_name: 'infoboxes',
                            entity_fields: {
                                art: art,
                                text: text,
                                datum_ab: datum_ab,
                                datum_bis: datum_bis
                            }
                        }
                    },
                },

                success:function(response){
                    setButtonStateSuccess();
                    document.getElementById('form_infobox').reset();
                    resetBoxText();
                    loadData();
                },
                error: function (response) {
                    setButtonStateError();
                }
            });
        }
    };

    function resetBoxText(){
        document.getElementById('box_text').innerText = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat.";
        box_art.classList.remove('infobox_user_info');
        box_art.classList.add('infobox_user_note');
        box_icon.classList.remove('fa-info');
        box_icon.classList.add('fa-exclamation');
    }

    loadData();

    function loadData(){
        loadDataInfo();
        loadDataNote();
    }

    function loadDataInfo(){
        xhr=new XMLHttpRequest();

        xhr.onreadystatechange=function()
        {
            if (xhr.readyState==4 && xhr.status==200)
            {
                let response = xhr.responseText;
                const obj = JSON.parse(response);
                createTable(['Box', 'Gültig ab', 'Gültig bis'], createArrayForTable(obj.data), 'table-infobox');
            }
        };

        xhr.open('POST', 'api', true);
        xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        data = {
            event: {
                name: 'entity.load',
                data: {
                    entity_name: 'infoboxes',
                    entity_filter: {art: 'info', datum_bis: '>='+getDateNow()}
                }
            }
        };

        xhr.send("event="+JSON.stringify(data.event));
    }

    function loadDataNote(){
        xhr2=new XMLHttpRequest();

        xhr2.onreadystatechange=function()
        {
            if (xhr2.readyState==4 && xhr2.status==200)
            {
                let response = xhr2.responseText;
                const obj = JSON.parse(response);
                createTable(['Box', 'Gültig ab', 'Gültig bis'], createArrayForTable(obj.data), 'table-hinweisbox');
            }
        };

        xhr2.open('POST', 'api', true);
        xhr2.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        data = {
            event: {
                name: 'entity.load',
                data: {
                    entity_name: 'infoboxes',
                    entity_filter: {art: 'hinweis', datum_bis: '>='+getDateNow()}
                }
            }
        };

        xhr2.send("event="+JSON.stringify(data.event));
    }


    function createArrayForTable(dataTable){
        let array = [];
        if(dataTable.length > 0) {
            for (let i = 0; i < dataTable.length; i++) {
                let cssClass;
                (dataTable[i].art === 'hinweis')? cssClass = 'infobox_user_note': cssClass = 'infobox_user_info';

                let icon;
                (dataTable[i].art === 'hinweis')? icon = 'fa-exclamation': icon = 'fa-info';

                let htmlInfobox = '<div class="'+cssClass+' box_table">\n' +
                    '<i class="fas '+icon+'"></i>\n' +
                    '<p>' + dataTable[i].text + '</p>\n' +
                    '<div class="close"><i class="fas fa-times"></i></div>\n' +
                    '</div>';

                let tempArray = [htmlInfobox, getDateFormattedDay(dataTable[i].datum_ab), getDateFormattedDay(dataTable[i].datum_bis)];
                array.push(tempArray);
            }
        }
        return array;
    }

    loadDataOld();

    function loadDataOld(){

        xhrOldData=new XMLHttpRequest();

        xhrOldData.onreadystatechange=function()
        {
            if (xhrOldData.readyState==4 && xhrOldData.status==200)
            {
                let response = xhrOldData.responseText;
                const obj = JSON.parse(response);
                createTable(['Box', 'Gültig ab', 'Gültig bis'], createArrayForTable(obj.data), 'table-data-old');
            }
        };

        xhrOldData.open('POST', 'api', true);
        xhrOldData.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        data = {
            event: {
                name: 'entity.load',
                data: {
                    entity_name: 'infoboxes',
                    entity_filter: {datum_bis: '<='+getDateNow()}
                }
            }
        };

        xhrOldData.send("event="+JSON.stringify(data.event));
    }

    document.getElementById('js-show-more').addEventListener('click', function () {
        showMore(this, 'fa-eye', 'fa-eye-slash');
    });

}