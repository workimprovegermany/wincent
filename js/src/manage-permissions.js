function init_permission(){
    em = new Entity_manager('permission');
    entity_filter = {id: '>0'};
    em.load_by_filter(entity_filter, process_permission_data);

    function process_permission_data(data) {
        let data_header = ['Name', 'Beschreibung', 'Löschen'],
            data_body = get_formatted_data_permission_for_table(data);

        createTable(data_header, data_body, 'js_table_container', 'responsive resp_as_accordion');
        init_event_listener_delete_permission();
    }

    function get_formatted_data_permission_for_table(data) {
        let new_array = [];
        for(let i = 0; i < data.length; i++){
            let tmp_array = [];
            tmp_array.push(data[i].name);
            tmp_array.push(data[i].description);
            tmp_array.push('<span class="js_delete_permission" data-nr="'+data[i].id+'"><i class="fas fa-trash-alt"></i></span>');

            new_array.push(tmp_array);
        }
        return new_array;
    }

    document.getElementById('js_add_permission').addEventListener('click', function (ev) {
        ev.preventDefault();
        show_overlay();
    });

    document.getElementById('form_add_permission').onsubmit = function (ev) {
        ev.preventDefault();
        em = new Entity_manager('permission');
        entity_fields = {name: document.getElementById('name').value.toUpperCase(), description: document.getElementById('beschreibung').value} ;
        em.save(entity_fields);
        hide_overlay();
        init_permission();
    };

    function init_event_listener_delete_permission(){
        let elemente = document.getElementsByClassName('js_delete_permission');
        for(let i = 0; i < elemente.length; i++){
            elemente[i].addEventListener('click', function (ev) {
                let id = this.getAttribute('data-nr'),
                    recht = this.parentElement.parentElement.children[0].innerText;
                delete_permission(id, recht);
            });
        }
    }

    function delete_permission(id, recht){
        if (confirm('Möchten Sie das Recht wirklich löschen? Recht: '+recht)) {
            em = new Entity_manager('permission');
            em.delete(id);
            init_permission();
        }
    }
}