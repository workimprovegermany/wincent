function ManageRole(){
    this.data_header = [];
    this.permissions = [];
}

var role = null;

function init_role(){
    role = new ManageRole();

    em = new Entity_manager('permission');
    entity_filter = {id: '>0'};
    em.load_by_filter(entity_filter, process_permission_data);

    function process_permission_data(data){
        role.data_header = ['Name', 'Beschreibung'];
        data.forEach(function (elem) {
            role.data_header.push(elem.name);
            role.permissions.push(elem);
        });
        role.data_header.push('Löschen');

        em = new Entity_manager('role');
        entity_filter = {id: '>0'};
        em.load_by_filter(entity_filter, process_role_data);
    }

    function process_role_data(data) {
        let data_body = get_formatted_data_role_for_table(data);

        createTable(role.data_header, data_body, 'js_table_container', 'responsive resp_as_accordion');
        init_event_listener_delete_role();
        add_event_listener_update_pemissions ();
    }

    function get_formatted_data_role_for_table(data) {

        let new_array = [];
        for(let i = 0; i < data.length; i++){
            let tmp_array = [];
            tmp_array.push(data[i].name);
            tmp_array.push(data[i].description);
            for(let x =0; x < role.permissions.length; x++){
                let checked = '';
                if(data[i].permission.indexOf(role.permissions[x].id) !== -1)
                    checked = 'checked';

                let checkbox = '<div class="relative inline-block"><input type="checkbox" class="js_update_permission role_'+data[i].id+'" name="permission-'+role.permissions[x].name+'" id="'+role.permissions[x].name+'-'+i+'" data-id="'+data[i].id+'" data-permission-id="'+role.permissions[x].id+'" '+checked+'><label for="'+role.permissions[x].name+'-'+i+'"></label></div>';

                tmp_array.push(checkbox);
            }
            tmp_array.push('<span class="js_delete_role" data-nr="'+data[i].id+'"><i class="fas fa-trash-alt"></i></span>');

            new_array.push(tmp_array);
        }
        return new_array;
    }

    document.getElementById('js_add_role').addEventListener('click', function (ev) {
        ev.preventDefault();
        show_overlay();
    });

    document.getElementById('form_add_role').onsubmit = function (ev) {
        ev.preventDefault();
        em = new Entity_manager('role');
        entity_fields = {name: document.getElementById('name').value, description: document.getElementById('beschreibung').value} ;
        em.save(entity_fields);
        init_role();
        hide_overlay();

    };

    function init_event_listener_delete_role(){
        let elemente = document.getElementsByClassName('js_delete_role');
        for(let i = 0; i < elemente.length; i++){
            elemente[i].addEventListener('click', function (ev) {
                let id = this.getAttribute('data-nr'),
                    rolle = this.parentElement.parentElement.children[0].innerText;
                delete_role(id, rolle);
            });
        }
    }

    function delete_role(id, rolle){
        if (confirm('Möchten Sie diese Rolle wirklich löschen? Rolle: '+rolle)) {
            em = new Entity_manager('role');
            em.delete(id);
            init_role();
        }
    }

    function add_event_listener_update_pemissions (){
        let elemente = document.getElementsByClassName('js_update_permission');
        for(let i = 0; i< elemente.length; i++){
            elemente[i].addEventListener('change', function (ev) {
                let role_id = this.getAttribute('data-id');
                let checkboxen = document.getElementsByClassName('role_'+role_id);
                let array_permissions = [];
                for(let x = 0; x < checkboxen.length; x++){
                    if(checkboxen[x].checked){
                        array_permissions.push(checkboxen[x].getAttribute('data-permission-id'));
                    }
                }

                em = new Entity_manager('role');
                entity_fields = {permission: array_permissions};
                em.update(role_id,entity_fields);
            })
        }
    }


}