function ManageUserRoles(){
    this.array_roles = [];
}
var userroles = null;

function init_user_role(){
    userroles = new ManageUserRoles();

    em = new Entity_manager('role');
    entity_filter = {id: '>0'};
    em.load_by_filter(entity_filter, process_role_data);
}

function process_role_data(data){

    data.forEach(function (elem) {
        userroles.array_roles.push(elem);
    });

    entity_fields = ['vorname', 'nachname', 'role'];
    em = new Entity_manager('user', entity_fields);
    entity_filter = {id: '>0'};
    em.load_by_filter(entity_filter, process_user_role_data);
}

function process_user_role_data(data){

    let columns = [
        [{
            field: 'vorname',
            title: 'Vorname',
            sortable: true,
            rowspan: 2,
            valign: 'middle'
        }, {
            field: 'nachname',
            title: 'Nachname',
            sortable: true,
            rowspan: 2,
            valign: 'middle'
        }, {
            title: 'Rollen',
            align: 'center',
            colspan: data.length
        }]
    ];

    let tmp_array = [];
    for(let i = 0; i < userroles.array_roles.length; i++){
        let temp_obj = {field: 'role-'+userroles.array_roles[i].id, title : userroles.array_roles[i].name, align: 'center'};
        tmp_array.push(temp_obj);
    }
    columns.push(tmp_array);

    data.forEach(function (elem) {

        let array_roles = elem.role;

        for(let i = 0; i < userroles.array_roles.length; i++){
            let checked = '';
            if(array_roles.indexOf(userroles.array_roles[i].id) !== -1){
                checked = 'checked';
            }

            elem['role-'+userroles.array_roles[i].id] = '<div class="relative inline-block"><input type="checkbox" class="js_update_user_role u_'+elem.id+'" name="role-'+userroles.array_roles[i].name+'" id="'+userroles.array_roles[i].name+'-'+elem.id+'" data-role-id="'+userroles.array_roles[i].id+'" data-id="'+elem.id+'" '+checked+'><label for="'+userroles.array_roles[i].name+'-'+elem.id+'"></label></div>';
        }
    });


    init_bootstrap_table(columns,data, '', add_event_listener_update_user_role );
    setTimeout(function () {
        window.parent.postMessage({"height": $(document).height()}, "*");
        add_event_listener_update_user_role ();
    }, 400);

    function add_event_listener_update_user_role (){
        let iframe = document.getElementById('iframe-table').contentWindow.document,
            elemente = iframe.getElementsByClassName('js_update_user_role');
        for(let i = 0; i< elemente.length; i++){
            elemente[i].addEventListener('change', function (ev) {

                let iframe = document.getElementById('iframe-table').contentWindow.document,
                    id = this.getAttribute('data-id'),
                    checkboxen = iframe.getElementsByClassName('u_'+id),
                    array_roles = [];
                for(let x = 0; x < checkboxen.length; x++){
                    if(checkboxen[x].checked){
                        array_roles.push(checkboxen[x].getAttribute('data-role-id'));
                    }
                }

                em = new Entity_manager('user');
                entity_fields = {role: array_roles};
                em.update(id,entity_fields);
            })
        }
    }
}