function init_form_pers_landing() {

    add_event_listener_toggle();
    get_short_id();
    add_event_listener_copy_url();

    document.getElementById('form_pers_landingpage').onsubmit = function(ev){
        ev.preventDefault();
        update_db_pers_landing(ev)
    };

    document.getElementById('form_pers_landingpage_kunde').onsubmit = function(ev){
        ev.preventDefault();
        update_db_pers_landing(ev)
    };

    function get_short_id(){
        entity_fields = ['short_id'];
        em = new Entity_manager('user');
        entity_filter = {id: app.user_id};
        em.load_by_filter(entity_filter, set_short_id_user);
    }

    function set_short_id_user(data) {
        let elemente = document.getElementsByClassName('js-short-id');
        for (let i = 0 ; i < elemente.length; i++){
            let link = elemente[i].parentElement;
            link.href = 'http://'+link.getElementsByClassName('js-url')[0].innerText+'/'+data[0].short_id;
            elemente[i].innerHTML = data[0].short_id;
        }
        load_data_form_pers_landing();
    }

    function update_db_pers_landing(ev){
        setButtonStateLoading();

        let div1 = getBoolBinaryForDB(document.forms[0].elements.bereich1.checked),
            div2 = getBoolBinaryForDB(document.forms[0].elements.bereich2.checked),
            div3 = getBoolBinaryForDB(document.forms[0].elements.bereich3.checked),
            div4 = getBoolBinaryForDB(document.forms[0].elements.bereich4.checked),
            div5 = getBoolBinaryForDB(document.forms[0].elements.bereich5.checked),
            div6 = getBoolBinaryForDB(document.forms[0].elements.bereich6.checked),
            div1_kunde = getBoolBinaryForDB(document.forms[1].elements.bereich1_kunde.checked),
            div2_kunde = getBoolBinaryForDB(document.forms[1].elements.bereich2_kunde.checked),
            id = document.forms[0].elements.id.value,
            video = '',
            radios = document.getElementsByName('video'),
            video_kunde = '',
            radios_kunde = document.getElementsByName('video_kunde');

        for (let i = 0; i < radios.length; i++)
        {
            if (radios[i].checked)
            {
                video = radios[i].value;
                break;
            }
        }
        for (let i = 0; i < radios_kunde.length; i++)
        {
            if (radios_kunde[i].checked)
            {
                video_kunde = radios_kunde[i].value;
                break;
            }
        }
        entity_fields = {div1: div1, div2: div2, div3: div3, div4: div4, div5: div5, div6: div6, user_id: app.user_id, video: video, div1_kunde: div1_kunde, div2_kunde: div2_kunde, video_kunde: video_kunde};
        em = new Entity_manager('personal_landingpage', entity_fields);

        if(hasClass(ev.target.getElementsByTagName('button')[0], 'js-update-data')){
            entity_id = id;
            em.update(entity_id,entity_fields, process_data_landing_page);
        } else {
            em.save(entity_fields, process_data_landing_page);
        }
    }


    function load_data_form_pers_landing(){
        em = new Entity_manager('personal_landingpage');
        entity_filter = {user_id: app.user_id};
        em.load_by_filter(entity_filter, process_data_form_pers_landing);
    }

    function process_data_form_pers_landing(data){
        if(data.length !== 0){
            document.forms[0].btn_submit.classList.add('js-update-data');
            document.forms[1].btn_submit_kunde.classList.add('js-update-data');
            checkUserData(data[0]);
        } else {
            setAllChecked();
        }
    }

    function process_data_landing_page(data) {
        //TODO Was passsiert bei error?
        //setButtonStateError();
        setButtonStateSuccess();
    }

    function checkUserData(data){
        let elemente = document.getElementsByClassName('js-toggle-area');
        document.forms[0].elements.id.value = data.id;
        document.forms[0].elements.bereich1.checked = getBoolVariable(data.div1);
        document.forms[0].elements.bereich2.checked = getBoolVariable(data.div2);
        document.forms[0].elements.bereich3.checked = getBoolVariable(data.div3);
        document.forms[0].elements.bereich4.checked = getBoolVariable(data.div4);
        document.forms[0].elements.bereich5.checked = getBoolVariable(data.div5);
        document.forms[0].elements.bereich6.checked = getBoolVariable(data.div6);
        document.forms[1].elements.bereich1_kunde.checked = getBoolVariable(data.div1_kunde);
        document.forms[1].elements.bereich2_kunde.checked = getBoolVariable(data.div2_kunde);


        if(data.video === '1'){
            document.forms[0].elements.video1.checked = true;
        } else {
            document.forms[0].elements.video2.checked = true;
        }

        switch (data.video_kunde){
            case '1':
                document.forms[1].elements.video1_kunde.checked = true;
                break;
            case '2':
                document.forms[1].elements.video2_kunde.checked = true;
                break;
            case '3':
                document.forms[1].elements.video3_kunde.checked = true;
                break;
            case '4':
                document.forms[1].elements.video4_kunde.checked = true;
                break;
        }

        for(let i = 0; i < elemente.length; i++){
            checkAktiveArea(elemente[i]);
        }
    }

    function setAllChecked(){
        let elemente = document.getElementsByClassName('js-toggle-area');
        for(let i = 0; i < elemente.length; i++){
            elemente[i].checked = true;
        }
    }

    function checkAktiveArea(checkboxElement){
        let num = checkboxElement.dataset.nr,
            elemente = Array.prototype.slice.call( document.getElementById('js-area-container').getElementsByTagName('div')),
            elemente_kunde = Array.prototype.slice.call(document.getElementById('js-area-container-kunde').getElementsByTagName('div'));

        elemente_kunde.forEach(function (element) {
            elemente.push(element);
        });

        for(let i = 0; i < elemente.length; i++){
            if(elemente[i].dataset.nr === num){
                if(checkboxElement.checked){
                    elemente[i].style.display = 'block';
                } else {
                    elemente[i].style.display = 'none';
                }
            }
        }
    }

    function add_event_listener_toggle(){
        let elemente = document.getElementsByClassName('js-toggle-area');
        for(let i = 0; i < elemente.length; i++){
            elemente[i].addEventListener('click', function (ev) {
                checkAktiveArea(this);
            })
        }
    }

    function add_event_listener_copy_url(){
        let elemente = document.getElementsByClassName('js_copy_url');
        for(let i = 0; i < elemente.length; i++){
            elemente[i].addEventListener('click', function (ev) {
                copyToClipboard(this.parentElement.getElementsByTagName('a')[0].innerText);
            })
        }
    }
}





