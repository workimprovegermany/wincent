function Provision(){
    this.array_provision = [];
    this.array_provision_all = [];
    this.array_users = [];
    this.array_provision_name = [];
    this.NOT_EDITABLE_PROVISION = '1';
    this.user_list_counter = 0;
}

var provision = null;

function init_provision_admin()
{
    app.check_page_access_restrictions(['DO_EVERYTHING'], window.location.origin);

    provision = new Provision();
    init_event_listener_refresh_data_table();

    entity_fields = ['uid', 'u_provision', 'u_provision_id', 'affiliate_uid'];
    em = new Entity_manager('user_provision_struktur', entity_fields);
    entity_filter = {aktuell: 1};
    em.load_by_filter(entity_filter, process_data_provison_admin);

    function process_data_provison_admin(data) {
        let array_ids = [];
        data.forEach(function (e) {
            if(e.u_provision_id === '1'){
                provision.array_provision.push(e);
            }
            provision.array_provision_all.push(e);
            array_ids.push(e.uid);
        });

        entity_fields = ['vorname', 'nachname', 'firma','short_id', 'skynet_id'];
        em = new Entity_manager('user', entity_fields);
        entity_filter = {id: array_ids};
        em.load_by_filter(entity_filter, process_data_provison_users);
    }


    function process_data_provison_users(data) {
        for(let i = 0; i < data.length; i++){
            provision.array_users.push(data[i]);
        }
        provision.array_provision.forEach(function (elem) {
            for(let i = 0; i < data.length; i++){
                if(data[i].id === elem.uid ){

                    elem['user'] = data[i].vorname+' '+data[i].nachname;

                    if(elem['user']=== ' ')
                        elem['user'] = data[i].firma;

                    elem['short_id'] = data[i].short_id;
                    elem['edit'] = '<span class="js-edit size30 icon_edit" data-user="'+data[i].id+'"></span>';
                    elem['u_provision'] = elem['u_provision'].replace('.',',');

                    break;

                }
            }
        });
        let columns = [{
            field: 'user',
            title: 'User',
            sortable: true
        }, {
            field: 'short_id',
            title: 'Short ID'
        }, {
            field: 'u_provision',
            title: 'Tippgeberprovision',
            sortable: true,
            formatter: add_thousandth_icon_formatter
        },{
            field: 'edit',
            title: 'Bearbeiten'
        }];

        function add_thousandth_icon_formatter(data) {
            return data + '‰';
        }

        init_bootstrap_table(columns,provision.array_provision, '', add_event_listener_edit_provision, provision_table_on_page_change );

        em = new Entity_manager('admin_provision_name');
        entity_filter = {id: '>0'};
        em.load_by_filter(entity_filter, process_data_provision_name);
    }

    function process_data_provision_name(data) {
        data.forEach(function (e) {
            provision.array_provision_name.push(e);
        });
    }

    function init_event_listener_refresh_data_table() {
        document.getElementById('js-close-overlay').removeEventListener('click', init_provision_admin);
        document.getElementById('js-close-overlay').addEventListener('click', init_provision_admin);
    }
}

function add_event_listener_edit_provision(){
    let iframe = document.getElementById('iframe-table').contentWindow.document,
        elemente = iframe.getElementsByClassName('js-edit');
    for(let i = 0; i < elemente.length; i++){
        elemente[i].addEventListener('click', function (ev) {
            let user_id = this.getAttribute('data-user');

            for(let i = 0; i < provision.array_provision.length; i++){
                if(provision.array_provision[i].uid === user_id){
                    document.forms[0].name.value = provision.array_provision[i].user;
                    document.forms[0].shorty.value = provision.array_provision[i].short_id;
                    document.forms[0].user_id.value = provision.array_provision[i].uid;
                    create_table_edit_provision(user_id);
                }
            }
            show_overlay();
            init_overlay_event_listener();
            add_event_listener_get_shorty();
            init_event_listener_add_provision();
            init_event_listener_update_and_delete();
        });
    }
}

function provision_table_on_page_change() {
    add_event_listener_edit_provision();
    $('iframe').css('height', $(document.getElementById('iframe-table').contentWindow.document.body).height() + 'px');
}

function create_table_edit_provision(user_id){
    let data_header = ['Name User', 'SHORTY', 'Art _Proision', 'Höhe-Provision', '', ''],
        data_body = get_data_for_table_edit_provision(user_id);
    createTable(data_header, data_body,'container_table_edit_provision', 'responsive resp_as_accordion');
}

function get_data_for_table_edit_provision(user_id){

    let data_to_user = provision.array_provision_all.filter(elem => elem.uid === user_id),
        data_array = [];

    for(let i = 0; i < data_to_user.length; i++){
        let element = data_to_user[i],
            user = provision.array_users.filter(elem => elem.id === element.affiliate_uid),
            html_delete = '<i data-id="'+element.id+'" class="fas fa-trash-alt js_delete"></i>',
            html_save = '<i data-id="'+element.id+'" class="far fa-save js_update"></i>',
            data_provision = provision.array_provision_name.filter(elem => elem.id === element.u_provision_id),
            html_provision_name = data_provision[0].name,
            html_user = user[0].vorname + ' '+user[0].nachname;

        if(element.u_provision_id !== provision.NOT_EDITABLE_PROVISION){
            html_provision_name = get_select_provision_name(element.u_provision_id);
            html_user = get_input_width_datalist_users(html_user);
        }

        let html_promille = '<div class="input_individuell grid_12"><input type="text" name="provision" id="provision" pattern="^([0-9]{1,2},[0-9]{0,6})$" value="'+element.u_provision.replace('.', ',')+'" required/><div data-content="‰"></div></div>';

        let tmp_array= [html_user, user[0].short_id, html_provision_name, html_promille, html_save, html_delete];
        data_array.push(tmp_array);
    }

    return data_array;
}

function get_select_provision_name(selected_element) {
    let select = document.createElement('select');
    select.name = 'provision_name';
    let data_provisionen_name = provision.array_provision_name.filter(elem => elem.id !== provision.NOT_EDITABLE_PROVISION);
    for (let x = 0; x < data_provisionen_name.length; x++){
        let option = document.createElement('option');
        option.text = data_provisionen_name[x].name;
        option.value = data_provisionen_name[x].id;

        if(data_provisionen_name[x].id === selected_element)
            option.setAttribute('selected', 'selected');

        select.add(option);
    }
    return select.outerHTML;
}

function get_input_width_datalist_users(value) {
    let input = '<input type="text" class="js_get_shorty" name="user_'+provision.user_list_counter+'" value="'+value+'" required list="list_users_'+provision.user_list_counter+'" ><datalist id="list_users_'+provision.user_list_counter+'">';

    provision.array_users.forEach(function (elem) {
        if(isNotEmpty(elem.nachname)){
            input += '<option value="'+elem.vorname + ' '+elem.nachname+' | '+elem.short_id+'" />';
        } else {
            input += '<option value="'+elem.firma+' | '+elem.short_id+'" />';
        }

    });

    input += '</datalist></input>';
    provision.user_list_counter++;
    return input;

}

function init_event_listener_add_provision() {
    document.getElementById('add_provision').addEventListener('click', function (ev) {
        ev.preventDefault();
        add_table_row_provision(ev);

    })
}

function add_table_row_provision(ev){

    let tbody = document.getElementById('overlay').getElementsByTagName('table')[0].getElementsByTagName('tbody')[0],
        new_row = '<tr>';
    new_row += '<td>'+get_input_width_datalist_users('')+'</td>';
    new_row += '<td> </td>';
    new_row += '<td>'+get_select_provision_name()+'</td>';
    new_row += '<td> <div class="input_individuell grid_12"><input type="text" name="provision_neu" pattern="^([0-9]{1,2},[0-9]{0,6})$" value="" required/><div data-content="‰"></div></div></td>';
    new_row += '<td> <i class="far fa-save processed js_save"></i></td>';
    new_row += '<td> <i class="fas fa-trash-alt js_remove"></i></td>';
    new_row += '</tr>';
    tbody.innerHTML += new_row;
    add_event_listener_get_shorty();
    init_event_listener_save_and_remove();
}

function add_event_listener_get_shorty() {
    let elemente = document.getElementsByClassName('js_get_shorty');
    for(let i = 0; i < elemente.length; i++){
        elemente[i].addEventListener('change', function (ev) {

            let user = '',
                shorty = '';
            if(isNotEmpty(this.value)){
                user = this.value.split('|')[0];
                shorty = this.value.split('|')[1];
            }

            this.value = user;
            this.setAttribute('value', user);
            this.parentNode.nextElementSibling.innerText = shorty;
        })
    }
}

function init_event_listener_save_and_remove(){
    let elemente = document.getElementsByClassName('js_save');
    add_event_listener_click(elemente, save_new_provision);

    elemente = document.getElementsByClassName('js_remove');
    add_event_listener_click(elemente, remove_new_provision);

}

function init_event_listener_update_and_delete(){
    let elemente = document.getElementsByClassName('js_update');
    add_event_listener_click(elemente, update_provision);

    elemente = document.getElementsByClassName('js_delete');
    add_event_listener_click(elemente, delete_provision);

}

function save_new_provision(){
    let cells = this.parentElement.parentElement.children,
        shorty = cells[1].innerText,
        provision_hoehe = cells[3].getElementsByTagName('input')[0].value,
        pattern = '^([0-9]{1,2},[0-9]{0,6})$';

    if(isNotEmpty(shorty) && isNotEmpty(provision_hoehe) && is_right_pattern(provision_hoehe, pattern)){
        let user = provision.array_users.filter(elem => elem.short_id === shorty),
            provision_art = cells[2].getElementsByTagName('select')[0].value,
            user_id = document.getElementById('user_id').value,
            affiliate_user_id = user[0].id;

        provision_hoehe = provision_hoehe.replace(',','.');

        em = new Entity_manager('user_provision_struktur');
        entity_fields = {affiliate_uid: affiliate_user_id, uid: user_id, u_provision: provision_hoehe, u_provision_id:provision_art, aktuell: 1};
        em.save(entity_fields);

        this.classList.remove('processed');
        this.classList.remove('js_save');
        cells[5].children[0].classList.remove('js_remove');

        this.removeEventListener('click', save_new_provision);
        cells[5].children[0].removeEventListener('click', remove_new_provision);
    }

}

function remove_new_provision(){
    this.parentElement.parentElement.remove();
}

function update_provision(){
    let cells = this.parentElement.parentElement.children,
        shorty = cells[1].innerText,
        provision_hoehe = cells[3].getElementsByTagName('input')[0].value,
        pattern = '^([0-9]{1,2},[0-9]{0,6})$';

    if(isNotEmpty(shorty) && isNotEmpty(provision_hoehe) && is_right_pattern(provision_hoehe, pattern)){
        let user = provision.array_users.filter(elem => elem.short_id === shorty),
            user_id = document.getElementById('user_id').value,
            affiliate_user_id = user[0].id,
            id = this.getAttribute('data-id');

        let select = cells[2].getElementsByTagName('select');
        let provision_art = provision.NOT_EDITABLE_PROVISION;
        if(select.length > 0){
            provision_art = cells[2].getElementsByTagName('select')[0].value;
        }


        provision_hoehe = provision_hoehe.replace(',','.');

        em = new Entity_manager('user_provision_struktur');
        entity_fields = {affiliate_uid: affiliate_user_id, u_provision: provision_hoehe, u_provision_id:provision_art, aktuell: 1};
        em.update(id, entity_fields);

        this.classList.remove('processed');
    } else {
        set_inputs_error(this)
    }

}

function delete_provision(){
    if (confirm('Möchten Sie diese Provision sicher löschen?')) {
        let id = this.getAttribute('data-id');
        em = new Entity_manager('user_provision_struktur');
        entity_fields = {aktuell: 0};
        em.update(id, entity_fields);
        this.parentElement.parentElement.remove();
    }

}

function is_right_pattern(value, pattern){
    return new RegExp(pattern).test(value);
}

function init_overlay_event_listener(){
    let elemente = document.getElementById('overlay').getElementsByTagName('input');
    for(let i = 0; i < elemente.length; i++){
        elemente[i].addEventListener('keyup', function (ev) {
            let element = this.parentNode.parentElement.getElementsByClassName('js_update');
            if(element.length === 0)
                element = this.parentNode.parentElement.parentElement.getElementsByClassName('js_update');

            element[0].classList.add('processed');
        });
    }

    let elemente_select = document.getElementById('overlay').getElementsByTagName('select');
    for(let i = 0; i < elemente_select.length; i++){
        elemente_select[i].addEventListener('change', function (ev) {
            let element = this.parentNode.parentElement.getElementsByClassName('js_update')[0].classList.add('processed');
        });
    }
}


function set_inputs_error(button){
    let inputs =button.parentNode.parentNode.getElementsByTagName('input');
    for(let i = 0; i < inputs.length; i++){
        if(!inputs[i].validity.valid){
            inputs[i].classList.add('error');
            inputs[i].addEventListener('keyup', function () {
                this.classList.remove('error');
            })
        }
    }
}