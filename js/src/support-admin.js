function Users(){
    this.arrayData = [];
    this.arrayUserIds = [];
    this.numberOfTabs = 0;
    this.chat_type = null;
}
var usersChat = null;

function initSupportAdmin()
{
    //app.check_page_access_restrictions(['DO_SUPPORT_SALES', 'DO_EVERYTHING'], window.location.origin);
    app.check_page_access_restrictions(['DO_EVERYTHING'], window.location.origin);

    usersChat = new Users();

    usersChat.chat_type = new URL(window.location.href).searchParams.get("type");

    set_interval_check_online_status();

    document.getElementById('form-support-message').onsubmit = function(ev){
        ev.preventDefault();
        let chat_user_id = getUserIdActiveChat(),
            valid = this.checkValidity();

        if(valid && chat_user_id !== undefined) {
            setButtonStateLoading();

            let nachricht = this.support_nachricht.value,
                datum = getDateNow();

            //DATEN SENDEN
            $.ajax({
                url: 'api',
                type: 'POST',
                data: {
                    event: {
                        name: 'entity.save',
                        data: {
                            entity_name: 'support_message',
                            entity_fields: {
                                nachricht: nachricht,
                                datum: datum,
                                absender: usersChat.chat_type,
                                empfaenger: chat_user_id,
                                gelesen: 0
                            }
                        }
                    },
                },

                success:function(response){
                    setButtonStateSuccess();
                    document.getElementById('form-support-message').reset();
                    support.array = [];
                    support.activeUser = '';
                    setUserMessageAnswered(chat_user_id);
                    hideChatWindowRD();
                },
                error: function (response) {
                    setButtonStateError();
                }
            });
        }
    };
    let elementBack = document.getElementById('js-back-to-tabs');

    if(!!elementBack){
        elementBack.addEventListener('click', function (ev) {
            setChatListelementInactive();
            resetSupportWindow();
        });
    }

    load_data_support_admin();

}

function load_data_support_admin() {
    entity_fields = ['absender'];
    em = new Entity_manager('support_message', entity_fields);
    entity_filter = {empfaenger: usersChat.chat_type, beantwortet: 0};
    em.load_by_filter(entity_filter, process_data_support_admin);
}

function process_data_support_admin(data){
    let tmpArray = data;
    if(usersChat.numberOfTabs !== tmpArray.length){
        usersChat.numberOfTabs = tmpArray.length;
        createChatTabs(tmpArray);
        for(let i = 0; i <tmpArray.length; i++){
            if(tmpArray[i].absender === support.activeUser){
                getUserChat(null, support.activeUser);
                break;
            }
        }
    } else {
        check_user_online_status();
    }

}


function getUserChat(ev, id){
    if(!isNotEmpty(id)){
        let users = usersChat.arrayData,
            hash= this.getAttribute('id');

        this.classList.remove('hat-nachricht');

        for(let i = 0; i < users.length; i++){
            let user = users[i];
            if(user.hash === hash){
                support.activeUser = user.id;
                break;
            }
        }

        setChatListelementActive(this);
    }



    if(support.activeUser){


        xhrChatData=new XMLHttpRequest();

        xhrChatData.onreadystatechange=function()
        {
            if (xhrChatData.readyState===4 && xhrChatData.status===200)
            {
                let response = xhrChatData.responseText;
                const obj = JSON.parse(response);
                let array = sortArrayByDate(obj.data);
                setMessageListSupportMsg(array);

            }
        };

        xhrChatData.open('POST', 'api', true);
        xhrChatData.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        data = {
            event: {
                name: 'entity.load',
                data: {
                    entity_name: 'support_message',
                    entity_filter: {_or:{empfaenger: support.activeUser, absender: support.activeUser}}
                }
            }
        };

        xhrChatData.send("event="+JSON.stringify(data.event));
    }
}

function setUserMessageAnswered(user_id) {

    let xhrUserData=new XMLHttpRequest();

    xhrUserData.onreadystatechange=function()
    {
        if (xhrUserData.readyState===4 && xhrUserData.status===200)
        {
            let response = xhrUserData.responseText;
            const obj = JSON.parse(response);
            updateUserMessageAnswered(obj.data);
        }
    };

    xhrUserData.open('POST', 'api', true);
    xhrUserData.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'entity.load',
            data: {
                entity_name: 'support_message',
                entity_filter: {absender: user_id , beantwortet: 0},
                entity_fields: ['id']
            }
        }
    };

    xhrUserData.send("event="+JSON.stringify(data.event));
}

function updateUserMessageAnswered(ids) {

    for(let i = 0; i < ids.length; i++){

        xhrUpdate=new XMLHttpRequest();

        if(i === (ids.length - 1)){
            xhrUpdate.onreadystatechange=function()
            {
                if (xhrUpdate.readyState===4 && xhrUpdate.status===200)
                {
                    load_data_support_admin();
                    resetSupportWindow();
                }
            };
        }

        xhrUpdate.open('POST', 'api', true);
        xhrUpdate.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        data = {
            event: {
                name: 'entity.update',
                data: {
                    entity_name: 'support_message',
                    entity_id: ids[i].id,
                    entity_fields: {
                        beantwortet: true
                    }
                }
            }
        };

        xhrUpdate.send("event="+JSON.stringify(data.event));
    }
}

function resetSupportWindow()
{
    document.getElementById('support_chat_fenster').innerHTML = '';
}

/**********SUPPORT ADMIN MENÜ***************/
function init_support_admin_menu(){
    //app.check_page_access_restrictions(['DO_SUPPORT_SALES', 'DO_EVERYTHING'], window.location.origin);
    app.check_page_access_restrictions(['DO_EVERYTHING'], window.location.origin);

    if(app.user_can('DO_EVERYTHING'))
        document.getElementsByClassName("js-support-tab")[0].parentNode.parentNode.style.display = 'block';

    entity_fields = ['empfaenger'];
    em = new Entity_manager('support_message', entity_fields);
    entity_filter = {empfaenger: ['support', 'vertrieb'], beantwortet: 0};
    em.load_by_filter(entity_filter, process_support_admin_menu);
}

function process_support_admin_menu(data){
    let array_support = data.filter(function (element) {
        return element.empfaenger === 'support';
    });
    let sum_support = array_support.length;
    if(sum_support > 0) {
        let element = document.getElementsByClassName('js-num-support')[0].parentElement.getElementsByTagName('span')[0],
            value = element.innerText;
        element.innerText = value+'  ('+sum_support+')';
    }

    let sum_sales = data.length - sum_support;
    if(sum_sales > 0) {
        let element = document.getElementsByClassName('js-num-support')[0].parentElement.getElementsByTagName('span')[0],
            value = element.innerText;
        element.innerText = value+'  ('+sum_sales+')';
    }

}

/**********ONLINE STATUS***************/
function check_user_online_status(){
    xhr_user_online_status=new XMLHttpRequest();

    xhr_user_online_status.onreadystatechange=function()
    {
        if (xhr_user_online_status.readyState===4 && xhr_user_online_status.status===200)
        {
            let response = xhr_user_online_status.responseText;
            const obj = JSON.parse(response);
            set_user_online_status_tabs(obj.data);
        }
    };

    xhr_user_online_status.open('POST', 'api', true);
    xhr_user_online_status.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'entity.load',
            data: {
                entity_name: 'user',
                entity_filter: {id: usersChat.arrayUserIds},
                entity_fields: ['online_status']
            }
        }
    };

    xhr_user_online_status.send("event="+JSON.stringify(data.event));

    function set_user_online_status_tabs(data){
        for(let i = 0; i < data.length; i++){
            var id = data[i].id;
            let element = usersChat.arrayData.filter(function (elem) {
                return elem.id === id;
            });

            if(data[i].online_status === '1'){
                document.getElementById(element[0].hash).classList.add('online');
            } else {
                document.getElementById(element[0].hash).classList.remove('online');
            }
        }
    }

}

function set_interval_check_online_status(){
    setInterval(function(){ check_user_online_status(); }, 10000);
}