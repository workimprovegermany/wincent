function Support(){
    this.array = [];
    this.arrayNotReadMsg = [];
    this.timeout = '';
    this.activeUser = '';
}

var support = new Support();

function initSupportUser(){
    if(app.user_can('DO_EVERYTHING')){
        let url = new URL(window.location.href),
            token = url.searchParams.get("token");
        window.location.replace(window.location.origin+'/intern/support-admin-menu.html?token='+token);
    }

    //support = new Support();

    document.getElementById('form-support-message').onsubmit = function(ev){
        let valid = this.checkValidity();
        ev.preventDefault();
        if(valid) {
            setButtonStateLoading();

            let nachricht = this.support_nachricht.value,
                datum = getDateNow(),
                empfaenger = '';

            if (document.getElementById('rd-support').checked) {
                empfaenger = document.getElementById('rd-support').value;
            } else {
                empfaenger = document.getElementById('rd-vertrieb').value;
            }
            const user_id = app.user_id;

            //DATEN SENDEN
            $.ajax({
                url: 'api',
                type: 'POST',
                data: {
                    event: {
                        name: 'entity.save',
                        data: {
                            entity_name: 'support_message',
                            entity_fields: {
                                nachricht: nachricht,
                                datum: datum,
                                absender: user_id,
                                empfaenger: empfaenger,
                                gelesen: 0
                            }
                        }
                    },
                },

                success:function(response){
                    setButtonStateSuccess();
                    document.getElementById('form-support-message').reset();
                    loadDataSupport();
                },
                error: function (response) {
                    setButtonStateError();
                }
            });
        }
    };

    loadDataSupport();
}

function loadDataSupport_response(data)
{
    addArrayDataSupport(data);
    loadDataSupportAnswers();
    initArrayNotReadMsg(data);
}

function loadDataSupport() {
    const user_id = app.user_id;
    support.array = [];

    em = new Entity_manager('support_message');
    entity_filter = {absender: user_id};
    em.load_by_filter(entity_filter, loadDataSupport_response);


}

function loadDataSupportAnswers() {

    const user_id = app.user_id;
    xhr=new XMLHttpRequest();

    xhr.onreadystatechange=function()
    {
        if (xhr.readyState===4 && xhr.status===200)
        {
            let response = xhr.responseText;
            const obj = JSON.parse(response);
            addArrayDataSupport(obj.data);
            setMessageListSupportMsg(support.array);

        }
    };

    xhr.open('POST', 'api', true);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'entity.load',
            data: {
                entity_name: 'support_message',
                entity_filter: {empfaenger: user_id}
            }
        }
    };

    xhr.send("event="+JSON.stringify(data.event));
    return xhr;
}

function setMessageListSupportMsg(data){
    let htmlListElement = '<div class="support_{{art}} {{status_gelesen}}" msg-id="{{msg-id}}">\n' +
        '                        <div>\n' +
        '                            <span>{{datum}}</span>\n' +
        '                            <p>{{nachricht}}</p>\n' +
        '                        </div>\n' +
        '                    </div>';

    let art = "",
        htmlListe = "",
        htmlListElementReplaced = "";

    for(let i = 0; i < data.length; i++){
        let status = '';

        if(data[i].absender === 'support'){
            art = "antwort";
            if(data[i].gelesen === '1'){
                status = 'antwort_gelesen';
            }
        } else if(data[i].absender === 'vertrieb'){
            art = "antwort_vertrieb";
            if(data[i].gelesen === '1'){
                status = 'antwort_gelesen';
            }
        } else {
            art = "anfrage";
            if(data[i].gelesen === '1'){
                status = 'gelesen';
            }
        }

        htmlListElementReplaced = htmlListElement.replace('{{status_gelesen}}', status).replace('{{msg-id}}', data[i].id).replace('{{art}}', art).replace('{{datum}}', getDateFormatted(data[i].datum)).replace('{{nachricht}}', data[i].nachricht);
        htmlListe = htmlListe + htmlListElementReplaced;
    }

    addClassToElements(document.getElementById('support_chat_fenster'), 'hidden');
    document.getElementById('support_chat_fenster').innerHTML = htmlListe;
    let objDiv = document.getElementById("support_chat_fenster");
    objDiv.scrollTop = objDiv.scrollHeight;
    updateMessagesReadSupport();
    if(app.user_can('DO_EVERYTHING')){
        loadUserImgForSupport(support.activeUser);
    } else {
        loadUserImgForSupport(app.user_id);
    }

}

function addArrayDataSupport(data){
    let array = support.array;

    for(let i = 0; i < data.length; i++){
        array.push(data[i])
    }
    array = sortArrayByDate(array);
    //console.log('Array: '+array);
}

function loadUserImgForSupport(user_id) {

    xhrUser=new XMLHttpRequest();

    xhrUser.onreadystatechange=function()
    {
        if (xhrUser.readyState===4 && xhrUser.status===200)
        {
            let response = xhrUser.responseText;
            const obj = JSON.parse(response);
            setUserImgSupportMsg(obj.data[0]);
        }
    };

    xhrUser.open('POST', 'api', true);
    xhrUser.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'entity.load',
            data: {
                entity_name: 'user',
                entity_filter: {id: user_id}
            }
        }
    };

    xhrUser.send("event="+JSON.stringify(data.event));
}

function setUserImgSupportMsg(data){
    let img_name = data.bild_name,
        img_pfad = data.bild_pfad;

    if(isNotEmpty(img_name)){
        let img_src = img_pfad+'/'+img_name;
        addClass('userbild', document.getElementsByClassName('support_anfrage'));
        $('head').append('<style>.userbild::after{background-image:url('+img_src+')}</style>');
    }
    removeClassToElements(document.getElementById('support_chat_fenster'), 'hidden');
}

function updateMessagesReadSupport(){
    let elemente;
    (app.user_can('DO_EVERYTHING'))? elemente = $('[class*="support_anfrage"]:not(.gelesen)') : elemente = $('[class*="support_antwort"]:not(.antwort_gelesen)');

    for(let i = 0; i < elemente.length; i++){
        let id = elemente[i].getAttribute('msg-id');
        xhrUpdate=new XMLHttpRequest();

        if(i === (elemente.length - 1)){
            xhrUpdate.onreadystatechange=function()
            {
                if (xhrUpdate.readyState===4 && xhrUpdate.status===200)
                {
                    checkNewMessageUser(app.user_id);
                }
            };
        }

        xhrUpdate.open('POST', 'api', true);
        xhrUpdate.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

        data = {
            event: {
                name: 'entity.update',
                data: {
                    entity_name: 'support_message',
                    entity_id: id,
                    entity_fields: {
                        gelesen: true
                    }
                }
            }
        };

        xhrUpdate.send("event="+JSON.stringify(data.event));
    }
}

function initArrayNotReadMsg(arrayData) {
    support.arrayNotReadMsg = [];
    for(let i = 0; i<arrayData.length; i++){
        if(arrayData[i].gelesen === '0'){
            support.arrayNotReadMsg.push(arrayData[i].id);
        }
    }
    checkNewMsgRead();
}

function checkNewMsgRead(){
    xhrCheckNewMsg=new XMLHttpRequest();

    xhrCheckNewMsg.onreadystatechange=function()
    {
        if (xhrCheckNewMsg.readyState===4 && xhrCheckNewMsg.status===200)
        {
            let response = xhrCheckNewMsg.responseText;
            const obj = JSON.parse(response);
            let tmpArray = obj.data,
                numberNotReadMsg = 0;
            tmpArray.forEach(function (element,x) {
                if(element.gelesen === '0'){
                    numberNotReadMsg++;
                }
            });

            if(numberNotReadMsg === 0 && support.arrayNotReadMsg.length > 0){
                setMsgReadIconCheck();
            } else {
                clearTimeout(support.timeout);
                support.timeout = setTimeout(function(){checkNewMsgRead();},10000);
            }

            get_admin_online_status();
        }
    };

    xhrCheckNewMsg.open('POST', 'api', true);
    xhrCheckNewMsg.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'entity.load',
            data: {
                entity_name: 'support_message',
                entity_filter: {absender: app.user_id},
                entity_fields: ['gelesen', 'id']
            }
        }
    };

    xhrCheckNewMsg.send("event="+JSON.stringify(data.event));
}

function setMsgReadIconCheck(){
    let elemente = document.getElementsByClassName('support_anfrage');

    for(let i=0; i< elemente.length;i++) {
        elemente[i].classList.add('gelesen');
    }
}

function get_admin_online_status(){
    let ADMIN = 1;

    xhrOnlineStatus=new XMLHttpRequest();

    xhrOnlineStatus.onreadystatechange=function()
    {
        if (xhrOnlineStatus.readyState==4 && xhrOnlineStatus.status==200)
        {
            let response = xhrOnlineStatus.responseText;
            const obj = JSON.parse(response);
            set_online_status_support_user(obj.online_status);
        }
    };

    xhrOnlineStatus.open('POST', 'api', true);
    xhrOnlineStatus.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'user.online_status',
            data: {
                role: ADMIN,
            }
        }
    };

    xhrOnlineStatus.send("event="+JSON.stringify(data.event));
}

function set_online_status_support_user(data){
    if(data === 'online'){
        document.getElementById('rd-support').classList.add('online');
        document.getElementById('rd-vertrieb').classList.add('online');
    } else {
        document.getElementById('rd-support').classList.remove('online');
        get_sales_online_status();
    }
}

function get_sales_online_status(){
    let SALES = 3;

    xhrOnlineStatus=new XMLHttpRequest();

    xhrOnlineStatus.onreadystatechange=function()
    {
        if (xhrOnlineStatus.readyState==4 && xhrOnlineStatus.status==200)
        {
            let response = xhrOnlineStatus.responseText;
            const obj = JSON.parse(response);
            set_online_status_sales(obj.online_status);
        }
    };

    xhrOnlineStatus.open('POST', 'api', true);
    xhrOnlineStatus.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");

    data = {
        event: {
            name: 'user.online_status',
            data: {
                role: SALES,
            }
        }
    };

    xhrOnlineStatus.send("event="+JSON.stringify(data.event));
}

function set_online_status_sales(data){
    if(data === 'online'){
        document.getElementById('rd-vertrieb').classList.add('online');
    } else {
        document.getElementById('rd-vertrieb').classList.remove('online');
    }
}
