function SkynetData(){
    this.startYear = 2018;
    this.thisYear = new Date().getFullYear();
    this.thisMonth = new Date().getMonth()+1;
    this.investors = '';
    this.sumInvestmentsActive = ''; //running & pending
    this.sumInvestmentsRunning = '';
    this.sumInvestmentsCanceled = '';
    this.sumInvestmentsPending = '';
    this.array_sum_by_month = {};
    this.array_sum_by_year = [];
    this.function_queue_number = 0;
    this.xhr = new XHR();
}

var skynet;

function updateSkynetAdminTable() {
    skynet = new SkynetData();
    xhr_admin = new XHR();
    get_number_of_investors();
}

function get_number_of_investors(){
    entity_filter = {investments_count: '>0'};
    entity_fields = ['investments_count'];
    em = new Entity_manager('skynet_projects', entity_fields, skynet.xhr);
    em.load_by_filter(entity_filter, get_data_investors);
}

function get_data_investors(data){
    let sum = 0;
    data.forEach(function (element) {
        sum = sum + parseInt(element.investments_count);
    });
    skynet.investors = sum;
    //document.write('Sum Investoren: '+skynet.investors+'<br>');

    em = new Entity_manager('skynet_admin', null, skynet.xhr);
    entity_filter = {state: 'investors'};
    em.load_by_filter(entity_filter, process_data_number_investors);

}

function process_data_number_investors(data) {
    em = new Entity_manager('skynet_admin', null, skynet.xhr);
    entity_fields = {state: 'investors', sum: skynet.investors} ;
    if(data.length > 0){
        em.update(data[0].id, entity_fields);
    } else {
        em.save(entity_fields);
    }
    obj_listener.register('prop',Prop);
    get_number_of_all_investments_from_state('running', process_data_of_all_investments_running);
}


var Prop = {
    notify: function(val) {
        switch (val){
            case 1:
                get_number_of_all_investments_from_state('pending', process_data_of_all_investments_pending);
                break;

            case 2:
                get_number_of_all_investments_from_state('canceled', process_data_of_all_investments_canceled);
                break;

            case 3:
                get_number_of_all_investments_from_state('running', process_data_by_month);
                break;

            case 4:
                get_number_of_all_investments_from_state('canceled', process_data_by_month);
                break;

            case 5:
                get_number_of_all_investments_from_state('pending', process_data_by_month_pending);
                break;


        }
    }
};

function process_data_of_all_investments_running(data){
    let sum = 0;
    data.forEach(function (element) {
        sum = sum + parseInt(element.amount);
    });

    skynet.sumInvestmentsRunning = sum;
    //document.write('Sum Investments running: '+skynet.sumInvestmentsRunning+'<br>');
    get_data_from_beginning('running');
}

function process_data_of_all_investments_pending(data){
    let sum = 0;
    data.forEach(function (element) {
        sum = sum + parseInt(element.amount);
    });

    skynet.sumInvestmentsPending = sum;
    //document.write('Sum Investments pending: '+skynet.sumInvestmentsPending+'<br>');
    skynet.sumInvestmentsActive = sum + parseInt(skynet.sumInvestmentsRunning);
    //document.write('Sum Investments running & pending: '+skynet.sumInvestmentsActive+'<br>');
    get_data_from_beginning('pending');
}

function process_data_of_all_investments_canceled(data){
    let sum = 0;
    data.forEach(function (element) {
        sum = sum + parseInt(element.amount);
    });

    skynet.sumInvestmentsCanceled = sum;
    //document.write('Sum Investments canceled: '+skynet.sumInvestmentsCanceled+'<br>');
    get_data_from_beginning('canceled');
}


function get_number_of_all_investments_from_state(state, ready_function){
    entity_fields = ['amount', 'payin_settled_at', 'state', 'checkout_finished_at'];
    em = new Entity_manager('skynet_investments', entity_fields, skynet.xhr);
    entity_filter = {state: state};
    em.load_by_filter(entity_filter, ready_function);
}

function get_data_from_beginning(state) {
    em = new Entity_manager('skynet_admin', null, skynet.xhr);
    entity_filter = {state: state, year: 0, month:0};
    em.load_by_filter(entity_filter, process_data_from_beginning,state);
}

function process_data_from_beginning(data, val){
    em = new Entity_manager('skynet_admin', null, skynet.xhr);
    let sum = 0,
        nextState = '';
    switch (val){
        case 'running':
            sum = skynet.sumInvestmentsRunning;
            break;

        case 'pending':
            sum = skynet.sumInvestmentsPending;
            break;

        case 'canceled':
            sum = skynet.sumInvestmentsCanceled;
            break;

        case 'investors':
            sum = skynet.investors;
            break;
    }

    if(data.length > 0){
        entity_fields = {sum: sum};
        em.update(data[0].id, entity_fields);

    } else {
        entity_fields = {state: val, sum: sum, year: 0, month:0};
        em.save(entity_fields);
    }
    skynet.function_queue_number++;
    obj_listener.setProp(skynet.function_queue_number);
}

function process_data_by_month(data){
    let sum = 0;
    data.forEach(function (element) {
        sum = sum + parseInt(element.amount);
    });

    let arrayData = new Array(data)[0],
        state = arrayData[0].state;

    //document.write('<br>---------- START Investments pro Monat '+state+'------------<br><br>');
    for(let y = skynet.startYear; y <= skynet.thisYear; y++){
        let tmpSumYear = 0;
        for (let m = 1; m <= 12; m++) {
            if(y === skynet.thisYear && m === skynet.thisMonth+1)
                break;

            (m < 10) ? _date = y + '-0' + m : _date = y + '-' + m;
            let tmpSum = 0;
            let filtered = arrayData.filter(function (item) {
                let tmp_bool = false;
                if(!!item.payin_settled_at){
                    tmp_bool = item.payin_settled_at.substr(0, 7) === _date;
                } else {
                    tmp_bool = item.checkout_finished_at.substr(0, 7) === _date;
                }

                return tmp_bool;
            });
            filtered.forEach(function (element) {
                tmpSum += parseInt(element.amount);
            });
            skynet.array_sum_by_month['date'+y+m] =  tmpSum;
            tmpSumYear += tmpSum;
            //document.write('Monat: '+_date+'   Summe: '+tmpSum+'<br>');
        }
        skynet.array_sum_by_year.push({[y]: tmpSumYear});
        //document.write('Jahressumme '+y+': '+tmpSumYear+'<br><br>');
    }
    //document.write('---------- ENDE '+state+'------------<br><br>');

    load_data_by_month(state);
}

function load_data_by_month(state) {
    let array_year = [];
    for(let y = skynet.startYear; y <= skynet.thisYear; y++){
        array_year.push(y);
    }
    entity_filter = {year: array_year, month: '>0', state: state};
    em = new Entity_manager('skynet_admin', null, skynet.xhr);
    em.load_by_filter(entity_filter, update_all_data_by_month, state);
}

function update_all_data_by_month(data, state){
    if(state === 'pending')
        return update_all_data_by_month_pending(data, state);

    if(data.length === 0){
        for(let y = skynet.startYear; y <= skynet.thisYear; y++)for(let m = 1; m <= 12; m++){
            if(y === skynet.thisYear && m === skynet.thisMonth+1)
                break;

            let key = 'date'+y+m,
                val = skynet.array_sum_by_month[key],
                full_date = y+'-'+add_leading_null(m);

            entity_fields = {state: state, sum: val, year: y, month: m, year_and_month:full_date} ;
            em.save(entity_fields);
        }
    } else {
        for(let y = skynet.startYear; y <= skynet.thisYear; y++)for(let m = 1; m <= 12; m++){
            if(y === skynet.thisYear && m === skynet.thisMonth+1)
                break;
            let key_array = 'date'+y+m,
                val = skynet.array_sum_by_month[key_array],
                full_date = y+'-'+add_leading_null(m),
                bool = false,
                id = null;

            for(let i = 0; i < data.length; i++){
                if(data[i].year_and_month === full_date){
                    bool = true;
                    id = data[i].id;
                }
            }
            if(bool){
                entity_fields = {sum: val} ;
                em.update(id, entity_fields);
            } else {
                entity_fields = {state: state, sum: val, year: y, month: m, year_and_month:full_date} ;
                em.save(entity_fields);
            }


        }
    }
    skynet.function_queue_number++;
    obj_listener.setProp(skynet.function_queue_number);

}

function process_data_by_month_pending(data){
    let sum = 0;
    data.forEach(function (element) {
        sum = sum + parseInt(element.amount);
    });

    let arrayData = new Array(data)[0],
        state = arrayData[0].state;
    skynet.array_sum_by_month = {};
    skynet.array_sum_by_year = [];

    //document.write('<br>---------- START Investments pro Monat '+state+'------------<br><br>');
    for(let y = skynet.startYear; y <= skynet.thisYear; y++){
        let tmpSumYear = 0;
        for (let m = 1; m <= 12; m++) {
            if(y === skynet.thisYear && m === skynet.thisMonth+1)
                break;
            (m < 10) ? _date = y + '-0' + m : _date = y + '-' + m;
            let tmpSum = 0;
            let filtered = arrayData.filter(function (item) {
                let tmp_bool = false;
                if(!!item.payin_settled_at){
                    tmp_bool = item.payin_settled_at.substr(0, 7) === _date;
                } else {
                    tmp_bool = item.checkout_finished_at.substr(0, 7) === _date;
                }

                return tmp_bool;
            });
            filtered.forEach(function (element) {
                tmpSum += parseInt(element.amount);
            });
            if(tmpSum > 0){
                skynet.array_sum_by_month['date'+y+m] =  tmpSum;
                tmpSumYear += tmpSum;
                //document.write('Monat: '+_date+'   Summe: '+tmpSum+'<br>');
            }
        }
        if(tmpSumYear > 0) {
            skynet.array_sum_by_year.push({[y]: tmpSumYear});
            //document.write('Jahressumme ' + y + ': ' + tmpSumYear + '<br><br>');
        }
    }
    //document.write('---------- ENDE '+state+'------------<br><br>');

    load_data_by_month(state);
}

function update_all_data_by_month_pending(data, state) {
    em = new Entity_manager('skynet_admin', null, skynet.xhr);

    if(data.length > 0){
        for(let i = 0; i < data.length; i++){
            entity_id = data[i].id;
            em.delete(entity_id);
        }
    }
    for(let y = skynet.startYear; y <= skynet.thisYear; y++)for(let m = 1; m <= 12; m++){
        if(y === skynet.thisYear && m === skynet.thisMonth+1)
            break;

        let key = 'date'+y+m,
            val = skynet.array_sum_by_month[key],
            full_date = y+'-'+add_leading_null(m);

        if(val){
            entity_fields = {state: state, sum: val, year: y, month: m, year_and_month:full_date} ;
            em.save(entity_fields);
        }
    }
    skynet.function_queue_number++;
    obj_listener.setProp(skynet.function_queue_number);
}


var obj_listener = {
    prop: false,
    events: {},

    setProp: function(prop) {
        this.prop = prop;
        this.notify('prop', this.prop);
    },

    addEvent: function(name) {
        if (typeof this.events[name] === "undefined") {
            this.events[name] = [];
        }
    },

    register: function(event, subscriber) {
        if (typeof subscriber === "object" && typeof subscriber.notify === 'function') {
            this.addEvent(event);
            this.events[event].push(subscriber);
        }
    },

    notify: function(event, data) {
        //console.log(this.events);
        var events = this.events[event];
        for (var e in events) {
            events[e].notify(data);
        }
    }
};