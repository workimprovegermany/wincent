<!DOCTYPE html>
<html lang="de">
    <?php
    include "inc/head.php";
    ?>
    <body id="index">
        <header>

        </header>

        <main class="extern">
            <div class="container-fluid">
                <section>
                    <div class="grid_6">
                        <div class="grid_12 pad300">
                            <img src="img/logo-wincent.png" alt="wincent your gigital assistant from moneywell" />
                        </div>
                        <div class="grid_12 pad300">
                            <h4 class="highlightFont1">Hier Bitte Ihr Passwort eintragen</h4>

                            <form id="loginform" class="grid_11" autocomplete="off">
                                <div class="grid_12 flex_center">
                                    <p class="error" id="error">Die eingegebenen Login-Daten sind nicht richtig. Bitte versuchen Sie es erneut.</p>
                                </div>
                                <div class="grid_12 flex_center">
                                    <div class="grid_3 pad150">
                                        <label>Passwort</label>
                                    </div>
                                    <div class="grid_9 pad150">
                                        <input type="password" class="js-check-password" name="newPassword" id="newPassword" required>
                                    </div>

                                    <div class="grid_3 pad150">
                                        <label>Passwort wiederholen</label>
                                    </div>
                                    <div class="grid_9 pad150">
                                        <input type="password" class="js-check-password-repeat" name="newPassword2" id="newPassword2" required>
                                        <p class="error">Die Passwörter stimmen nicht überein!</p>
                                    </div>
                                    <input type="hidden" name="id" id="id" value="<?php echo $_GET['id']; ?>">

                                </div>
                                <div class="grid_12 clear">
                                    <button class="btn_01 float-right js-save-new-passwort" type="submit">Speichern</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </section>


            </div>
            <section id="bg-index">
                <img src="img/logo-mw-vertrieb.png" alt="moneywell" />
            </section>

        </main>

        <?php
        include "inc/footer.php";
        ?>

    <script>
           (function()
           {
               var form = document.getElementById('loginform');

               form.addEventListener('submit', function (event) {
                   event.preventDefault();

                   xhr=new XMLHttpRequest();

                   xhr.onreadystatechange=function()
                   {
                       if (xhr.readyState==4 && xhr.status==200)
                       {
                           console.log('answer');
                           var response = xhr.responseText;
                           console.log(response);
                           //token = JSON.parse(response);
                           //console.log(token);
                            window.location.href="index.php";

                    }
                };

                xhr.open('POST', 'api', true);

                var form_data = new FormData();

                form_data.append('af_cmd', 'user.passwort_update');

                   form_data.append('pass', document.querySelectorAll('input[name="newPassword"]')[0].value);
                   form_data.append('session', document.querySelectorAll('input[name="id"]')[0].value);

                xhr.send(form_data);
            }, false);

        }())
    </script>
    </body>
</html>