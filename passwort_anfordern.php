<!DOCTYPE html>
<html lang="de">
    <?php
    include "inc/head.php";
    ?>
    <body id="index">
        <header>

        </header>

        <main class="extern">
            <div class="container-fluid">
                <section>
                    <div class="grid_6">
                        <div class="grid_12 pad300">
                            <a href="./"><img src="img/logo-wincent.png" alt="wincent your gigital assistant from moneywell" /></a>
                        </div>
                        <div class="grid_12 pad300">
                            <h4 class="highlightFont1">Hier bitte Ihre E-Mail Adresse eintragen</h4>

                            <form id="pass_form" class="grid_11" autocomplete="off">
                                <div class="grid_12 flex_center">
                                    <p class="error" id="error">Diese E-Mail Adresse ist in unserem System noch nicht registriert.</p>
                                    <div class="grid_3 pad150">
                                        <label>Benutzer</label>
                                    </div>
                                    <div class="grid_9 marg150 input_email">
                                        <input type="text" name="email_user" class="float-left" autocomplete="off" aria-autocomplete="none" required/>
                                        <input type="text" class="label_email float-left" name="email_domain" autocomplete="off" aria-autocomplete="none" required/>
                                        <div></div>
                                    </div>
                                </div>
                                <div class="grid_12 clear">
                                    <button class="btn_01 float-right" type="submit">Anfordern</button>
                                </div>

                            </form>
                        </div>
                    </div>
                </section>


            </div>
            <section id="bg-index">
                <img src="img/logo-mw-vertrieb.png" alt="moneywell" />
            </section>

        </main>

        <?php
        include "inc/footer.php";
        ?>

    <script>
        (function()
        {
            var form = document.getElementById('pass_form');

            form.addEventListener('submit', function (event) {
                event.preventDefault();

                xhr=new XMLHttpRequest();

                xhr.onreadystatechange=function()
                {
                    if (xhr.readyState==4 && xhr.status==200)
                    {
                        console.log('answer');
                        var response = xhr.responseText;
                        console.log(response);
                        token = JSON.parse(response);
                        console.log(token['data']);

                        if(token['data'] == 'error_email') {
                            // alert(token);
                            document.getElementById("error").style.display='block';
                        }
                        else {
                            window.location.href="email_erfolgreich.php";
                        }
                    }
                };

                xhr.open('POST', 'api', true);

                var form_data = new FormData();

                form_data.append('af_cmd', 'user.passwort_anfordern');

                email_user = document.querySelectorAll('input[name="email_user"]')[0].value;
                email_domain = document.querySelectorAll('input[name="email_domain"]')[0].value;

                form_data.append('user', email_user+'@'+email_domain);

                xhr.send(form_data);
            }, false);

        }())
    </script>
    </body>
</html>