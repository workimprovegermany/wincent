<?php

require_once('../app-foundation/server-php/foundation.php');

$app_config = new Config(APP_CONFIG);
$app_name = $app_config->entry('app_name').'App';
require_once(APP_ROOT_PATH.'/'.$app_name.'.php');
$app = $app_name::get_instance();


$investments_mapper = new Entity_mapper($app->db(),'skynet_investments/1.0/config.xml');
$investors_mapper = new Entity_mapper($app->db(),'skynet_investors/1.0/config.xml');
$emissions_mapper = new Entity_mapper($app->db(),'skynet_emissions/1.0/config.xml');
$affiliate_conv_mapper = new Entity_mapper($app->db(),'skynet_affiliate_conversions/1.0/config.xml');
$affiliates_mapper = new Entity_mapper($app->db(),'skynet_affiliates/1.0/config.xml');

$abrechnung_mapper = new Entity_mapper($app->db(),'abrechnung_vorlauf_01/1.0/config.xml');



$i= 1;
$investments_ids = $app->db()->query_all("SELECT * from skynet_investments WHERE state ='running' ");
foreach ($investments_ids as $investment_id) {

    $invm_id = $investment_id['id'];

        $investor = $investors_mapper->find_by_id($investment_id['investor_id']);
        $emission = $emissions_mapper->find_by_id($investment_id['emission_id']);


       if($emission->field('minimum_volume') == 'NULL' || empty($emission->field('minimum_volume'))) {
        $abrechnung_fields = [
            'investments_id' => $invm_id,
            'investor_id' => $investment_id['investor_id'],
            'reference_nr' => $investment_id['reference'],
            'investor_first_name' => $investor->field('first_name'),
            'investor_last_name' => $investor->field('last_name'),
            'emission_id' => $investment_id['emission_id'],
            'emission_reference' => $emission->field('reference'),
            'betrag' => $investment_id['amount'],
            'state' => $investment_id['state'],
            'payin_settled_at' => $investment_id['payin_settled_at'],
        ];

           echo 'a)'.$investment_id['amount']."-";
           echo $investor->field('first_name')."--";

           echo $investment_id['id']."<pre>";

        $abr_inv = $abrechnung_mapper->find_by_fields(['investments_id' => $invm_id]);

        if ($abr_inv !== null)
        {
            //ja, existiert: updaten
            $abrechnung_mapper->update($abr_inv[0]->id(), $abrechnung_fields);
        }
        else {
            //nein neu anlegen
            $abr = Entity::from_state($abrechnung_fields, 'abrechnung_vorlauf_01/1.0/config.xml');
            $abrechnung_mapper->insert($abr);
        }




       }

    $affiliate_conv = $affiliate_conv_mapper->find_by_fields(['investment_id' => $invm_id]);

    if($affiliate_conv !== null) {
    //echo $affiliate_conv[0]->field('affiliate')."TEST";
    $affiliate = $affiliates_mapper->find_by_id($affiliate_conv[0]->field('affiliate'));

        $user_mapper = new Entity_mapper($app->db(),'user/1.0/config.xml');
        $user_all = $user_mapper->find_by_fields(['short_id' =>$affiliate->field('param')]);

    echo $i.") ".$invm_id."--".$affiliate_conv[0]->field('affiliate')."<pre>";
    $abrechnung_fields_two = [
        'uid' => $user_all[0]->id(),
        'affiliate_id' => $affiliate_conv[0]->field('affiliate'),
        'affiliate_name' => $affiliate->field('name'),
        'affiliate_param' => $affiliate->field('param'),
        'network_id' => $affiliate_conv[0]->field('network_id'),
        ];
        $abrechnung_mapper->update($abr_inv[0]->id(), $abrechnung_fields_two);
        $i++;
    }
    else {
        $affiliate_id = '171';
        $affiliate_name = 'Moneywell Vertriebsgesellschaft mbH';
        $affiliate_param = '2c89U';
        $user_id = '145';

        $abrechnung_fields_two = [
            'affiliate_id' => $affiliate_id,
            'affiliate_name' => $affiliate_name,
            'affiliate_param' => $affiliate_param,
            'uid' => $user_id,

        ];
        $abrechnung_mapper->update($abr_inv[0]->id(), $abrechnung_fields_two);
    }

}





