<?php

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Europe/Berlin');

$filter = json_decode('
    {
        "entity_filter": 
        {
            "gelesen": "0",
            "_or": 
            {
                "absender" : "3",
                "empfaenger": "3"
            },
            "beantwortet": [0,1]
        }
    }
', true);


echo "<pre>";
echo "filter <br/>";
print_r($filter);
echo "error <br/>";
echo json_last_error();
echo json_last_error_msg();