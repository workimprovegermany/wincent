<?php

use setasign\Fpdi\Fpdi;

// BEACHTE: DER IMPORT VON PDF DOKUMENTEN GEHT NUR BIS PDF-VERSION 1.4

require_once ("fpdf.php");
require_once('FPDI-2.2.0/src/autoload.php');

$id = $_GET['id'];

// initiate FPDI
$pdf = new Fpdi();
$pdf->AddPage();
$pdf->setSourceFile('premium_juniorpaket.pdf');


$tplIdx = $pdf->importPage(1);
$pdf->useTemplate($tplIdx, 0, 0, 210);

$pdf->AddPage();
$tplIdx = $pdf->importPage(2);
$pdf->useTemplate($tplIdx, 0, 0, 210);

$pdf->SetFont('Helvetica');
$pdf->SetTextColor(0, 0, 0);
$pdf->SetXY(140, 24);
$pdf->Write(0, $id);

for ($i=3; $i<=23; $i++)
{
    $pdf->AddPage();
    $tplIdx = $pdf->importPage($i);
    $pdf->useTemplate($tplIdx, 0, 0, 210);
}

$pdf->Output();

