<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>


<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);

require_once("../WincentApp.php");
$app = WincentApp::get_instance();

$mail_event = new AF\Event('mail.send', [
    'config' => 'supportmails',
    'template' => ['mails/user_blocked.html',
        [
            'user_login' => 1,
        ]
    ],
]);

$app->dispatch_event($mail_event);
?>

</body>
</html>