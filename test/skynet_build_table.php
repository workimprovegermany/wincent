<?php

require_once('../app-foundation/server-php/foundation.php');

$app_config = new Config(APP_CONFIG);
$app_name = $app_config->entry('app_name').'App';
require_once(APP_ROOT_PATH.'/'.$app_name.'.php');
$app = $app_name::get_instance();

echo "<pre>";

require_once ("skynet.php");
$rc = new Skynet();

//$rc->all_to_db($app->db(),'projects');
//$rc->all_to_db($app->db(),'emissions');
//$rc->all_to_db($app->db(),'issuers');
//$rc->all_to_db($app->db(),'investors');
//$rc->all_to_db($app->db(),'investments');
//$rc->all_to_db($app->db(),'affiliates');
//$rc->all_to_db($app->db(),'affiliate_conversions');

// export affiliates to user and user_login tables
$affiliate_mapper = new Entity_mapper($app->db(),'skynet_affiliates/1.0/config.xml');
$user_mapper = new Entity_mapper($app->db(),'user/1.0/config.xml');
$user_login_mapper = new Entity_mapper($app->db(),'user_login/1.0/config.xml');

$investor_ids = $app->db()->query_all("SELECT id from skynet_investors");
foreach ($investor_ids as $investor_id)
{
    $iid = $investor_id['id'];
    $investor = Entity_mapper::load_entity_from_storage($iid, 'skynet_investors/1.0/config.xml', $app->db());

    $affiliate = $affiliate_mapper->find_by_fields(['name' => $investor->field('first_name').' '.$investor->field('last_name')]);


    /* NICHT IMPORTIEREN
    if ($affiliate !== null)
    {
        $affiliate_fields = [
            'short_id' => $affiliate[0]->field('param'),
            'firma' => $affiliate[0]->field('company_register_name'),
            'vorname' => $investor->field('first_name'),
            'nachname' => $investor->field('last_name'),
            'email' => $investor->field('email'),
            'anrede' => $investor->field('gender') == 'male' ? '1' : '2',
            'strasse' => $investor->field('street'),
            'hausnummer' => $investor->field(''),
            'plz' => $investor->field('zip_code'),
            'ort' => $investor->field('city'),
            'telefon' => $investor->field('phone_number'),
            'kontoinhaber' => $investor->field('bank_account_owner'),
            'bank' => $investor->field(''),
            'iban' => $investor->field('bank_account_iban'),
            'bic' => $investor->field('bank_account_bic'),
            'steuernummer' => $investor->field('tax_identification_number'),
        ];

        $user = $user_mapper->find_by_id($iid);
        if ($user !== null)
        {
            //ja, existiert: updaten
            $user_mapper->update($iid, $affiliate_fields);
        }
        else
        {
            //nein neu anlegen
            $u = Entity::from_state($affiliate_fields, 'user/1.0/config.xml');
            $user_mapper->insert($u);

            $ul = Entity::from_state([
                    'uid' => $u->id(),
                    'uname' => $u->field('email'),
                ], 'user_login/1.0/config.xml');
            Entity_mapper::save_entity_to_storage($ul, $app->db());
        }
    }*/

}






