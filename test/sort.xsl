<?xml version="1.0"?>

<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:function name="ima:insertion_sort" as="xs:anyAtomicType*">
        <xsl:param name="m_in" as="xs:anyAtomicType*" />

        <xsl:sequence select="ima:insertion_sort( $m_in, 1 to count($m_in), ())" />
    </xsl:function>

    <xsl:function name="ima:insertion_sort" as="xs:anyAtomicType*">
    <xsl:param name="m_in"         as="xs:anyAtomicType*" />
    <xsl:param name="m_index_list" as="xs:integer*" />
    <xsl:param name="m_out"        as="xs:anyAtomicType*" />

    <xsl:variable name="m_index"    as="xs:integer?" select="ima:head( $m_index_list )" />
    <xsl:variable name="m_position" as="xs:integer"  select="ima:get_position($m_out, $m_in[$m_index])" />
    <xsl:variable name="m_new_out"  as="xs:anyAtomicType*" select="($m_out[position() lt $m_position],$m_in[$m_index],$m_out[position() ge $m_position])" />

    <xsl:sequence select="if( $m_index ) then ima:insertion_sort( $m_in, ima:tail($m_index_list), $m_new_out ) else $m_new_out" />
    </xsl:function>

    <xsl:function name="ima:get_position" as="xs:integer">
    <xsl:param name="m_list"  as="xs:anyAtomicType*" />
    <xsl:param name="m_value" as="xs:anyAtomicType?" />

    <xsl:sequence select="count($m_list[. lt $m_value]) + 1" />
    </xsl:function>

</xsl:stylesheet>